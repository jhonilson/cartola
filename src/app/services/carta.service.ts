import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Carta1content } from '../model/carta1content';
import { Carta1detalle } from '../model/carta1detalle';
import { Carta1total } from '../model/carta1total';
import { Carta2content } from '../model/carta2content';
import { Carta2detalle } from '../model/carta2detalle';

@Injectable({
  providedIn: 'root'
})
export class CartaService {

  constructor(private http: HttpClient) { }

  url = environment.urlCarta1;

  // private httpHeader = new HttpHeaders('Content-Type: application/json');

  getCarta1Content(socio: string) {
    return this.http.post<Carta1content[]>(this.url + '/generarCarta', socio);
  }

  getCarta1Detalle(socio: string) {
    return this.http.post<Carta1detalle[]>(this.url + '/generarCartaDetalles', socio);
  }

  getCarta1Total(socio: string) {
    return this.http.post<Carta1total[]>(this.url + '/generarCartaTotales', socio);
  }

  // *******************************

  getCarta2Content(socio: string) {
    return this.http.post<Carta2content[]>(this.url + '/generarCarta2', socio);
  }

  getCarta2Detalle(socio: string) {
    return this.http.post<Carta2detalle[]>(this.url + '/generarCarta2Detalles', socio);
  }

}
