import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecuperosLista } from '../model/recuperos-lista';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RecuperosService {
  constructor(private http: HttpClient) {
    // this.http = http;
  }

  // url = 'http://localhost:7000/socios';

  urlRecuperos = environment.urlRecuperosParciales;
  // url = 'http://logicad.cardif.cl/CartolaBack/socios';

  // private httpHeader = new HttpHeaders('Content-Type: application/json');

  buscarRecuperos(recuperos: any[]) {
    let dia = '';
    let mes = '';
    let ano = '';
    for (let i = 0; i < recuperos.length; i++) {
      console.log('recuperos[i].mesCobertura ', recuperos[i].mesCobertura);
      console.log('recuperos[i].mesCobertura substr 0, 2' , String(recuperos[i].mesCobertura).substr(0, 2));
      console.log('recuperos[i].mesCobertura substr 3, 5' , String(recuperos[i].mesCobertura).substr(3, 2));
      console.log('recuperos[i].mesCobertura substr 6, 10' , String(recuperos[i].mesCobertura).substr(6, 4));
      dia = String(recuperos[i].mesCobertura).substr(0, 2);
      mes = String(recuperos[i].mesCobertura).substr(3, 2);
      ano = String(recuperos[i].mesCobertura).substr(6, 4);
      recuperos[i].mesCobertura = dia + '/' + mes + '/' + ano;
      dia = String(recuperos[i].mesContableCore).substr(0, 2);
      mes = String(recuperos[i].mesContableCore).substr(3, 2);
      ano = String(recuperos[i].mesContableCore).substr(6, 4);
      recuperos[i].mesContableCore = dia + '/' + mes + '/' + ano;
    }

    const request = {
      Body: recuperos,
    };

    console.log('request recuperos ', request);
    return this.http.post<RecuperosLista>(this.urlRecuperos, request);
  }

}
