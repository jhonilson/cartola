import { Injectable } from '@angular/core';
import { Auditoria } from '../model/auditoria';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Fechas } from '../model/fechas';

@Injectable({
  providedIn: 'root',
})
export class AuditoriaService {
  constructor(private http: HttpClient) {
    // this.http = http;
  }

  // urlJohn = 'http://localhost:7000/auditoria';
 // url = 'http://localhost:8080/CARWSCartola/auditoria';
 url = environment.urlAuditoria;

  // private httpHeader = new HttpHeaders('Content-Type: application/json');

  getAll() {
     return this.http.get<Auditoria[]>(this.url);
    // return this.http.get<Auditoria[]>(this.url + '/get');
  }

  save(p: Auditoria) {
    return this.http.post<any>(this.url + '/add', p);
  }

  findById(id: number) {
    return this.http.get<Auditoria>(this.url + '/findById/' + '' + id);
  }

  update(p: Auditoria) {
    return this.http.put<any>(this.url + '/update ', p);
  }

  delete(id: number) {
    return this.http.delete<any>(this.url + '/delete/' + '' + id);
  }

  getRango(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo) + ' *** ' + JSON.stringify(f.flujo));
    // console.log('en el servicio: '+ JSON.stringify(f));
    return this.http.get<Auditoria[]>(this.url + '/findRangoGet/'  + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) + '/' + f.flujo );
    // return this.http.get<Auditoria[]>(`http://localhost:7000/auditoria/findRangoGet/${JSON.stringify(f.dateFrom)}/${JSON.stringify(f.dateTo)}/${f.flujo}` );
  }

/*   getRango(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo) + ' *** ' + f.flujo);
    // console.log('en el servicio: '+ JSON.stringify(f));
    return this.http.get<Auditoria[]>(this.url + '/findRangoGet/' +  f.flujo );
  } */
}
