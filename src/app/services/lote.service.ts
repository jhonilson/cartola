import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Fechas } from '../model/fechas';
import { Lote } from '../model/lote';
import { LoteDetalle } from '../model/lote-detalle';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoteService {

  constructor(
    private http: HttpClient
  ) { }

  urlExtraccion = environment.urlExtraccion;

  getLotes(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo)) ;
    // console.log('en el servicio: '+ JSON.stringify(f));
  //  return this.http.get<Lote[]>(this.urlJohn + 'lotes/findRangoGet/' + '/' + f.dateFrom + '/' + f.dateTo );
    return this.http.get<Lote[]>(this.urlExtraccion + '/lotes/findRangoGet/' + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) );
  }

  getLoteDetalle(id: string){
    return this.http.get<LoteDetalle[]>(this.urlExtraccion + '/lotesDetalle/findById/' + id);
  }

}
