import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Entidad } from '../model/entidad';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntidadService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  // url = 'http://localhost:7000/entidades/';
  // url = 'http://localhost:8080/CARWSCartola/entidades/';
  // http://localhost:8080/entidades/entBySocio/1
   // url = 'http://logicad.cardif.cl/CartolaBack/entidades/';
   url = environment.urlEntidades;

  getAll() {
     return this.http.get<Entidad[]>(this.url + 'get');
   //  return this.http.get<Entidad[]>(this.url );
  }

  save(p: Entidad) {
    return this.http.post<any>(this.url + 'add ', p);
  }

  findById(id: number) {
    return this.http.get<Entidad>(this.url + 'findById/' + id);
  }

  update(p: Entidad) {
    return this.http.put<any>(this.url + 'update', p);
  }

  delete(id: number ) {
    return this.http.delete<any>(this.url + 'delete/' + '' + id);
  }

  entBySocio(id: number) {
    return this.http.get<Entidad[]>(this.url + 'entBySocio/' + id);
  }

}
