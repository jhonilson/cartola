import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Filtros } from '../model/filtros';
import { ResponseAgrupaciones } from '../model/response-agrupaciones';
import { DetalleAgrupacion } from '../model/detalle-agrupacion';
import { Agrupacion } from '../model/agrupacion';
import { Fechas } from '../model/fechas';

@Injectable({
  providedIn: 'root'
})
export class AgrupacionesService {

  urlGroup = environment.urlGroup;
  urlGroupDetalles = environment.urlGroupDetalles;
  urlAgrupacionReporte = environment.urlAgrupacionReporteRango;

  constructor(private http: HttpClient) {  }

  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getAll(f: Filtros) {

    console.log('filtros: ', f);
    const requestAgrupaciones = {
      Body: {
      socio: f.socio,
      compania: f.compania,
      mesCobertura: f.mesCobertura,
      periodoContable: f.periodoContable,
      estado: f.estado,
      concepto: f.concepto,
      subConcepto: f.subConcepto,
      poliza: f.poliza
      }
    };

    return this.http.post<ResponseAgrupaciones>(this.urlGroup, requestAgrupaciones);
  }

  getDetalles(id: string) {
    console.log('ID: ', id);
    return this.http.get<DetalleAgrupacion>(this.urlGroupDetalles + '' + id);
  }

  
  // *********** para reporte agrupacion - patalla gestion ******************
  getAllAgrupacion() {
    return this.http.get<Agrupacion[]>(this.urlAgrupacionReporte + '/agrupacion');
   // return this.http.get<Auditoria[]>(this.url + '/get');
 }

  getAgrupacionByRango(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo)) ;
    // console.log('en el servicio: '+ JSON.stringify(f));
  //  return this.http.get<Lote[]>(this.urlJohn + 'lotes/findRangoGet/' + '/' + f.dateFrom + '/' + f.dateTo );
    return this.http.get<Agrupacion[]>(this.urlAgrupacionReporte + '/agrupacion/findRangoGet/' + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) + '/' + f.estado);
  }

  // ******************************************************

}
