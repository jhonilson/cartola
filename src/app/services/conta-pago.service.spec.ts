import { TestBed } from '@angular/core/testing';

import { ContaPagoService } from './conta-pago.service';

describe('GastosService', () => {
  let service: ContaPagoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContaPagoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
