import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  // url = 'http://localhost:7000/socios';
 // url = 'http://localhost:8080/CARWSCartola/socios';
  // url = 'http://logicad.cardif.cl/CartolaBack/socios';
   url = environment.urlSocios;
   url2 = environment.urlSocios2;

  private httpHeader = new HttpHeaders('Content-Type: application/json');

   getAll() {
   // return this.http.get<Socio[]>(this.url);
    return this.http.get<Socio[]>(this.url + '/get');
  } 
/*   getAll() {
    // return this.http.get<Socio[]>(this.url);
      return this.http.get<Socio[]>(this.url );
   } */

  save(p: Socio) {
    return this.http.post<any>(this.url + '/add', p);
  }

  findById(id: number) {
    return this.http.get<Socio>(this.url + '/findById/' + '' + id);
  }

  update(p: Socio) {
    return this.http.put<any>(this.url + '/update ', p);
  }

  delete(id: number ) {
    return this.http.delete<any>(this.url + '/delete/' + '' + id);
  }


  getSociosPago() {
     return this.http.get<string[]>(this.url2 + '/listaSociosPago');
   } 

}
