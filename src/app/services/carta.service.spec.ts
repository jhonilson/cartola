import { TestBed } from '@angular/core/testing';

import { Carta1Service } from './carta.service';

describe('Carta1Service', () => {
  let service: Carta1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Carta1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
