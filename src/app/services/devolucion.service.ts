import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Devolucion } from '../model/devolucion';
import { Filtros } from '../model/filtros';

@Injectable({
  providedIn: 'root',
})
export class DevolucionService {
  constructor(private http: HttpClient) {}

  url = environment.urlDevolucion;

  // private httpHeader = new HttpHeaders('Content-Type: application/json');

  getAll() {
    return this.http.get<Devolucion[]>(this.url);
  }

  getPagos(f: Filtros) {
    const request = {
      socio: f.socio,
      compania: f.compania,
      portalBancario: f.portalBancario,
      medioPago: f.medioPago,
      pagara: f.pagara
    };
    console.log(request);
    return this.http.post<any>(this.url + '/listarPagos', request);
  }


  crearNomina2(sociox: string, pagos: Devolucion[]) {
    // console.log('PAGOS: ' + JSON.stringify(pagos));
    const request = {
      socio: sociox,
      devoluciones: pagos,
    };
    console.log(request);
    return this.http.post<any>(this.url + '/generarNomina', request);
  }

  crearNomina(f: Filtros, pagos: Devolucion[]) {
    const request = {
      socio: f.socio,
      compania: f.compania,
      portalBancario: f.portalBancario,
      medioPago: f.medioPago,
      devoluciones: pagos,
    };
    console.log(request);
    return this.http.post<any>(this.url + '/generarNomina', request);
  }

  // save(p: Devolucion) {
  //   return this.http.post<any>(this.url + '/add', p);
  // }

  // findById(id: number) {
  //   return this.http.get<Devolucion>(this.url + '/findById/' + '' + id);
  // }

  // update(p: Devolucion) {
  //   return this.http.put<any>(this.url + '/update ', p);
  // }

  // delete(id: number) {
  //   return this.http.delete<any>(this.url + '/delete/' + '' + id);
  // }
}
