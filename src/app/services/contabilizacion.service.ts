import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Fechas } from '../model/fechas';
import { Contabilizacion } from '../model/contabilizacion';


@Injectable({
  providedIn: 'root'
})
export class ContabilizacionService {

  constructor(
    private http: HttpClient
  ) { }

  urlContabilizacionReporte = environment.urlContabilizacionReporte;

  getAll() {
    return this.http.get<Contabilizacion[]>(this.urlContabilizacionReporte + '/contabilizacion');
   // return this.http.get<Auditoria[]>(this.url + '/get');
 }

  getContabilizaciones(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo)) ;
    // console.log('en el servicio: '+ JSON.stringify(f));
  //  return this.http.get<Lote[]>(this.urlJohn + 'lotes/findRangoGet/' + '/' + f.dateFrom + '/' + f.dateTo );
    return this.http.get<Contabilizacion[]>(this.urlContabilizacionReporte + '/contabilizacion/findRangoGet/' + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) + '/' + f.estado);
  }

/*   getLoteDetalle(id: string){
    return this.http.get<LoteDetalle[]>(this.urlExtraccion + '/lotesDetalle/findById/' + id);
  } */

}
