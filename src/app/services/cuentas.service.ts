import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { ResponseCuentas } from '../model/response-cuentas';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuentasService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  urlCuentas = environment.urlCuentas;

  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getCuentas(id: string) {
    const request = {
        Body: {
          idSocio: id
        }
      };
    return this.http.post<ResponseCuentas>(this.urlCuentas, request);
  }

}
