import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { ResponseNetsuite } from '../model/response-netsuite';
import { environment } from '../../environments/environment';
import { PeriodosCierre } from '../model/periodos-cierre';
import { CerrarPeriodo } from '../model/cerrar-periodo';

@Injectable({
  providedIn: 'root'
})
export class PeriodosCierreService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  //urlNetsuite = 'http://localhost:8080/CARWSCartola/netsuite/get';
  urlPeriodosCierre = environment.urlPeriodosCierre;
  urlCerrarPeriodo = environment.urlCerrarPeriodo;
  urlAbrirPeriodo = environment.urlAbrirPeriodo;
  // http://localhost:8080/entidades/entBySocio/1
  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getPeriodosCierre() {
    const request = {
      };

    return this.http.post<PeriodosCierre>(this.urlPeriodosCierre, {});
    //return this.http.get<PeriodosCierre>('./assets/periodoCierreDummy.json');
  }

  getCierre(idPeriodo: string){
    let nombreUsuario = sessionStorage.getItem('usuario');
    const request = {
      Body: {
        idPeriodo: idPeriodo,
        usuario: nombreUsuario
      }
    }

    return this.http.post<CerrarPeriodo>(this.urlCerrarPeriodo, request);
  }
  
  getAbrirPeriodo(idPeriodo: string){
    let nombreUsuario = sessionStorage.getItem('usuario');
    const request = {
      Body: {
        idPeriodo: idPeriodo,
        usuario: nombreUsuario
      }
    }

    return this.http.post<CerrarPeriodo>(this.urlAbrirPeriodo, request);
  }

}
