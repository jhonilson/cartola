import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { ResponseSunsystem } from '../model/response-sunsystem';
import { ResponseValidaciones } from '../model/response-validaciones';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ValidacionesService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  urlValidaciones = environment.urlValidaciones;
  // http://localhost:8080/entidades/entBySocio/1
  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getValidaciones(strTipo: string) {
    const request = {
      Body: {
        tipo: strTipo
      }
    };

   // return this.http.post<ResponseValidaciones>(this.urlValidaciones, request);
    return this.http.get<ResponseValidaciones>('./assets/validaDummy.json');
  }

}
