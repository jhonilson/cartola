import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TransaccionRej } from '../model/transaccion-rej';
import { environment } from '../../environments/environment';
import { Fechas } from '../model/fechas';

@Injectable({
  providedIn: 'root'
})
export class TransaccionService {

  constructor(private http: HttpClient) {
    // this.http = http;
  }

   //url = 'http://localhost:7000/transaccionesRej';
 // url = 'http://localhost:8080/CARWSCartola/auditoria';
  url = environment.urlTransacciones;

  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getAll() {
     return this.http.get<TransaccionRej[]>(this.url);
    // return this.http.get<Auditoria[]>(this.url + '/get');
  }

  save(p: TransaccionRej) {
    return this.http.post<any>(this.url + '/add', p);
  }

  findById(id: number) {
    return this.http.get<TransaccionRej>(this.url + '/findById/' + '' + id);
  }

  update(p: TransaccionRej) {
    return this.http.put<any>(this.url + '/update ', p);
  }

  delete(id: number) {
    return this.http.delete<any>(this.url + '/delete/' + '' + id);
  }

  getRango(f: Fechas) {
    console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo) + ' *** ' + JSON.stringify(f.subconcepto));
    // console.log('en el servicio: '+ JSON.stringify(f));
    return this.http.get<TransaccionRej[]>(this.url + '/findRangoGet/'  + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) + '/' + f.subconcepto );

  }

}
