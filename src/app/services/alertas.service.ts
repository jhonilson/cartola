import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor() { }

  alertBasic(msg: string) {
    Swal.fire(msg);
  }

  alertBasicIcon(title: string, msg: string, icon: SweetAlertIcon) { // 'question', 'info', 'error', 'warning'
    Swal.fire(
      title,
      msg,
      icon
    );
  }

  alertBasic2( msg: string, icon: SweetAlertIcon) {
    Swal.fire({
      position: 'center',
      icon,
      title: msg,
      showConfirmButton: true,
      // timer: 1500
    });
  }

/*   alertConfirmation() {
    Swal.fire({
      title: '¿Esta seguro que desea ejecutar esta operacion?',
      text: 'Esta no se podra revertir!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
         Swal.fire(
          'Ejecutado!',
          'Su operación se ha realizado con Exito!',
          'success'
        )
      }
    });
  } */

  alertNotification2( msg: string, icon: SweetAlertIcon){
    Swal.fire({
      position: 'top-end',
      title: msg,
      showConfirmButton: false,
      timer: 2000
    });
  }
  
  alertNotification(msg: string, icono: SweetAlertIcon){
    const Toast = Swal.mixin({
      toast: true,
      // background: 'rgba(0,0,0,0) linear-gradient(#444,#111) repeat scroll 0 0',
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: icono,
      title: '<span style="color:red">' + msg + '</span>'
    })
  }

}
