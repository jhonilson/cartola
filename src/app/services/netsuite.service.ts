import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { ResponseNetsuite } from '../model/response-netsuite';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NetsuiteService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  // urlNetsuite = 'http://localhost:8080/CARWSCartola/netsuite/get';
  urlNetsuite = environment.urlNetsuite;
  // http://localhost:8080/entidades/entBySocio/1
  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getNetsuite(numCuenta: string, compania: string) {
    const request = {
      Body: {
        id: numCuenta,
        cia: compania
      }
    };
    console.log('request nestuite ' , request);
    return this.http.post<ResponseNetsuite>(this.urlNetsuite, request);
    // return this.http.get<ResponseNetsuite>('./assets/netsuiteDummy.json');
  }

}
