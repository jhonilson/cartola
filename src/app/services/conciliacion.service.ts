import { Injectable } from '@angular/core';
import { ConciliacionReporte } from '../model/conciliacion-reporte';
import { HttpClient } from '@angular/common/http';
import { Filtros } from '../model/filtros';
import { DetalleConciliacion } from '../model/detalle-conciliacion';
import { environment } from '../../environments/environment';
import { Conciliacion } from '../model/conciliacion';
import { formatDate } from '@angular/common';
import { ConciliacionRango } from '../model/conciliacion-rango';
import { Fechas } from '../model/fechas';

@Injectable({
  providedIn: 'root',
})
export class ConciliacionService {
  constructor(private http: HttpClient) {
    // this.http = http;
  }

  // url = 'http://localhost:7000/socios';
  // url = 'http://localhost:8080/CARWSCartola/conciliacion/';
  // url = 'http://logicad.cardif.cl/CartolaBack/socios';
  url = environment.urlConciliacionReporte;
  urlConciliar = environment.urlConciliar;
  urlConciliacionReporte = environment.urlConciliacionReporteRango;
  urlContaPago = environment.urlContaPago;

  // private httpHeader = new HttpHeaders('Content-Type: application/json');
  user = sessionStorage.getItem('usuario');

  getAll(f: Filtros) {
    const request = {
      Body: {
        socio: f.socio,
        compania: f.compania,
        mesCobertura: f.mesCobertura,
        periodoContable: f.periodoContable,
        estado: f.estado,
        subconcepto: f.subConcepto,
        poliza: f.poliza,
        aprobacion: f.aprobacion,
        anoContable: f.anoContable,
      },
    };
    return this.http.post<ConciliacionReporte>(
      this.url + 'get_report_conciliacion',
      request
    );
  }

  getDetalles(id: string) {
    const request = {
      Body: {
        idConciliacion: id,
      },
    };
    return this.http.post<DetalleConciliacion>(
      this.url + 'get_det_report',
      request
    );
  }

  aprobarConciliacion(id: string) {
    const request = {
      Body: {
        idConciliacion: id,
        usuario: this.user,
      },
    };
    console.log('request: ' + JSON.stringify(request));
    return this.http.post<any>(this.url + 'aprobacion', request);
  }

  rechazarConciliacion(id: string) {
    const request = {
      Body: {
        idConciliacion: id,
        usuario: this.user,
      },
    };
    console.log('request: ' + JSON.stringify(request));
    return this.http.post<any>(this.url + 'rechazo', request);
  }

  reversarConciliacion(id: string, reversa: string) {
    const request = {
      Body: {
        idConciliacion: id,
        usuario: this.user,
        accion: reversa
      },
    };
    console.log('request: ' + JSON.stringify(request));
    console.log('reversa: ' + reversa);

    return this.http.post<any>(this.url + 'reversa', request);
  }


    // *********** para reporte conciliacion ******************
    getAllConciliacion() {
      return this.http.get<ConciliacionRango[]>(this.urlConciliacionReporte + '/conciliacion');
     // return this.http.get<Auditoria[]>(this.url + '/get');
   }

    getConciliacionByRango(f: Fechas) {
      console.log('en el servicio: '+ JSON.stringify(f.dateFrom) + ' *** ' + JSON.stringify(f.dateTo)) ;
      // console.log('en el servicio: '+ JSON.stringify(f));
    //  return this.http.get<Lote[]>(this.urlJohn + 'lotes/findRangoGet/' + '/' + f.dateFrom + '/' + f.dateTo );
      return this.http.get<ConciliacionRango[]>(this.urlConciliacionReporte + '/conciliacion/findRangoGet/' + JSON.stringify(f.dateFrom) + '/' + JSON.stringify(f.dateTo) + '/' + f.estado);
    }

    // ******************************************************


  getConciliacion(
    listaPrima: any[],
    listaComision: any[],
    listaDevolucion: any[],
    listaRecupero: any[],
    listaGasto: any[],
    listaSunsystem: any[],
    listaNetsuite: any[],
    monto: any,
    prio: string,
    tolerancia: any,
    salidaDinero: string,
    periodoContable: string,
    listaValidaciones: any[]
  ) {
    let arrPrima = [];
    let arrComision = [];
    let arrDevolucion = [];
    let arrRecupero = [];
    let arrGasto = [];
    let arrSunsystem = [];
    let arrNetsuite = [];
    let hoy = new Date();
    let fechaHoy = '';
    // fechaHoy = hoy.getDay().toString() + "-" + hoy.getMonth().toString() + "-" + hoy.getDay().toString();
    fechaHoy = formatDate(hoy, 'dd-MM-yyyy', 'en-US');
    if (listaPrima.length > 0) {
      for (let prima of listaPrima) {
        var obj = { idAgrupacion: prima.idAgrupacion };
        arrPrima.push(obj);
      }
    }
    if (listaComision.length > 0) {
      for (let comision of listaComision) {
        var obj = { idAgrupacion: comision.idAgrupacion };
        arrComision.push(obj);
      }
    }
    if (listaDevolucion.length > 0) {
      for (let devolucion of listaDevolucion) {
        var obj = { idAgrupacion: devolucion.idAgrupacion };
        arrDevolucion.push(obj);
      }
    }
    if (listaRecupero.length > 0) {
      for (let recupero of listaRecupero) {
        var obj = { idAgrupacion: recupero.idAgrupacion };
        arrRecupero.push(obj);
      }
    }
    if (listaGasto.length > 0) {
      for (let gasto of listaGasto) {
        var obj = { idAgrupacion: gasto.idGastos };
        arrGasto.push(obj);
      }
    }
    if (listaSunsystem.length > 0) {
      for (let sunsystem of listaSunsystem) {
        var objSun = {
          // idSunsystem: sunsystem.idSystem,
          accountCode: sunsystem.accountCode,
          allocationMarker: sunsystem.allocationMarker,
          analysisCode1: sunsystem.analysisCode1,
          analysisCode2: sunsystem.analysisCode2,
          analysisCode3: sunsystem.analysisCode3,
          analysisCode4: sunsystem.analysisCode4,
          analysisCode5: sunsystem.analysisCode5,
          analysisCode6: sunsystem.analysisCode6,
          analysisCode7: sunsystem.analysisCode7,
          analysisCode8: sunsystem.analysisCode8,
          analysisCode9: sunsystem.analysisCode9,
          analysisCode10: sunsystem.analysisCode10,
          baseAmount: sunsystem.baseAmount,
          debitCredit: sunsystem.debitCredit,
          description: sunsystem.description,
          journalNumber: sunsystem.journalNumber,
          transactionDate: sunsystem.transactionDate,
          TransactionReference: sunsystem.TransactionReference
        };
        arrSunsystem.push(objSun);
      }
    }
    if (listaNetsuite.length > 0) {
      for (let netsuite of listaNetsuite) {
        var objNet = { idNetsuite: netsuite.idNetsuite };
        arrNetsuite.push(objNet);
      }
    }

    const request = {
      Body: {
        Prima: arrPrima,
        Comision: arrComision,
        Devolucion: arrDevolucion,
        Recupero: arrRecupero,
        Gastos: arrGasto,
        Validaciones: listaValidaciones,
        Sunsystems: arrSunsystem,
        Netsuite: arrNetsuite,
        Conciliacion: {
          tolerancia: tolerancia,
          fechaConversion: fechaHoy,
          salidaDinero: salidaDinero,
          periodoDeConciliacion: periodoContable,
          montoContraSunsystem: monto,
          Usuairo: 'Cartola',
          medioPago: '',
          prio: prio,
        },
      },
    };

    console.log('requestConci ' + JSON.stringify(request));

    return this.http.post<Conciliacion>(this.urlConciliar, request);
  }


  getContaPago(f: Filtros) {
    console.log('request contapago ', f);
    const request = {
      Body: {
        compania: f.compania,
        anoContable: f.anoContable,
        socio: f.socio,
        poliza: f.poliza,
        periodoContable: f.periodoContable,
        estado: f.estado,
        aprobacion: f.aprobacion
      },
    };
    // return this.http.post<ConciliacionReporte>(this.urlContaPago, request);
    return this.http.post<ConciliacionReporte>(this.urlContaPago, request);
    // return this.http.get<ConciliacionReporte>('./assets/contaPagoDummy.json');
  }


  getAllReverse(f: Filtros) {
    const request = {
      Body: {
        socio: f.socio,
        compania: f.compania,
        mesCobertura: f.mesCobertura,
        periodoContable: f.periodoContable,
        estado: f.estado,
        concepto: f.concepto,
        poliza: f.poliza,
        aprobacion: f.aprobacion,
        anoContable: f.anoContable,
      },
    };
    return this.http.post<ConciliacionReporte>(
      this.url + 'get_report_conciliacion_reversa',
      request
    );
  }
  
}
