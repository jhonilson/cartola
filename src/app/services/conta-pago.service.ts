import { Injectable } from '@angular/core';
import { ConciliacionReporte } from '../model/conciliacion-reporte';
import { HttpClient } from '@angular/common/http';
import { Filtros } from '../model/filtros';
import { DetalleConciliacion } from '../model/detalle-conciliacion';
import { environment } from '../../environments/environment';
import { Conciliacion } from '../model/conciliacion';
import { formatDate } from '@angular/common';
import { VoucherReintento } from '../model/voucher-reintento';

@Injectable({
  providedIn: 'root',
})
export class ContaPagoService {
  constructor(private http: HttpClient) {
  }

  url = environment.urlConciliacionReporte;
  urlContaPago = environment.urlContaPago;
  urlReintento = environment.urlVoucherReintento;
  urlVoucherManual = environment.urlVoucherManual;
  // private httpHeader = new HttpHeaders('Content-Type: application/json');
  user = sessionStorage.getItem('usuario');

  getContaPago(f: Filtros) {
    console.log("request contapago ", f);
    const request = {
      Body: {
        compania: f.compania,
        anoContable: f.anoContable,
        socio: f.socio,
        poliza: f.poliza,
        periodoContable: f.periodoContable,
        estado: f.estado,
        aprobacion: f.aprobacion
      },
    };
    //return this.http.post<ConciliacionReporte>(this.urlContaPago, request);
    return this.http.post<ConciliacionReporte>(this.urlContaPago, request);
    //return this.http.get<ConciliacionReporte>('./assets/contaPagoDummy.json');
  }

  getReintento(idVoucher: string, idContabilizacion: string){
      const request = {
          Body: {
              id: idVoucher,
              idContabilizacion: idContabilizacion
          }
      }

      return this.http.post<VoucherReintento>(this.urlReintento, request);
  }

  setVoucherManual(voucher: any){
      const request = {
          Body: {
              id: voucher.idContabilizacion,
              voucher: voucher.voucher
          }
      }

      return this.http.post<VoucherReintento>(this.urlVoucherManual, request);
  }

}
