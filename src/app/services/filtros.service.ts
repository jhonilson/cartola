import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ResponseFiltros } from '../model/response-filtros';
import { ResponseAgrupaciones } from '../model/response-agrupaciones';
import { Filtros } from '../model/filtros';
import { Observable } from 'rxjs';
import { ResponseContabilizacion } from '../model/response-contabilizacion';
import { ResponseReversa } from '../model/response-reversa';
import { ResponseContaIdVoucher } from '../model/response-conta-IdVoucher';
import { environment } from '../../environments/environment';
import { Estado } from '../model/estado';

@Injectable({
  providedIn: 'root',
})
export class FiltrosService {
  // urlFiltros = 'http://localhost:8080/CARWSCartola/ws/get_filters';
  // urlGroup = 'http://localhost:8080/CARWSCartola/ws/get_groupings';
  // urlContabilizacion = 'http://localhost:8080/CARWSCartola/ws/get_assess';
  // urlReversa = 'http://localhost:8080/CARWSCartola/ws/get_assess_reverse';
  // urlContaIdVoucher = 'http://localhost:8080/CARWSCartola/ws/set_voucher_manual';

  // urlFiltros = 'http://logicad.cardif.cl/CartolaBack/ws/get_filters';
  // urlGroup = 'http://logicad.cardif.cl/CartolaBack/ws/get_groupings';
  // urlContabilizacion = 'http://logicad.cardif.cl/CartolaBack/ws/get_assess';
  // urlReversa = 'http://logicad.cardif.cl/CartolaBack/ws/get_assess_reverse';
  // urlContaIdVoucher = 'http://logicad.cardif.cl/CartolaBack/ws/set_voucher_manual';

  urlFiltros = environment.urlFiltros;
  urlGroup = environment.urlGroup;
  urlContabilizacion = environment.urlContabilizacion;
  urlReversa = environment.urlReversa;
  urlContaIdVoucher = environment.urlContaIdVoucher;
  urlGroupConci = environment.urlGroupConci;

  urlAuditoria = environment.urlAuditoria;
  urlTransaccion = environment.urlTransacciones;
  urlEstados = environment.urlEstados;

  constructor(private http: HttpClient) {}

  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getCompany() {
    const request = {
      Body: {
        type: 'compania',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getMesCobertura() {
    const request = {
      Body: {
        type: 'mes cobertura',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getSocio() {
    const request = {
      Body: {
        type: 'socio',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getPeriodoContable() {
    const request = {
      Body: {
        type: 'periodo contable',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getEstado() {
    const request = {
      Body: {
        type: 'estado',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getEstadoAgrupacion() {
    const request = {
      Body: {
        type: 'estado reporte agrupacion',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getConcepto() {
    const request = {
      Body: {
        type: 'concepto',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getSubConcepto() {
    const request = {
      Body: {
        type: 'subconcepto',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  // getGroup(compania: string, mesCobertura: string, socio: string, poliza: string,
  //          periodoContable: string, estado: string, concepto: string) {
  getGroup(f: Filtros) {
    console.log('filtros: ', f);
    const requestAgrupaciones = {
      Body: {
        socio: f.socio,
        compania: f.compania,
        mesCobertura: f.mesCobertura,
        periodoContable: f.periodoContable,
        estado: f.estado,
        concepto: f.concepto,
        poliza: f.poliza,
        subConcepto: f.subConcepto,
      },
    };

    return this.http.post<ResponseAgrupaciones>(
      this.urlGroup,
      requestAgrupaciones
    );
    // return this.http.get<ResponseAgrupaciones>('./assets/agrupacion.json');
  }

  getFiles(): Observable<any[]> {
    // return this.http.get<any>('http://localhost:3000/archivos');
    return this.http.get<any[]>('./assets/database.json');
  }

  getContabilizacion(idAgrupacion: string) {
    const request = {
      Body: {
        id: idAgrupacion,
      },
    };
    return this.http.post<ResponseContabilizacion>(
      this.urlContabilizacion,
      request
    );
  }

  getReversa(idAgrupacion: string) {
    const request = {
      Body: {
        id: idAgrupacion,
      },
    };
    return this.http.post<ResponseReversa>(this.urlReversa, request);
  }

  getGuardarIdVoucher(idAgrupacion: string, idVoucher: string) {
    const request = {
      Body: {
        id: idAgrupacion,
        voucher: idVoucher,
      },
    };

    return this.http.post<ResponseContaIdVoucher>(
      this.urlContaIdVoucher,
      request
    );
  }

  getMediosPago() {
    const request = {
      Body: {
        type: 'medio de pago',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getPeriodoConciliacion() {
    const request = {
      Body: {
        type: 'periodo contable',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getPaymentGroups(f: Filtros) {
    const requestAgrupaciones = {
      Body: {
        socio: f.socio,
        compania: f.compania,
        mesCobertura: f.mesCobertura,
        periodoContable: f.periodoContable,
        estado: f.estado,
        concepto: 'PAGO',
        poliza: f.poliza,
        subconcepto: f.subConcepto,
      },
    };

    return this.http.post<ResponseAgrupaciones>(
      this.urlGroupConci,
      requestAgrupaciones
    );
  }

  getMesDeCarga() {
    const request = {
      Body: {
        type: 'mes de carga',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  getFlujos() {
    return this.http.get<string[]>(this.urlAuditoria + '/flujos');
  }

  getSubConceptos() {
    return this.http.get<string[]>(this.urlTransaccion + '/subconceptos');
  }

  // ***** para el componente fecha ******
  getEstados(lista: string) {
    return this.http.get<Estado[]>(this.urlEstados + '/estados/' + lista);
  }

  getEstadosAgrupacion() {
    return this.http.get<Estado[]>(this.urlEstados + '/estados/agrupacion');
  }

  getEstadosContabilizacion() {
    return this.http.get<Estado[]>(this.urlEstados + '/estados/contabilizacion');
  }

  getEstadosConciliacion() {
    return this.http.get<Estado[]>(this.urlEstados + '/estados/conciliacion');
  }

  // *********************************
  getCompanySun() {
    const request = {
      Body: {
        type: 'codigo compania',
        location: '',
      },
    };
    return this.http.post<ResponseFiltros>(this.urlFiltros, request);
  }

  // getPaymentGroups(f: Filtros) {
  //   const request = {
  //     Body: {
  //     socio: f.socio,
  //     compania: f.compania,
  //     mesCobertura: f.mesCobertura,
  //     periodoContable: f.periodoContable,
  //     estado: f.estado,
  //     concepto: 'PAGO',
  //     poliza: f.poliza,
  //     subconcepto: f.concepto
  //     }
  //   };

  //   return this.http.post<ResponseAgrupaciones>(this.urlGroupConci, request);
  // }
}
