import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Gastos } from '../model/gastos';
import { GastosLista } from '../model/gastos-lista';
import { environment } from '../../environments/environment';
import { Filtros } from '../model/filtros';
import { Fechas } from '../model/fechas';

@Injectable({
  providedIn: 'root',
})
export class GastosService {
  constructor(private http: HttpClient) {
    // this.http = http;
  }

  // url = 'http://localhost:7000/socios';
 // url = 'http://localhost:8080/CARWSCartola/gastos';
  // url = 'http://logicad.cardif.cl/CartolaBack/socios';
  url = environment.urlGastos;

  // private httpHeader = new HttpHeaders('Content-Type: application/json');

  getAll() {
    const request = {
      Body: {
        fechaInicio: '05/05/2020',
        fechaFin: '05/05/05',
        uso: '',
      },
    };
    return this.http.post<GastosLista>(this.url + '/get', request);
  }

  getAllConci(f: Filtros) {
    const request = {
      subconcepto: "",
      compania: "",
      socio: "",
      mesCobertura: ""
    };
    
    return this.http.post<GastosLista>(this.url + '/get_report', request);
    //return this.http.get<GastosLista>('./assets/gastoDummy.json');
  }

  save(p: Gastos) {
    return this.http.post<any>(this.url + '/add', p);
  }

  getGastosRango(f: Fechas) {
    const request = {
      Body: {
        fechaInicio: f.dateFrom,
        fechaFin: f.dateTo,
        uso: '',
      },
    };
    return this.http.post<GastosLista>(this.url + '/get', request);
  }
}
