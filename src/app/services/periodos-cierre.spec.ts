import { TestBed } from '@angular/core/testing';

import { PeriodosCierreService } from './periodos-cierre.service';

describe('SociosService', () => {
  let service: PeriodosCierreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeriodosCierreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
