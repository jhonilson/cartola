import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Socio } from '../model/socio';
import { ResponseSunsystem } from '../model/response-sunsystem';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SunsystemService {

  constructor(private http: HttpClient) {
   // this.http = http;
   }

  urlSunsystem = environment.urlSunsystem;
  // http://localhost:8080/entidades/entBySocio/1
  private httpHeader = new HttpHeaders('Content-Type: application/json');

  getSunsystem(numCuenta: string, socio: string, compania: string) {
    const request = {
      Body: {
        id: numCuenta,
        socio: socio,
        compania: compania 
      }
    };

    console.log("request sunsystem ", request);
    return this.http.post<ResponseSunsystem>(this.urlSunsystem, request);
    //return this.http.get<ResponseSunsystem>('./assets/sunsystemDummy.json');
  }

}
