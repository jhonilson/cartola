import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosConciliacionComponent } from './filtros-conciliacion.component';

describe('FiltrosConciliacionComponent', () => {
  let component: FiltrosConciliacionComponent;
  let fixture: ComponentFixture<FiltrosConciliacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosConciliacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosConciliacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
