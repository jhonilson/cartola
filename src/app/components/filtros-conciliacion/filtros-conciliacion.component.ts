import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FiltrosService } from 'src/app/services/filtros.service';

import { ResponseAgrupaciones } from 'src/app/model/response-agrupaciones';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Filtros } from 'src/app/model/filtros';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-filtros-conciliacion',
  templateUrl: './filtros-conciliacion.component.html',
  styleUrls: ['./filtros-conciliacion.component.scss']
})
export class FiltrosConciliacionComponent implements OnInit {
  @Input() compania = true;
  @Input() cobertura = true;
  @Input() socio = true;
  @Input() periodo = true;
  @Input() estado = true;
  @Input() concepto = true;
  @Input() subconcepto = true;
  @Input() poliza = true;
  @Input() anoContable = true;
  @Input() aprobacion = true;
  @Input() mesCarga = true;
  
  // botones
  @Input() callAgrupacion = false;
  @Input() callFiles = false;
  @Input() callConciliacion = false;
  @Input() callPrimas = false;
  @Input() callComisiones = false;
  @Input() callDevoluciones = false;
  @Input() callRecuperos = false;
  @Input() callGastos = false;

  @Output() public childEvent = new EventEmitter();
  @Output() socioEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() companiaEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() parentFun: EventEmitter<any> = new EventEmitter();
  @Output() listarAgrupaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarConciliaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarFacturaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarPrimas: EventEmitter<any> = new EventEmitter();
  @Output() listarComisiones: EventEmitter<any> = new EventEmitter();
  @Output() listarDevoluciones: EventEmitter<any> = new EventEmitter();
  @Output() listarRecuperos: EventEmitter<any> = new EventEmitter();
  @Output() listarGastos: EventEmitter<any> = new EventEmitter();


  @Output() filtros: EventEmitter<Filtros> = new EventEmitter();

  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];
  anosContable: any[];
  subConceptos: any[];
  mesesCarga: any[];

  items = [];
  pageOfItems: Array<any>;
  agrupaciones: any[];
  archivos: any[];
  conciliaciones: any[];
  p = 1;
  searchCompany: string;
  searchSocio: string;
  filas = GlobalConstants.filas;
  total = 0;

  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private filtroService: FiltrosService,
    private conciliaService: ConciliacionService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {
    // ---------------------
    this.filtroForm = new FormGroup({
      compania: new FormControl('', Validators.required),
      socio: new FormControl('', Validators.required),
      mesCobertura: new FormControl(''),
      poliza: new FormControl(''),
      periodoContable: new FormControl(''),
      estado: new FormControl(''),
      concepto: new FormControl(''),
      subConcepto: new FormControl(''),
      anoContable: new FormControl(''),
      aprobacion: new FormControl(''),
      mesCarga: new FormControl('')
    });

    // this.busquedaForm = new FormGroup({
    //   searchCompany: new FormControl(null),
    //   searchSocio: new FormControl(null)
    // });

    // ----------------
    this.listarCompanies();
    this.listarCoberturas();
    this.listarSocios();
    this.listarPeriodos();
    this.listarEstados();
    this.listarConceptos();
    this.listarSubConceptos();
    this.listarMesCarga();
    // this.listarAgrupacion();
  }

  public listarCompanies() {
    this.filtroService.getCompany().subscribe(
      (data: any) => {
        console.log('COMPAÑIAS:' + JSON.stringify(data.Body));
        this.companias = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarCoberturas() {
    this.filtroService.getMesCobertura().subscribe(
      (data: any) => {
        console.log('COBERTURAS:' + JSON.stringify(data.Body));
        this.mesesCobertura = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSocios() {
    this.filtroService.getSocio().subscribe(
      (data: any) => {
        console.log('SOCIOS:' + JSON.stringify(data.Body));
        this.socios = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarPeriodos() {
    this.filtroService.getPeriodoContable().subscribe(
      (data: any) => {
        console.log('PERIODOS:' + JSON.stringify(data.Body));
        this.periodosContable = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarEstados() {
    this.filtroService.getEstado().subscribe(
      (data: any) => {
        console.log('ESTADOS:' + JSON.stringify(data.Body));
        this.estados = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarConceptos() {
    this.filtroService.getConcepto().subscribe(
      (data: any) => {
        console.log('CONCEPTOS:' + JSON.stringify(data.Body));
        this.conceptos = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSubConceptos() {
    this.filtroService.getSubConcepto().subscribe(
      (data: any) => {
        console.log('SUBCONCEPTOS:' + JSON.stringify(data.Body));
        this.subConceptos = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarMesCarga() {
    this.filtroService.getMesDeCarga().subscribe(
      (data: any) => {
        console.log('MES DE CARGA:' + JSON.stringify(data.Body));
        this.mesesCarga = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public emitFiltro() {
    this.filtros.emit(this.filtroForm.value);
  }

  public setSocioEvent() {
    this.socioEvent.emit(this.filtroForm.value.socio);
  }

  public setCompaniaEvent() {
    this.companiaEvent.emit(this.filtroForm.value.compania);
  }

  // public listarAgrupacion() {
  //   console.log('FORMULARIO:' + JSON.stringify(this.filtroForm.value));
  //   // this.total = this.agrupaciones.length;
  //   if (this.filtroForm.valid) {

  //   this.filtroService.getGroup(this.filtroForm.value).subscribe(
  //     (data: any) => {
  //       console.log('AGRUPACIONES:' + JSON.stringify(data.Body));
  //       this.agrupaciones = data.Body.Groupings;
  //       this.childEvent.emit(this.agrupaciones);
  //       // alert('funciona:  ' + data.Body.Groupings.length);
  //       this.total = data.Body.Groupings.length;
  //       if (this.total === 0) {
  //         this.alerta.alertBasic('No hay registros para esta busqueda');
  //       }
  //     },
  //     (error) => {
  //       console.log('ERROR: ', error);
  //     }
  //   );

  // } else {
  //   this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
  // }

  // }

  // public getFiles() {
  //   console.log('FORMULARIO:' + JSON.stringify(this.filtroForm.value));
  //   this.filtroService.getFiles().subscribe(
  //     (data: any[]) => {
  //       console.log('archivos:', data);
  //       this.archivos = data;
  //       this.childEvent.emit(this.archivos);
  //       this.total = data.length;
  //       if (this.total === 0) {
  //         this.alerta.alertBasic('No hay registros para esta busqueda');
  //       }
  //     },
  //     (error) => {
  //       console.log('ERROR: ', error);
  //     }
  //   );
  // }

  // public listarConciliacion() {
  //   console.log('FORMULARIO:' + JSON.stringify(this.filtroForm.value));
  //   // this.total = this.agrupaciones.length;
  //   if (this.filtroForm.valid) {
  //   this.conciliaService.getAll(this.filtroForm.value).subscribe(
  //     (data: any) => {
  //       console.log('CONCILICIONES:' + JSON.stringify(data.Body));
  //       this.conciliaciones = data.Body;
  //       this.childEvent.emit(this.conciliaciones);
  //       // alert('funciona:  ' + data.Body);
  //       this.total = data.Body.length;
  //       if (this.total === 0) {
  //         this.alerta.alertBasic('No hay registros para esta busqueda');
  //       }
  //     },
  //     (error) => {
  //       console.log('ERROR: ', error);
  //     }
  //   );

  //   } else {
  //     this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
  //   }

  // }


  listarAgrupacion() {
    if (this.filtroForm.valid) {
      this.listarAgrupaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarConciliacion() {
    if (this.filtroForm.valid) {
      this.listarConciliaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarFacturacion() {
    if (this.filtroForm.valid) {
      this.listarFacturaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarPrima() {
    if (this.filtroForm.valid) {
      this.listarPrimas.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarComision() {
    if (this.filtroForm.valid) {
      this.listarComisiones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarDevolucion() {
    if (this.filtroForm.valid) {
      this.listarDevoluciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarRecupero() {
    if (this.filtroForm.valid) {
      this.listarRecuperos.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarGasto() {
    if (this.filtroForm.valid) {
      this.listarGastos.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  setValues(f: Filtros) {
    this.filtroForm.patchValue({
      compania: f.compania,
      socio: f.socio,
      mesCobertura: f.mesCobertura,
      periodoContable: f.periodoContable,
      poliza: f.poliza
    });
  }

  limpiar() {
    // this.filtroForm.reset();
    this.filtroForm.patchValue({
      compania: '',
      socio: '',
      mesCobertura: '',
      periodoContable: '',
      poliza: ''
    });
  }

  public prueba(){
    alert('funciona');
  }

}
