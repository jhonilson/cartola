import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConciliacionmodalComponent } from './conciliacionmodal.component';

describe('ConciliacionmodalComponent', () => {
  let component: ConciliacionmodalComponent;
  let fixture: ComponentFixture<ConciliacionmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConciliacionmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConciliacionmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
