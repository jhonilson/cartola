import { Component, OnInit, Input } from '@angular/core';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { AlertasService } from 'src/app/services/alertas.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-conciliacionmodal',
  templateUrl: './conciliacionmodal.component.html',
  styleUrls: ['./conciliacionmodal.component.scss']
})
export class ConciliacionmodalComponent implements OnInit {

  accion: any;
  reversa: string;

 @Input() detallesConciliacion: any[];
 @Input() Primaconciliacion: any[];
 @Input() Comisionconciliacion: any[];
 @Input() Devolucionconciliacion: any[];
 @Input() Recuperoconciliacion: any[];
 @Input() Gastosconciliacion: any[];
 @Input() Validacionesconciliacion: any[];
 @Input() Sunsystemconciliacion: any[];
 @Input() Netsuiteconciliacion: any[];
 @Input() muestraBotonera: boolean;

  constructor(
    private service: ConciliacionService,
    private alerta: AlertasService
  ) { }

  ngOnInit(): void {
    this.Primaconciliacion = [];
    this.Comisionconciliacion = [];
    this.Devolucionconciliacion = [];
    this.Recuperoconciliacion = [];
    this.Gastosconciliacion = [];
    this.Sunsystemconciliacion = [];
    this.Netsuiteconciliacion = [];
    console.log("Primaconciliacion " , this.Primaconciliacion);
  }

  public accionAprobar(id: string) {
    console.log('ID:' + id);

    Swal.fire({
      title: 'Usted aprobará la conciliación',
      text: '¿Está seguro que desea ejecutar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {

        this.service.aprobarConciliacion(id).subscribe(
          (data) => {
            console.log('Accion: ' + JSON.stringify(data));
            if(data.Status[0].code == "200"){
              this.accion = data.Body;
              this.alerta.alertBasic2( 'Conciliación Aprobada con Éxito!', 'success');
            }else{
              this.alerta.alertBasicIcon('Error', data.Body.mensaje, 'error');  
            }
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );
        
      }
    });

  }

  public accionRechazar(id: string) {
    console.log('ID:' + id);

    Swal.fire({
      title: 'Se va a Rechazar la Conciliación',
      text: '¿Está seguro que desea ejecutar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {

        this.service.rechazarConciliacion(id).subscribe(
          (data) => {
            console.log('Accion: ' + JSON.stringify(data));
            if(data.Status[0].code == "200"){
              this.accion = data.Body;
              this.alerta.alertBasic2(data.Body.mensaje, 'success');
            }else{
              this.alerta.alertBasicIcon('Error', data.Body.mensaje, 'error');  
            }
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );

      }
    });

  }

  public accionReversar(id: string) {
    console.log('ID:' + id);

    Swal.fire({
      title: 'Se va a Reversar la Conciliación',
      text: '¿Está seguro que desea ejecutar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {

        this.service.reversarConciliacion(id, this.reversa).subscribe(
          (data) => {
            if(data.Status[0].code == "200"){
              console.log('Accion: ' + JSON.stringify(data));
              this.accion = data.Body;
              this.alerta.alertBasic2( 'Conciliacion Reversada con Exito!', 'success');
            }else{
              this.alerta.alertBasicIcon('Error', data.Body.mensaje, 'error');  
            }
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );
        
      }
    });

  }

  public changeVariable(e) {
    this.reversa = e.target.value;
  }

}
