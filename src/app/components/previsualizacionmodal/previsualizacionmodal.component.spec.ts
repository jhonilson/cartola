import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevisualizacionmodalComponent } from './previsualizacionmodal.component';

describe('PrevisualizacionmodalComponent', () => {
  let component: PrevisualizacionmodalComponent;
  let fixture: ComponentFixture<PrevisualizacionmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrevisualizacionmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrevisualizacionmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
