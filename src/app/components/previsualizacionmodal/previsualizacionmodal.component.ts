import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { AlertasService } from 'src/app/services/alertas.service';

@Component({
  selector: 'app-previsualizacionmodal',
  templateUrl: './previsualizacionmodal.component.html',
  styleUrls: ['./previsualizacionmodal.component.scss']
})
export class PrevisualizacionmodalComponent implements OnInit {


  accion: any;

 @Input() detallesConciliacion: any[];
 @Input() Primaconciliacion: any[];
 @Input() Comisionconciliacion: any[];
 @Input() Devolucionconciliacion: any[];
 @Input() Recuperoconciliacion: any[];
 @Input() Gastosconciliacion: any[];
 @Input() Validacionesconciliacion: any[];
 @Input() Sunsystemconciliacion: any[];
 @Input() Netsuiteconciliacion: any[];

 @Output() parentFun: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private service: ConciliacionService,
    private alerta: AlertasService
  ) { }

  ngOnInit(): void {
  }

  public accionAprobar(id: string) {
    console.log('ID:' + id);
    this.service.aprobarConciliacion(id).subscribe(
      (data) => {
        console.log('Accion: ' + JSON.stringify(data));
        this.accion = data.Body;
        this.alerta.alertBasic2( 'Conciliacion Aprobada con Exito!', 'success');
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public accionRechazar(id: string) {
    console.log('ID:' + id);
    this.service.rechazarConciliacion(id).subscribe(
      (data) => {
        console.log('Accion: ' + JSON.stringify(data));
        this.accion = data.Body;
        this.alerta.alertBasic2( 'Conciliacion Rechazada!', 'success');
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public accionReversar(id: string) {
    console.log('ID:' + id);
    this.service.rechazarConciliacion(id).subscribe(
      (data) => {
        console.log('Accion: ' + JSON.stringify(data));
        this.accion = data.Body;
        this.alerta.alertBasic2( 'Conciliacion Rechazada!', 'success');
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }


}
