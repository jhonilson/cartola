import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FiltrosService } from 'src/app/services/filtros.service';

import { ResponseAgrupaciones } from 'src/app/model/response-agrupaciones';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Filtros } from 'src/app/model/filtros';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.scss'],
})
export class FiltrosComponent implements OnInit {
  @Input() compania = true;
  @Input() cobertura = true;
  @Input() socio = true;
  @Input() periodo = true;
  @Input() estado = true;
  @Input() concepto = true;
  @Input() subconcepto = true;
  @Input() poliza = true;
  @Input() anoContable = true;
  @Input() aprobacion = true;
  @Input() estadoAgrupacion = false;

  // botones
  @Input() callAgrupacion = false;
  @Input() callFiles = false;
  @Input() callConciliacion = false;

  @Output() public childEvent = new EventEmitter();
  @Output() parentFun: EventEmitter<any> = new EventEmitter();
  @Output() listarAgrupaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarConciliaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarFacturaciones: EventEmitter<any> = new EventEmitter();

  @Output() filtros: EventEmitter<Filtros> = new EventEmitter();

  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];
  anosContable: any[];
  subConceptos: any[];
  estadosAgrupacion: any[];

  items = [];
  pageOfItems: Array<any>;
/*   agrupaciones: any[];
  archivos: any[];
  conciliaciones: any[]; */
  p = 1;
  searchCompany: string;
  searchSocio: string;
  filas = GlobalConstants.filas;
  total = 0;

  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private filtroService: FiltrosService,
    private conciliaService: ConciliacionService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {
    // ---------------------
    this.filtroForm = new FormGroup({
      compania: new FormControl('', Validators.required),
      socio: new FormControl('', Validators.required),
      mesCobertura: new FormControl(''),
      poliza: new FormControl(''),
      periodoContable: new FormControl(''),
      estado: new FormControl(''),
      concepto: new FormControl(''),
      subConcepto: new FormControl(''),
      anoContable: new FormControl(''),
      aprobacion: new FormControl(''),
      estadoAgrupacion: new FormControl('')
    });

    // this.busquedaForm = new FormGroup({
    //   searchCompany: new FormControl(null),
    //   searchSocio: new FormControl(null)
    // });

    // ----------------
    this.listarCompanies();
    this.listarCoberturas();
    this.listarSocios();
    this.listarPeriodos();
    this.listarEstados();
    this.listarConceptos();
    this.listarSubConceptos();
    this.listarEstadosAgrupacion();
    // this.listarAgrupacion();
  }

  public listarCompanies() {
    this.filtroService.getCompany().subscribe(
      (data: any) => {
        console.log('COMPAÑIAS:' + JSON.stringify(data.Body));
        this.companias = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarCoberturas() {
    this.filtroService.getMesCobertura().subscribe(
      (data: any) => {
        console.log('COBERTURAS:' + JSON.stringify(data.Body));
        this.mesesCobertura = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSocios() {
    this.filtroService.getSocio().subscribe(
      (data: any) => {
        console.log('SOCIOS:' + JSON.stringify(data.Body));
        this.socios = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarPeriodos() {
    this.filtroService.getPeriodoContable().subscribe(
      (data: any) => {
        console.log('PERIODOS:' + JSON.stringify(data.Body));
        this.periodosContable = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarEstados() {
    this.filtroService.getEstado().subscribe(
      (data: any) => {
        console.log('ESTADOS:' + JSON.stringify(data.Body));
        this.estados = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarEstadosAgrupacion() {
    this.filtroService.getEstadoAgrupacion().subscribe(
      (data: any) => {
        console.log('ESTADOS AGRUPACION:' + JSON.stringify(data.Body));
        this.estadosAgrupacion = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarConceptos() {
    this.filtroService.getConcepto().subscribe(
      (data: any) => {
        console.log('CONCEPTOS:' + JSON.stringify(data.Body));
        this.conceptos = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSubConceptos() {
    this.filtroService.getSubConcepto().subscribe(
      (data: any) => {
        console.log('SUBCONCEPTOS:' + JSON.stringify(data.Body));
        this.subConceptos = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  listarAgrupacion() {
    if (this.filtroForm.valid) {
      this.listarAgrupaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarConciliacion() {
    if (this.filtroForm.valid) {
      this.listarConciliaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

  listarFacturacion() {
    if (this.filtroForm.valid) {
      this.listarFacturaciones.emit(this.filtroForm.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son obligatorios!', 'error');
    }
  }

}
