import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AlertasService } from 'src/app/services/alertas.service';
import { Fechas } from 'src/app/model/fechas';
import { FiltrosService } from 'src/app/services/filtros.service';
import { loadCldr, L10n } from '@syncfusion/ej2-base';
// Here we have referred local json files for preview purpose

declare var require: any;

loadCldr(
  require('cldr-data/main/es/numbers.json'),
  require('cldr-data/main/es/ca-gregorian.json'),
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/es/timeZoneNames.json'),
  require('cldr-data/supplemental/weekdata.json') // To load the culture based first day of week
);

@Component({
  selector: 'app-fechas',
  templateUrl: './fechas.component.html',
  styleUrls: ['./fechas.component.scss'],
})
export class FechasComponent implements OnInit {

  @Input() callAuditoria = false;
  @Input() callExtraccion = false;
  @Input() callTransaccion = false;
  @Input() callContabilizacion = false;
  @Input() callAgrupacion = false;
  @Input() callConciliacion = false;
  @Input() callGastos = false;
  @Input() flujo = true;
  @Input() subconcepto = true;
  @Input() estado = true;
  @Input() listaEstado: string ;

  @Output() public childEvent = new EventEmitter();
  @Output() filtros: EventEmitter<Fechas> = new EventEmitter();
  @Output() listarAuditorias: EventEmitter<any> = new EventEmitter();
  @Output() listarLotes: EventEmitter<any> = new EventEmitter();
  @Output() listarTransacciones: EventEmitter<any> = new EventEmitter();
  @Output() listarContabilizaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarAgrupaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarConciliaciones: EventEmitter<any> = new EventEmitter();
  @Output() listarGastos: EventEmitter<any> = new EventEmitter();
  
  public cssClass = 'e-custom-style';
  public month: number = new Date().getMonth();
  public fullYear: number = new Date().getFullYear();
  // public minDate: Date = new Date();
  // public minDate: Date = new Date(this.fullYear, this.month , 22, 12);
  // public maxDate: Date = new Date(this.fullYear, this.month, 25, 17);
  // public dateValue: Date = new Date();

  // *******************
  public today: Date = new Date();
  public currentYear: number = this.today.getFullYear();
  public currentMonth: number = this.today.getMonth();
  public currentDay: number = this.today.getDate();
  // public dateValue1: Object = new Date(new Date().setDate(14));
  public minDate: Object = new Date(this.currentYear, this.currentMonth, 1);
  public maxDate: Object = new Date();

  form: FormGroup;

  flujos: any[];
  subconceptos: any[];
  estados: any[];

  constructor(
    private fb: FormBuilder,
    private alerta: AlertasService,
    private filtroService: FiltrosService
    ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.listarFlujos();
    this.listarSubConceptos();
    this.listarEstados();

    L10n.load({
      es: {
      datepicker: {
        placeholder: 'Elige una fecha',
        today:'Hoy'
        }
      }
    });

  }

  createForm() {
    this.form = this.fb.group(
      {
        dateTo: new FormControl('', [Validators.required]),
        dateFrom: new FormControl('', [Validators.required]),
        flujo: new FormControl(''),
        subconcepto: new FormControl('' ),
        estado: new FormControl('' ),
      },
      { validator: this.dateLessThan('dateFrom', 'dateTo') }
    );
  }

  public listarFlujos() {
    this.filtroService.getFlujos().subscribe(
      (data: any) => {
        console.log('FLUJOS:' + JSON.stringify(data));
        this.flujos = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSubConceptos() {
    this.filtroService.getSubConceptos().subscribe(
      (data: any) => {
        console.log('SUBCONCEPTOS:' + JSON.stringify(data));
        this.subconceptos = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarEstados() {
    this.filtroService.getEstados(this.listaEstado).subscribe(
      (data: any) => {
        console.log('ESTADOS:' + JSON.stringify(data));
        this.estados = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
     // alert(f.value + ' - ' + t.value);
      if (f.value > t.value) {
        return {
          dates: 'Fecha Desde debe ser menor que Fecha Hasta',
        };
      }
      return {};
    };
  }

  onSubmit() {
    if (this.form.valid) {
      console.log('Probando');
      console.log('FORM: ' + this.form);
      console.log('FORM-VALUE: ' + JSON.stringify(this.form.value));
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario', 'error');
    }
  }


  listarAuditoriaByRango() {
    if (this.form.valid) {
      this.listarAuditorias.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarLoteByRango() {
    if (this.form.valid) {
      this.listarLotes.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarTransaccionByRango() {
    if (this.form.valid) {
      this.listarTransacciones.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarContabilizacionByRango() {
    if (this.form.valid) {
      this.listarContabilizaciones.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarAgrupacionByRango() {
    if (this.form.valid) {
      this.listarAgrupaciones.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarConciliacionByRango() {
    if (this.form.valid) {
      this.listarConciliaciones.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

  listarGastoByRango() {
    if (this.form.valid) {
      this.listarGastos.emit(this.form.value);
    } else {
      this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
    }
  }

}
