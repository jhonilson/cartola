import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { AppService } from '../utils/services/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  // public user = {
  //   firstName: 'Alexander',
  //   lastName: 'Pierce',
  //   image: 'assets/img/user2-160x160.jpg'
  // };

  invalidLogin: boolean;
  // userPattern = '^[a-zA-Z0-9]{4,}$';
  // userPattern = '[A-z0-9]$';
  userPattern = '^[a-zA-Z0-9]{4,}$';

  public loginForm: FormGroup;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private appService: AppService,
    private router: Router
  ) {
    if (this.appService.loggedIn()) {
      this.router.navigate(['/main']);
  }
  }

  ngOnInit() {
    this.renderer.addClass(document.body, 'login-page');
    this.loginForm = new FormGroup({
      usuario: new FormControl('', [Validators.required, Validators.maxLength(8), Validators.pattern(this.userPattern)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(15)])
    });
  }

  logIn() {
    if (this.loginForm.valid) { 
      sessionStorage.setItem('usuario', this.loginForm.controls.usuario.value);
      this.appService.login();
    } else {
      this.toastr.error('Login Invalido', 'Login invalido!');
    }
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'login-page');
  }

}
