import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteConciliacion2Component } from './reporte-conciliacion2.component';

describe('ReporteConciliacion2Component', () => {
  let component: ReporteConciliacion2Component;
  let fixture: ComponentFixture<ReporteConciliacion2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteConciliacion2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteConciliacion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
