import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertasService } from 'src/app/services/alertas.service';
import { Fechas } from 'src/app/model/fechas';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { ConciliacionRango } from 'src/app/model/conciliacion-rango';
import * as xlsx from 'xlsx';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-conciliacion2',
  templateUrl: './reporte-conciliacion2.component.html',
  styleUrls: ['./reporte-conciliacion2.component.scss']
})
export class ReporteConciliacion2Component implements OnInit {

  @ViewChild('agrutable', { static: false }) agrutable: ElementRef;

  conciliaciones: ConciliacionRango[];

  p = 1;
  searchCompany: string;
  searchSocio: string;
  tipo: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados='conciliacion';

  public busquedaForm: FormGroup;

  constructor(
    private service: ConciliacionService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(''),
      searchSocio: new FormControl(''),
      tipo: new FormControl('')
    });

    this.listarConciliaciones();
  }

  public listarConciliaciones() {
    this.service.getAllConciliacion().subscribe(
      (data: any) => {
        console.log('conciliaciones:' + JSON.stringify(data));
        this.conciliaciones = data;
       // this.contaDetalle = data.detalle;
        if(data.length > 0 ){
          this.conciliaciones = data;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarConciRango(f: Fechas) {
    this.service.getConciliacionByRango(f).subscribe(
      (data: any) => {
        console.log('conciliaciones:' + JSON.stringify(data));
        this.conciliaciones = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.agrutable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'conciliaciones.xlsx');
   }


}
