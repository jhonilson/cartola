import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AuditoriaService } from 'src/app/services/auditoria.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as xlsx from 'xlsx';
import { Fechas } from 'src/app/model/fechas';
import { AlertasService } from 'src/app/services/alertas.service';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-auditoria',
  templateUrl: './reporte-auditoria.component.html',
  styleUrls: ['./reporte-auditoria.component.scss'],
})
export class ReporteAuditoriaComponent implements OnInit {
  @ViewChild('auditable', { static: false }) auditable: ElementRef;

  auditorias: any[];
  p = 1;
  searchFechaIni: string;
  searchFechaFin: string;
  estatus: string;
  flujo: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados = '';
  // formFiltros: Fechas;

  public busquedaForm: FormGroup;

  constructor(
    private service: AuditoriaService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchFechaIni: new FormControl(''),
      searchFechaFin: new FormControl(''),
      estatus: new FormControl(''),
      flujo: new FormControl('')
    });

    // this.listarAuditoria();
  }

  public listarAuditoria() {
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('auditorias:' + JSON.stringify(data));
        this.auditorias = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarAuditoriaRango(f: Fechas) {
    console.log('fechas: ' + JSON.stringify(f));
    this.service.getRango(f).subscribe(
      (data: any) => {
        this.auditorias = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
        console.log('auditoriasRango:' + JSON.stringify(this.auditorias));
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.auditable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'auditoria.xlsx');
   }

}
