import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAgrupacion2Component } from './reporte-agrupacion2.component';

describe('ReporteAgrupacion2Component', () => {
  let component: ReporteAgrupacion2Component;
  let fixture: ComponentFixture<ReporteAgrupacion2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteAgrupacion2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAgrupacion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
