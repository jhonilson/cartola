import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ContabilizacionDetalles } from 'src/app/model/contabilizacion-detalles';
import { FormGroup, FormControl } from '@angular/forms';
import { ContabilizacionService } from 'src/app/services/contabilizacion.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Fechas } from 'src/app/model/fechas';
import * as xlsx from 'xlsx';
import { Agrupacion } from 'src/app/model/agrupacion';
import { AgrupacionesService } from 'src/app/services/agrupaciones.service';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-agrupacion2',
  templateUrl: './reporte-agrupacion2.component.html',
  styleUrls: ['./reporte-agrupacion2.component.scss']
})
export class ReporteAgrupacion2Component implements OnInit {

  @ViewChild('agrutable', { static: false }) agrutable: ElementRef;

  agrupaciones: Agrupacion[];

  p = 1;
  searchCompany: string;
  searchSocio: string;
  tipo: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados = 'agrupacion';

  public busquedaForm: FormGroup;

  constructor(
    private service: AgrupacionesService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(''),
      searchSocio: new FormControl(''),
      tipo: new FormControl('')
    });

    this.listarAgrupaciones();
  }

  public listarAgrupaciones() {
    this.service.getAllAgrupacion().subscribe(
      (data: any) => {
        console.log('agrupaciones:' + JSON.stringify(data));
        this.agrupaciones = data;
       // this.contaDetalle = data.detalle;
        if(data.length > 0 ){
          this.agrupaciones = data;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarAgrupRango(f: Fechas) {
    this.service.getAgrupacionByRango(f).subscribe(
      (data: any) => {
        console.log('lotes:' + JSON.stringify(data));
        this.agrupaciones = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.agrutable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'agrupaciones.xlsx');
   }


}
