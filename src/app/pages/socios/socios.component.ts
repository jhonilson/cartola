import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Socio } from 'src/app/model/socio';
import { SociosService } from 'src/app/services/socios.service';
import { Router } from '@angular/router';
import { EntidadService } from 'src/app/services/entidad.service';
import { Entidad } from 'src/app/model/entidad';

@Component({
  selector: 'app-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.scss']
})
export class SociosComponent implements OnInit {

  selected: any = '';
  public socios: Socio[];
  public entidades: Entidad[];
  public socioForm: FormGroup;

  constructor(
    private socioService: SociosService,
    private entidadService: EntidadService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.socioForm = new FormGroup({
      company: new FormControl('', Validators.required)
    });

    this.listarSocios();
  }

  public listarSocios() {
    this.socioService.getAll().subscribe( (data: any) => {
      console.log('socios:' + JSON.stringify(data) );
      this.socios = data;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarEntidadBySocio(id: number) {
    this.entidadService.findById(id).subscribe(
      (data: any) => {
        console.log(data);
        this.entidades = data;
      }, error => {
        console.log('ERROR: ', error);
      });
  }

  changeSuit(e: any) {
    this.socioForm.get('company').setValue(e, {
       onlySelf: true
    });
    alert(e);
  }

}
