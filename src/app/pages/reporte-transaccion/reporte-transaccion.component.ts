import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as xlsx from 'xlsx';
import { TransaccionService } from 'src/app/services/transaccion.service';
import { Fechas } from 'src/app/model/fechas';
import { AlertasService } from 'src/app/services/alertas.service';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-transaccion',
  templateUrl: './reporte-transaccion.component.html',
  styleUrls: ['./reporte-transaccion.component.scss']
})
export class ReporteTransaccionComponent implements OnInit {

  @ViewChild('transtable', { static: false }) transtable: ElementRef;

  transacciones: any[];
  p = 1;
  searchFechaIni: string;
  searchFechaFin: string;
  subConcepto: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados = '';

  public busquedaForm: FormGroup;

  constructor(
    private service: TransaccionService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchFechaIni: new FormControl(''),
      searchFechaFin: new FormControl(''),
      subConcepto: new FormControl('')
    });

   // this.listarTransaccion();
  }

  public listarTransaccion() {
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('transacciones:' + JSON.stringify(data));
        this.transacciones = data;
        if(data.length > 0 ){
          this.transacciones = data;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  
  public listarTransaccionRango(f: Fechas) {
    console.log('fechas: ' + JSON.stringify(f));
    this.service.getRango(f).subscribe(
      (data: any) => {
        this.transacciones = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
        console.log('TRANSACCIONESRango:' + JSON.stringify(this.transacciones));
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.transtable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'transacciones.xlsx');
   }

}
