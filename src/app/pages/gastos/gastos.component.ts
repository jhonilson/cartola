import { Component, OnInit, ViewChild } from '@angular/core';
import { Gastos } from 'src/app/model/gastos';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertasService } from 'src/app/services/alertas.service';
import { GastosService } from 'src/app/services/gastos.service';
import { DatePipe } from '@angular/common';
import { GastosLista } from 'src/app/model/gastos-lista';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';
import { GlobalConstants } from 'src/app/model/global-constants';
@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html',
  styleUrls: ['./gastos.component.scss']
})
export class GastosComponent implements OnInit {


  @ViewChild('csvReader') csvReader: any;
  public records: any[] = [];
  public headersRow: any[] = [];
  public gastos: Gastos[];
  public allGastos: GastosLista[];

  countRegister = 0;
  resume: string;
  term: string;
  concepto: string;
  subconcepto: string;
  lista = 'records';

  p = 1;
  filas = GlobalConstants.filas;
  total = 0;

  todayNumber: number = Date.now();
  todayDate: Date = new Date();
  todayString: string = new Date().toDateString();
  todayISOString: string = new Date().toISOString();

  usuario: string = sessionStorage.getItem('usuario');
  fechaCarga: string;
  nombreArchivo: string;
  cantidadRegistros: number;

  spinner: boolean;
  spinnerConfig: ISpinnerConfig;

  public busquedaForm: FormGroup;

  constructor(
    private alerta: AlertasService,
    private service: GastosService
    ) {
    const datePipe = new DatePipe('en-US');
    this.fechaCarga = datePipe.transform(this.todayISOString, 'dd/MM/yyyy');
  }

  ngOnInit(): void {

    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: '6rem',
      color: '#1dab5a',
      bgColor: 'rgba(255,255,255,0.8)'
    };

    this.busquedaForm = new FormGroup({
      concepto: new FormControl(null),
      subconcepto: new FormControl(null)
    });

    this.get();
  }


  uploadListener($event: any): void {
    // let texto = [];
    const files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {
      const input = $event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      this.nombreArchivo = input.files[0].name;
      console.log('archivo: ' + this.nombreArchivo );
      reader.onload = () => {
        const csvData = reader.result;
       // let csvRecordsArray = (<string> csvData).split(/\r\n|\n/);
        const csvRecordsArray = (csvData as string).split(/\r\n|\n/);

        // imprime los titulos de las columnas
        this.headersRow = this.getHeaderArray(csvRecordsArray);
        console.log(this.headersRow);
        console.log('lenght: ' + this.headersRow.length);
        // validando cantidad de columnas
        if (this.headersRow.length === 15) {
        } else {
         // alert('numero de columnas incorrecto');
         // this.alertService.basic('numero de columnas incorrecto');
         this.spinner = false;
         this.alerta.alertBasicIcon('Error', 'Numero de columnas incorrectos.', 'error');
        }
        this.spinner = true;
        this.records = this.getDataRecordsArrayFromCSVFile(
          csvRecordsArray,
          this.headersRow.length
        );
        console.log('records: ' + JSON.stringify(this.records));
        this.gastos = this.records;
        this.addGastos(this.gastos);
      };

      
      // tslint:disable-next-line:only-arrow-functions
      reader.onerror = function() {
        console.log('error is occured while reading file!');
      };
    } else {
      this.spinner = false;
      alert('La extension del archivo es incorrecta.');
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    const csvArr = [];
    this.cantidadRegistros = csvRecordsArray.length - 1;
    console.log('registros: ' + csvRecordsArray.length);

    for (let i = 1; i < csvRecordsArray.length; i++) {
     // let curruntRecord = (<string> csvRecordsArray[i]).split(';');
      const curruntRecord = (csvRecordsArray[i] as string).split(';');
      console.log('curruntRecord: ' + curruntRecord);
      console.log('columnas:' + curruntRecord.length);
      if (curruntRecord.length === headerLength) {
        // let csvRecord: CSVRecord = new CSVRecord();
        // csvRecord.id = curruntRecord[0].trim();
        // csvRecord.firstName = curruntRecord[1].trim();
        // csvRecord.lastName = curruntRecord[2].trim();
        // csvRecord.age = curruntRecord[3].trim();
        // csvRecord.position = curruntRecord[4].trim();
        // csvRecord.mobile = curruntRecord[5].trim();
        // csvArr.push(csvRecord);
         const gasto: any = new Gastos();

         console.log('current: ' + curruntRecord[0].trim());

       // gasto.push({subconcepto : curruntRecord[0].trim()});
         gasto.subconcepto = curruntRecord[0].trim();
         gasto.operatoria = curruntRecord[1].trim();
         gasto.codigo = curruntRecord[2].trim();
         gasto.codigoDesc = curruntRecord[3].trim();
         gasto.compania = curruntRecord[4].trim();
         gasto.socio = curruntRecord[5].trim();
         gasto.policy = curruntRecord[6].trim();
         gasto.concepto = curruntRecord[7].trim();
         gasto.idProducto = 1;
         gasto.mesCobertura = curruntRecord[9].trim();
         gasto.primaAsociada = curruntRecord[10].trim();
         gasto.montoFinal = curruntRecord[11].trim();
         gasto.ivaAfecto = curruntRecord[12].trim();
         gasto.ivaExento = curruntRecord[13].trim();
         gasto.porcentaje = curruntRecord[14].trim();
         gasto.idPrdt = curruntRecord[8].trim();

         gasto.cantidadRegistros = this.cantidadRegistros;
         gasto.nombreArchivo = this.nombreArchivo;
         gasto.usuario = sessionStorage.getItem('usuario');
         gasto.fechaCargArch = this.fechaCarga;

         csvArr.push(gasto);

      } else if (curruntRecord.length < headerLength ) {
        this.countRegister = this.countRegister + 1;
      }
    }
    // this.alertService.alertBasic('No se mostraran: ' + this.countRegister + 'no seran cargados debidos ');
   // this.alertService.alertBasicIcon(this.countRegister + 'registros', 'no se cargaran debido poseer mas de 1 columnas vacia', 'warning');
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith('.csv');
  }

  getHeaderArray(csvRecordsArr: any) {
    const headers = (csvRecordsArr[0] as string).split(';');
    const headerArray = [];
    // tslint:disable-next-line:prefer-for-of
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = '';
    this.records = [];
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  public addGastos(g: any) {
    this.service.save(g).subscribe(
        (data) => {
          console.log('DATA:' + JSON.stringify(data));
          // alert('agregado con exito!');
          this.spinner = false;
          this.alerta.alertBasic2('Guardado con exito!', 'success');
        },
        (error) => {
          console.log('ERROR: ', error);
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );
  }

  public get() {
    // alert('funciona');
    this.spinner = true;
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('archivos:', data.Body);
        this.allGastos = data.Body;
        this.spinner = false;
      },
      (error) => {
        this.spinner = false;
        console.log('ERROR: ', error);
      }
    );
  }

}
