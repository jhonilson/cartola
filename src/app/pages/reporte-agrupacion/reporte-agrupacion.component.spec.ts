import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReporteAgrupacionComponent } from './reporte-agrupacion.component';


describe('ReporteAgrupacionesComponent', () => {
  let component: ReporteAgrupacionComponent;
  let fixture: ComponentFixture<ReporteAgrupacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteAgrupacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAgrupacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
