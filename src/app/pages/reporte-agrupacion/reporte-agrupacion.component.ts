import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FiltrosService } from 'src/app/services/filtros.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import * as xlsx from 'xlsx';
import { AgrupacionesService } from 'src/app/services/agrupaciones.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DetalleAgrupacion } from 'src/app/model/detalle-agrupacion';
import { Filtros } from 'src/app/model/filtros';
import { GlobalConstants } from 'src/app/model/global-constants';
import { AlertasService } from 'src/app/services/alertas.service';


@Component({
  selector: 'app-tabla2',
  templateUrl: './reporte-agrupacion.component.html',
  styleUrls: ['./reporte-agrupacion.component.scss']
})
export class ReporteAgrupacionComponent implements OnInit {

  @ViewChild('agruptable', { static: false }) agruptable: ElementRef;

  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];

  items = [];
  pageOfItems: Array<any>;
  agrupaciones: any[];
  detalles: DetalleAgrupacion[];
  // detallesExcel: DetalleAgrupacionExcel[];
  p = 1;
  searchCompany: string;
  searchSocio: string;
  filas = GlobalConstants.filas;
  total = 0;

  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private filtroService: FiltrosService,
    private agrupService: AgrupacionesService,
    private alerta: AlertasService
  ) {

  }

  ngOnInit(): void {

    // ---------------------
    // this.filtroForm = new FormGroup({
    //   compania: new FormControl(null, Validators.required),
    //   socio: new FormControl(null, Validators.required),
    //   mesCobertura: new FormControl(),
    //   poliza: new FormControl(),
    //   periodoContable: new FormControl(),
    //   estado: new FormControl(),
    //   concepto: new FormControl()
    // });

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(null),
      searchSocio: new FormControl(null)
    });

    // ----------------
    this.listarCompanies();
    this.listarCoberturas();
    this.listarSocios();
    this.listarPeriodos();
    this.listarEstados();
    this.listarConceptos();
   // this.listarAgrupacion();

  }


  public listarCompanies() {
    this.filtroService.getCompany().subscribe( (data: any) => {
      console.log('COMPAÑIAS:' + JSON.stringify(data.Body) );
      this.companias = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarCoberturas() {
    this.filtroService.getMesCobertura().subscribe( (data: any) => {
      console.log('COBERTURAS:' + JSON.stringify(data.Body) );
      this.mesesCobertura = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarSocios() {
    this.filtroService.getSocio().subscribe( (data: any) => {
      console.log('SOCIOS:' + JSON.stringify(data.Body) );
      this.socios = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarPeriodos() {
    this.filtroService.getPeriodoContable().subscribe( (data: any) => {
      console.log('PERIODOS:' + JSON.stringify(data.Body) );
      this.periodosContable = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarEstados() {
    this.filtroService.getEstado().subscribe( (data: any) => {
      console.log('ESTADOS:' + JSON.stringify(data.Body) );
      this.estados = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarConceptos() {
    this.filtroService.getConcepto().subscribe( (data: any) => {
      console.log('CONCEPTOS:' + JSON.stringify(data.Body) );
      this.conceptos = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarAgrupacion(f: Filtros) {
    console.log('FORMULARIO:' + JSON.stringify(f));
    // this.total = this.agrupaciones.length;
    this.agrupService.getAll(f).subscribe( (data: any) => {
      console.log('AGRUPACIONES:' + JSON.stringify(data.Body) );
      // this.agrupaciones = data.Body.Groupings;
     // alert('funciona:  ' + data.Body.Groupings.length);
     console.log("data.Body.Groupings.length " , data.Body.Groupings.length);
      this.total = data.Body.Groupings.length;
      console.log("this.total " , this.total);
      if(data.Body.Groupings.length > 0){
        this.agrupaciones = data.Body.Groupings;
      }else{
        this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
      }
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public detalleAgrupacion(id: string) {
    console.log('ID: ' + id);
    this.agrupService.getDetalles(id).subscribe((data: any) => {
      console.log('DETTALES: ' +  JSON.stringify(data.Body));
      this.detalles = data.Body;
      this.directToExcel(id);
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  pageChanged(event: any) {

  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.agruptable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'agruptable.xlsx');
   }

   public directToExcel(id: string) {
    if (typeof this.detalles !== 'undefined') {
      const ws: xlsx.WorkSheet =
      xlsx.utils.json_to_sheet(this.detalles);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
      xlsx.writeFile(wb, 'detalleAgrupacion.xlsx');
    }
   }

}

