import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FiltrosService } from 'src/app/services/filtros.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { stringify } from 'querystring';
import { Validaciones } from 'src/app/model/validaciones';
import { ValidacionesService } from 'src/app/services/validaciones.service';
import { nonWhiteSpace } from 'html2canvas/dist/types/css/syntax/parser';
import { CuentasService } from 'src/app/services/cuentas.service';
import { SunsystemService } from 'src/app/services/sunsystem.service';
import { NetsuiteService } from 'src/app/services/netsuite.service';
import { GastosService } from 'src/app/services/gastos.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Recuperos } from 'src/app/model/recuperos';
import { RecuperosService } from 'src/app/services/recuperos.service';
import { TotalProduccion } from 'src/app/model/total-produccion';
import { TotalRespaldo } from 'src/app/model/total-respaldo';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { Filtros } from 'src/app/model/filtros';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';
import { FiltrosConciliacionComponent } from 'src/app/components/filtros-conciliacion/filtros-conciliacion.component';

@Component({
  selector: 'app-conciliacion',
  templateUrl: './conciliacion.component.html',
  styleUrls: ['./conciliacion.component.scss']
})
export class ConciliacionComponent implements OnInit {

  @Input() Netsuiteconciliacion: any[];
  @ViewChild(FiltrosConciliacionComponent) child: FiltrosConciliacionComponent;

  socioGral: string = "";
  companiaGral: string = "";
  formFiltros: Filtros;
  companiasSun: any[];
  cuentasSocio: any[] = [];
  periodosConciliacion: any[];
  mediosPago: any[];
  validaCombinaciones: any[] = [];
  validaConciliacion: any[] = [];
  validaGastos: any[] = [];
  arrValidacion: any[] = [];
  esCombiacionValida: boolean= false;
  configCombinacion: any;
  //json = {"conceptos": [{"nombre": "prima"}, {"nombre": "comision"}], "tipoCuentas": [{"nombre": "CPD"}, {"nombre": "FOA"}]};
  //json2 = '"{"conceptos": [{"nombre": "prima"}, {"nombre": "comision"}], "tipoCuentas": [{"nombre": "CPD"}, {"nombre": "FOA"}]}"';
  
  validaCuentas: any[];
  dataPrimas: any[];
  dataComisiones: any[];
  dataDevoluciones: any[];
  dataRecuperos: any[];
  dataGastos: any[];

  dataSunsystem: any[];
  dataNetsuite: any[];
  p = 1;
  filas = 10;
  filasC = 5;

  pPrima = 0;
  pPrimaC = 0;
  filasPrima = this.filas;
  filasPrimaC = this.filasC;

  pComision = 0;
  pComisionC = 0;
  filasComision = this.filas;
  filasComisionC = this.filasC;

  pDevolucion = 0;
  pDevolucionC = 0;
  filasDevolucion = this.filas;
  filasDevolucionC = this.filasC;

  pRecupero = 0;
  pRecuperoC = 0;
  filasRecupero = this.filas;
  filasRecuperoC = this.filasC;

  pGasto = 0;
  pGastoC = 0;
  filasGasto = this.filas;
  filasGastoC = this.filasC;

  pSunsystem = 0;
  pSunsystemC = 0;
  filasSunsystem = this.filas;
  filasSunsystemC = this.filasC;

  pNetsuite = 0;
  pNetsuiteC = 0;
  filasNetsuite = this.filas;
  filasNetsuiteC = this.filasC;

  listaPrimas:Array<any> = [];
  primasConci:Array<any> = [];
  listaComisiones:Array<any> = [];
  comisionesConci:Array<any> = [];
  listaDevoluciones:Array<any> = [];
  devolucionesConci:Array<any> = [];
  listaRecuperos:Array<any> = [];
  recuperosConci:Array<any> = [];
  listaGastos:Array<any> = [];
  gastosConci:Array<any> = [];
  
  listaSunsystem:Array<any> = [];
  sunsystemConci:Array<any> = [];
  listaNetsuite:Array<any> = [];
  netsuiteConci:Array<any> = [];

  totalProduccion: TotalProduccion[] = [];
  totProduccion: any;
  totalRespaldo: TotalRespaldo[] = [];
  totRespaldo: any;
  totRespaldoSV: any;
  totRespaldoNV: any;
  totRespaldoSG: any;
  totRespaldoNG: any;

  primaOP = "";
  comisionOP = "";
  devolucionOP = "";
  recuperoOP = "";
  gastoOP = ""; 
  facturaOP = "";
  notaCreditoOP = "";
  notaDebitoOP = "";

  sumaTotProduccion: any = 0;
  sumaTotRespaldo: any = 0;
  companiadisabled: boolean = true;

  showButtonSun: boolean = false; // para habilitar o no la búsqueda en sunsystem cuando selecciona prima, comision, devolución o recupero
  showButtonSun2: boolean = false; // para habilitar o no la búsqueda en netsuite según la configuración de la combinación en bd
  showButtonNet: boolean = false; // para habilitar o no la búsqueda en sunsystem cuando selecciona comisión, gasto o devolución
  showButtonNet2: boolean = false; // para habilitar o no la búsqueda en netsuite según la configuración de la combinación en bd
  showButtonCPD: boolean = false;
  showButtonCDR: boolean = false;
  showButtonPRIO: boolean = false;
  showButtonTolerancia: boolean = false;
  showButtonSSDinero: boolean = false;
  showButtonConci: boolean = false;
  montoConci: Number = 0;
  icono: boolean = false;

  //seleccionar todo
  masterSelected: boolean;
  checkedList: any[];

  //variables Subir Recuperos
  nombreArchivo: string;
  public headersRow: any[] = [];
  public records: any[] = [];
  public recuperos: Recuperos[];
  cantidadRegistros: number;
  fechaCarga: string;
  countRegister = 0;
  @ViewChild('csvReader') csvReader: any;

  public filtroFormGralD: FormGroup;  
  public filtroFormConci: FormGroup;

  public formFile: FormGroup;

  public filtroFormSUN: FormGroup;
  public filtroFormNET: FormGroup;
  //public validaciones: Validaciones[] = [];

  public formValidaPrima: FormGroup;


  public marcaPrima: Boolean = false;
  public marcaDevolucion: Boolean = false;
  public marcaComision: Boolean = false;
  public marcaRecupero: Boolean = false;
  public marcaGasto: Boolean = false;

  public marcaSunsystem: Boolean = false;
  public marcaNetsuite: Boolean = false;
  public stringCombinacion: String;
  public stringCuentas: String;
  public requiereSunsystem: boolean = false;
  public requiereRespaldo: boolean = false;

  tolerancia: Number = 5000;
  
  spinner: boolean;
  spinnerConfig: ISpinnerConfig;

  periodoActual = "";
  
  constructor(
    private filtroService: FiltrosService,
    private validacionService: ValidacionesService,
    private cuentasService: CuentasService,
    private sunsystemService: SunsystemService,
    private netsuiteService: NetsuiteService,
    private gastosService: GastosService,
    private alertaService: AlertasService,
    private recuperosService: RecuperosService,
    private conciliacionService: ConciliacionService,
    private alerta: AlertasService
  ) { 
    this.masterSelected = false;
  }

  ngOnInit(): void {

    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: "6rem",
      color: "#1dab5a",
      bgColor: "rgba(255,255,255,0.8)"
    };

    //this.showSpinner();
    //bgColor: "rgba(255,255,255,0.8)"


    this.filtroFormGralD = new FormGroup({
      compania: new FormControl(''),
      checkPrio: new FormControl(''),
      // checkTolerancia: new FormControl(''),
      checkSalidaDinero: new FormControl('')
    });

    this.filtroFormConci = new FormGroup({
      periodoContable: new FormControl('', Validators.required),
      medioPago: new FormControl(''),
    });

    this.formFile = new FormGroup({
      fileRecupero: new FormControl(''),
    });

    this.filtroFormSUN = new FormGroup({
      cuenta: new FormControl('')
    });

    this.filtroFormNET = new FormGroup({
      cuenta: new FormControl('')
    });

    this.formValidaPrima = new FormGroup({
      optDifPrima: new FormControl('0')
    });

    this.getValidaciones();
    this.listarCompaniesSun();
    this.listarPeriodosConciliacion();
    this.listarMediosPago();

  }

  public getValidaciones(){
    this.validacionService.getValidaciones('ALL').subscribe((data: any) =>{
      console.log("data Validaciones ", data);
      if(data.Status[0].code == "200"){
        for(let valida of data.Body){
          console.log("valida.tipo ", valida.tipo );
          if(valida.tipo == "combinacion"){
            this.validaCombinaciones.push(valida);
          }
          if(valida.tipo == "validacion"){
            this.validaConciliacion.push(valida);
          }
          if(valida.tipo == "gastos"){
            this.validaGastos.push(valida);
          }
        }
        
        console.log("this.validaCombinaciones ", this.validaCombinaciones);
        console.log("this.validaConciliacion " , this.validaConciliacion);
        console.log("this.validaGastos ", this.validaGastos);
      }
    })
  };

  public listarCompaniesSun() {
    this.companiasSun = [];
    this.filtroService.getCompanySun().subscribe( (data: any) => {
      //console.log('COMPAÑIAS:' + JSON.stringify(data.Body) );
      this.companiasSun = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarPeriodosConciliacion() {
    this.filtroService.getPeriodoConciliacion().subscribe( (data: any) => {
      console.log('PERIODOS:' + JSON.stringify(data.Body) );
      console.log("ULTIMO PERIODO.name " , data.Body[data.Body.length - 1].name);
      this.periodoActual = data.Body[data.Body.length - 1].value;
      this.filtroFormConci.patchValue({
        periodoContable: this.periodoActual
      });
      this.periodosConciliacion = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarMediosPago() {
    this.filtroService.getMediosPago().subscribe( (data: any) => {
      //console.log('PERIODOS:' + JSON.stringify(data.Body) );
      this.mediosPago = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  

  public listarPrimas(f: Filtros){
    let criterios = '';
    f.subConcepto = "PRIMA";
    f.socio = this.formFiltros.socio;
    f.compania = this.formFiltros.compania;
      console.log('listarPrimas:' + JSON.stringify(f));
      this.spinner = true;
      this.filtroService.getPaymentGroups(f).subscribe( (data: any) => {
        if(data.Status[0].code=="200"){
          this.spinner = false;
          if(data.Body.Groupings.length > 0){
            this.dataPrimas = this.filterGroupings(data.Body.Groupings, this.primasConci);
          }else{
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        }else{
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
        }
      }, error => {
        this.spinner = false;
        this.alerta.alertBasicIcon('Error', 'Error al consultar Primas', 'error');
      });
  }

  public filterGroupings(modalList: any, conciList: any){
    var filtered = [];
    var existe = 0;
    if(conciList.length > 0){
      for(var arr in modalList){
        existe = 0;
        for(var filter in conciList){
          if(modalList[arr].idAgrupacion == conciList[filter].idAgrupacion){
            existe = 1;
          }
        }
        if(existe == 0){
          filtered.push(modalList[arr]);
        }
      }
    }else{
      return modalList;
    }
    return filtered;
  }

  public filterGastos(modalList: any, gastoList: any){
    var filtered = [];
    var existe = 0;
    if(gastoList.length > 0){
      for(var arr in modalList){
        existe = 0;
        for(var filter in gastoList){
          if(modalList[arr].idGastos == gastoList[filter].idGastos){
            existe = 1;
          }
        }
        if(existe == 0){
          filtered.push(modalList[arr]);
        }
      }
    }else{
      return modalList;
    }
    return filtered;
  }

  public filterSunsystem(modalList: any, sunList: any){
    var filtered = [];
    var existe = 0;
    if(sunList.length > 0){
      for(var arr in modalList){
        existe = 0;
        for(var filter in sunList){
          if(modalList[arr].idSystem == sunList[filter].idSystem){
            existe = 1;
          }
        }
        if(existe == 0){
          filtered.push(modalList[arr]);
        }
      }
    }else{
      return modalList;
    }
    return filtered;
  }

  public filterNetsuite(modalList: any, netList: any){
    var filtered = [];
    var existe = 0;
    if(netList.length > 0){
      for(var arr in modalList){
        existe = 0;
        for(var filter in netList){
          if(modalList[arr].idnetsuite == netList[filter].idnetsuite){
            existe = 1;
          }
        }
        if(existe == 0){
          filtered.push(modalList[arr]);
        }
      }
    }else{
      return modalList;
    }
    return filtered;
  }

  
  public clearSunsystem(){
    this.dataSunsystem = [];
    this.listaSunsystem =  [];
    this.pSunsystem = 0;
    if(this.filtroFormGralD.value.compania != ""){ 
      switch(this.filtroFormGralD.value.compania){
        case "SVC": this.companiaGral = "VIDA";break;
        case "SGC": this.companiaGral = "GENERALES";break;
      }
    }else{
      this.companiaGral = this.formFiltros.compania;
    }
    this.socioGral = this.formFiltros.socio;
  }

  public clearNetsuite(){
    this.dataNetsuite = [];
    this.listaNetsuite =  [];
    this.pNetsuite = 0;
    if(this.filtroFormGralD.value.compania != ""){ 
      switch(this.filtroFormGralD.value.compania){
        case "SVC": this.companiaGral = "VIDA";break;
        case "SGC": this.companiaGral = "GENERALES";break;
      }
    }else{
      this.companiaGral = this.formFiltros.compania;
    }
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }

  public clearPrimas(){
    this.dataPrimas = [];
    this.listaPrimas =  [];
    this.pPrima = 0;
    this.companiaGral = this.formFiltros.compania; 
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }

  public clearComisiones(){
    this.dataComisiones = [];
    this.listaComisiones = [];
    this.pComision = 0;
    this.companiaGral = this.formFiltros.compania; 
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }

  public clearDevoluciones(){
    this.dataDevoluciones = [];
    this.listaDevoluciones = [];
    this.pDevolucion = 0;
    this.companiaGral = this.formFiltros.compania; 
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }

  public clearRecuperos(){
    this.dataRecuperos = [];
    this.listaRecuperos = [];
    this.pRecupero = 0;
    this.companiaGral = this.formFiltros.compania; 
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }

  public clearGastos(){
    this.dataGastos = [];
    this.listaGastos = [];
    this.pGasto = 0;
    this.companiaGral = this.formFiltros.compania; 
    this.socioGral = this.formFiltros.socio;
    this.masterSelected = false;
  }
  
  public listarComisiones(f: Filtros){
    let criterios = '';
    f.subConcepto = "COMISION";
    f.compania = this.formFiltros.compania;
    f.socio = this.formFiltros.socio;
    console.log("listarComisiones", JSON.stringify(f));
    this.spinner = true;
      this.filtroService.getPaymentGroups(f).subscribe( (data: any) => {
        if(data.Status[0].code=="200"){
          this.spinner = false;
          if(data.Body.Groupings.length > 0){
            this.dataComisiones = this.filterGroupings(data.Body.Groupings, this.comisionesConci);
          }else{
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        }else{
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
        }
      }, error => {
        this.spinner = false;
        this.alerta.alertBasicIcon('Error', 'Error al consultar Comisiones', 'error');
      });
  }

  public listarDevoluciones(f: Filtros){
      let criterios = '';
      f.subConcepto = "DEVOLUCION";
      f.compania = this.formFiltros.compania;
      f.socio = this.formFiltros.socio;
      console.log('FORMULARIO:' + JSON.stringify(f));
      this.spinner = true;
      this.filtroService.getPaymentGroups(f).subscribe( (data: any) => {
        if(data.Status[0].code=="200"){
          this.spinner = false;
          if(data.Body.Groupings.length > 0){
            this.dataDevoluciones = this.filterGroupings(data.Body.Groupings, this.devolucionesConci);
          }else{
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        }else{
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
        }
      }, error => {
        this.spinner = false;
        this.alerta.alertBasicIcon('Error', 'Error al consultar Devoluciones', 'error');
      });
  }

  public listarRecuperos(f: Filtros){
    let criterios = '';
    f.subConcepto = "RECUPERO";
    f.compania = this.formFiltros.compania;
    f.socio = this.formFiltros.socio;
    console.log('FORMULARIO:' + JSON.stringify(f));
    this.spinner = true;
    this.filtroService.getPaymentGroups(f).subscribe( (data: any) => {
      if(data.Status[0].code=="200"){
        this.spinner = false;
        if(data.Body.Groupings.length > 0){
          this.dataRecuperos = this.filterGroupings(data.Body.Groupings, this.recuperosConci);
        }else{
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      }else{
        this.spinner = false;
        this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
      }
    }, error => {
      this.spinner = false;
      this.alerta.alertBasicIcon('Error', 'Error al consultar Recuperos', 'error');
    });
  }

  public listarGastos(f: Filtros){
    let criterios = '';
    f.compania = this.formFiltros.compania;
    f.socio = this.formFiltros.socio;
    console.log('FORMULARIO:' + JSON.stringify(f));
    this.spinner = true;
    this.gastosService.getAllConci(f).subscribe( (data: any) => {
      if(data.Status[0].code=="200"){
        this.spinner = false;
        console.log("data.Body.Groupings.lenght " , data.Body.length);
        if(data.Body.length > 0){
          this.dataGastos = this.filterGastos(data.Body, this.gastosConci);
        }else{
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      }else{
        this.spinner = false;
        this.alertaService.alertBasicIcon('Error', data.Status.error, 'error');
      }
    }, error => {
      this.spinner = false;
      this.alerta.alertBasicIcon('Error', 'Error al consultar Gastos', 'error');
    });
  }

  public agregarPrimas(){
    let resultado = null;
    // if(this.listaPrimas.length > 0){
    if(this.checkedList.length > 0){
      // for( let prima of this.listaPrimas){
      for( let prima of this.checkedList){
        resultado = this.primasConci.find( primaC => primaC.idAgrupacion === prima.idAgrupacion);
        if(resultado == null){
          this.primasConci.push(prima);
        }
      }
      this.marcaPrima = true;
    }else{
      this.marcaPrima = false;
    }
    //this.totalizaRespaldo();
    this.validaMontosTotales();
    this.validaCombinacionTotaliza("PRIMA");
  }

  public validaCombinacionTotaliza(concepto: string){
    this.validaTipoCuenta();
    console.log("ingresa 599");
    if(this.esCombiacionValida){
      console.log("ingresó this.esCombiacionValida 504")
      this.totalizaProduccion(concepto);
    }else{
      console.log("NO ingresó this.esCombiacionValida 504");
      if(this.marcaPrima == true || this.marcaComision == true || this.marcaDevolucion == true
        || this.marcaRecupero == true || this.marcaGasto == true){
          this.alertaService.alertNotification('Operación no permitida', 'error');
        }
        this.sumaTotProduccion = 0;
        this.totalProduccion = [];
    }
  }

  public agregarComisiones(){
    let resultado = null;
    // if(this.listaComisiones.length > 0){
    if(this.checkedList.length > 0){
      // for( let comision of this.listaComisiones){
      for( let comision of this.checkedList){
        resultado = this.comisionesConci.find( comisionC => comisionC.idAgrupacion === comision.idAgrupacion);
        if(resultado == null){
          this.comisionesConci.push(comision);
        }
      }
      this.marcaComision = true;
    }else{
      this.marcaComision = false;
    }
    //this.totalizaRespaldo();
    this.validaMontosTotales();
    this.validaCombinacionTotaliza("COMISION");
  }

  public agregarDevoluciones(){
    let resultado = null;
    // if(this.listaDevoluciones.length > 0){
    if(this.checkedList.length > 0){
      // for( let devolucion of this.listaDevoluciones){
      for( let devolucion of this.checkedList){
        resultado = this.devolucionesConci.find( devolucionC => devolucionC.idAgrupacion === devolucion.idAgrupacion);
        if(resultado == null){
          this.devolucionesConci.push(devolucion);
        }
      }
      this.marcaDevolucion = true;
    }else{
      this.marcaDevolucion = false;
    }
    //this.totalizaRespaldo();
    this.validaMontosTotales();
    this.validaCombinacionTotaliza("DEVOLUCION");
  }

  public agregarRecuperos(){
    let resultado = null;
    // if(this.listaRecuperos.length > 0){
    if(this.checkedList.length > 0){
      // for( let recupero of this.listaRecuperos){
      for( let recupero of this.checkedList){
        resultado = this.recuperosConci.find( recuperoC => recuperoC.idAgrupacion === recupero.idAgrupacion);
        if(resultado == null){
          this.recuperosConci.push(recupero);
        }
      }
      this.marcaRecupero = true;
    }else{
      this.marcaRecupero = false;
    }
    //this.totalizaRespaldo();
    this.validaMontosTotales();
    this.validaCombinacionTotaliza("RECUPERO");
  }

  public agregarGastos(){
    let resultado = null;
    // if(this.listaGastos.length > 0){
    if(this.checkedList.length > 0){
      // for( let gasto of this.listaRecuperos){
      for( let gasto of this.checkedList){
        resultado = this.gastosConci.find( gastoC => gastoC.idGastos === gasto.idGastos);
        if(resultado == null){
          this.gastosConci.push(gasto);
        }
      }
      this.gastosConci = this.checkedList;
      this.marcaGasto = true;
    }else{
      this.marcaGasto = false;
    }
    //this.totalizaRespaldo();
    this.validaMontosTotales();
    this.validaCombinacionTotaliza("GASTO");
  }

  public agregarCuentas(){
    let resultado = null;
    // if(this.listaSunsystem.length > 0){
    if(this.checkedList.length > 0){
      //this.sunsystemConci = this.listaSunsystem;
      // for( let sun of this.listaSunsystem){
      for( let sun of this.checkedList){
        resultado = this.sunsystemConci.find( sunC => sunC.idSystem === sun.idSystem);
        if(resultado == null){
          this.sunsystemConci.push(sun);
        }
      }
      this.marcaSunsystem = true;
    }else{
      this.marcaSunsystem = false;
    }
    //this.validaTipoCuenta();
    this.totalizaRespaldo();
  }

  public agregarNetsuite(){

    let resultado = null;
    // if(this.listaNetsuite.length > 0){
    if(this.checkedList.length > 0){
      //this.sunsystemConci = this.listaSunsystem;
      // for( let net of this.listaNetsuite){
      for( let net of this.checkedList){
        resultado = this.netsuiteConci.find( netC => netC.idnetsuite === net.idnetsuite);
        if(resultado == null){
          this.netsuiteConci.push(net);
        }
      }
      this.marcaNetsuite = true;
    }else{
      this.marcaNetsuite = false;
    }
    //this.validaTipoCuenta();
    this.totalizaRespaldo();
  }

  public totalizaRespaldo(){
    //this.totalRespaldo = [];
    this.sumaTotRespaldo = 0;
    let monSunV = 0;
    this.totRespaldoSV = {};
    this.totRespaldoSG = {};
    this.totRespaldoNV = {};
    this.totRespaldoNG = {};
    this.totalRespaldo = [];
    let monSunG = 0;
    let monNetV = 0;
    let monNetG = 0;

    console.log("SUNSYSTEMCONCI " , this.sunsystemConci);

    if(this.sunsystemConci.length > 0){
      for(let sun of this.sunsystemConci){
        this.sumaTotRespaldo = this.sumaTotRespaldo + Math.round(Number(sun.baseAmount));
        if(sun.compania == "VIDA"){
          monSunV = monSunV + Math.round(Number(sun.baseAmount));
        }
        if(sun.compania == "GENERALES"){
          monSunG = monSunG + Math.round(Number(sun.baseAmount));
        }
      }
      if(monSunV > 0){
        this.totRespaldoSV.concepto = "SUNSYSTEM";
        this.totRespaldoSV.compania = "VIDA";
        this.totRespaldoSV.monto = monSunV;
      }
      if(monSunG > 0){
        this.totRespaldoSG.concepto = "SUNSYSTEM";
        this.totRespaldoSG.compania = "GENERALES";
        this.totRespaldoSG.monto = monSunG;
      }
    }

    if(this.netsuiteConci.length > 0){
      for(let net of this.netsuiteConci){

        if(this.sunsystemConci.length == 0)
          this.sumaTotRespaldo = this.sumaTotRespaldo + Math.round(Number(net.bruto));
        if(net.compania == "VIDA"){
          monNetV = monNetV + Math.round(Number(net.bruto));
        }
        if(net.compania == "GENERALES"){
          monNetG = monNetG + Math.round(Number(net.bruto));
        }
      }
      if(monNetV > 0){
        this.totRespaldoNV.concepto = "NETSUITE";
        this.totRespaldoNV.compania = "VIDA";
        this.totRespaldoNV.monto = monNetV;
      }
      if(monNetG > 0){
        this.totRespaldoNG.concepto = "NETSUITE";
        this.totRespaldoNG.compania = "GENERALES";
        this.totRespaldoNG.monto = monNetG;
      }
    }

    if(this.marcaPrima){
      if(monSunV > 0) this.totalRespaldo.push(this.totRespaldoSV);
      if(monSunG > 0) this.totalRespaldo.push(this.totRespaldoSG);
      //if(this.sunsystemConci.length > 0){this.sumaRespaldo(0, "SUNSYSTEM", this.sunsystemConci);}
    }else{
      if(this.filtroFormGralD.value.checkSalidaDinero){
        if(monSunV > 0) this.totalRespaldo.push(this.totRespaldoSV);
        if(monSunG > 0) this.totalRespaldo.push(this.totRespaldoSG);
        //if(this.sunsystemConci.length > 0){this.sumaRespaldo(0, "SUNSYSTEM", this.sunsystemConci);}
      }else{
        console.log("monNetV " , monNetV);
        console.log("monNetG " , monNetG);
        console.log("this.totRespaldoNV " , this.totRespaldoNV);
        console.log("this.totRespaldoNG " , this.totRespaldoNG);
        if(monNetV > 0) this.totalRespaldo.push(this.totRespaldoNV);
        if(monNetG > 0) this.totalRespaldo.push(this.totRespaldoNG);
        //if(this.netsuiteConci.length > 0){this.sumaRespaldo(1, "NETSUITE", this.netsuiteConci);}
      }
    }
    this.validaMontosTotales();
  }

  public sumaRespaldo(indice: number, respaldoTipo: string, lista: any[]){

    let sumMonto = 0;
    this.totRespaldo = {};
    let compania = "";
    let indicador = 0;
    if(this.filtroFormGralD.value.compania != ""){ 
      //compania = this.filtroFormGralD.value.compania;
      switch(this.filtroFormGralD.value.compania){
        case "SVC":compania = "VIDA";break;
        case "SGC":compania = "GENERALES";break;
      }
    }else{
      compania = this.formFiltros.compania;
    }

    console.log("compania ", compania);
    switch(indice){
      case 0:
        console.log("744");
        for(let respaldo of lista){
          sumMonto = sumMonto + Math.round(Number(respaldo.baseAmount));
          this.sumaTotRespaldo = this.sumaTotRespaldo + Math.round(Number(respaldo.baseAmount));
        }
        break;
      case 1:
        console.log("781");
        for(let respaldo of lista){
          sumMonto = sumMonto + Math.round(Number(respaldo.montoFacturaNet));
          this.sumaTotRespaldo = this.sumaTotRespaldo + Math.round(Number(respaldo.montoFacturaNet));
        }
        break;
      default:
        break;
    }

    this.totRespaldo.concepto = respaldoTipo;
    this.totRespaldo.monto = sumMonto;
    this.totRespaldo.compania = compania;
    console.log("totalRespaldo" , this.totalRespaldo);    
    console.log("respaldoTipo" , respaldoTipo);    
    console.log("compania" , compania);

    for(let total of this.totalRespaldo){
      if(total.concepto == respaldoTipo && total.compania == compania){
        console.log("ingresa 874");
        indicador = 1;
        this.totRespaldo.concepto = respaldoTipo;
        this.totRespaldo.monto = sumMonto;
        this.totRespaldo.compania = compania;
                
        console.log("totalRespaldo 1", this.totalRespaldo);
        this.totalRespaldo = this.totalRespaldo.filter(tot => tot.compania != compania);
        console.log("totalRespaldo 2", this.totalRespaldo);
        //this.totalRespaldo.push(this.totRespaldo);
        //console.log("totalRespaldo 3", this.totalRespaldo);
      }
    }

    console.log("totalRespaldo" , this.totalRespaldo);    

    if(indicador == 0){
      console.log("ingresa 888");
      this.totalRespaldo.push(this.totRespaldo);
      this.totalRespaldo.push(this.totRespaldo);
    }
    console.log("totalRespaldo" , this.totalRespaldo);    
  }


  public validaMontosTotales(){
    // if(this.sumaTotProduccion > 0 && this.sumaTotRespaldo > 0){
      console.log("sumaTotProduccion " + this.sumaTotProduccion);
      console.log("sumaTotRespaldo " + this.sumaTotRespaldo);
      let diff = 0;
      diff = Number(this.sumaTotRespaldo) - Number(this.sumaTotProduccion);
      // if(this.sumaTotProduccion <= this.sumaTotRespaldo){
      console.log("diff " , diff);
      console.log("Number(this.tolerancia) " , Number(this.tolerancia));
      console.log("Number(this.tolerancia) * -1 " , Number(this.tolerancia) * -1);

      if(this.sumaTotRespaldo > 0 && this.sumaTotProduccion > 0){
        this.montoConci = Number(this.sumaTotRespaldo) - Number(this.sumaTotProduccion);
        if(diff <= Number(this.tolerancia) && diff >= (Number(this.tolerancia) * -1)){
          console.log("this.montoConci " , this.montoConci);
          console.log("this.tolerancia ", this.tolerancia);
          if(this.montoConci > this.tolerancia){
            this.showButtonTolerancia = true;
          }else{
            this.showButtonTolerancia = false;
          }
          this.showButtonConci = true;
        }else{
          if(this.marcaPrima && !this.marcaComision && !this.marcaDevolucion && !this.marcaRecupero
            && !this.marcaGasto){
            this.showButtonConci = true;
          }else{
            this.showButtonConci = false;
          }
        }
      }else{
        this.showButtonConci = false;
      }
    // }else{
    //   this.showButtonConci = false;
    // }
  }

  public totalizaProduccion(concepto: string){

    this.primaOP = "";
    this.comisionOP = "";
    this.devolucionOP = "";
    this.recuperoOP = "";
    this.gastoOP = "";
    this.facturaOP = "";
    this.notaCreditoOP = "";
    this.notaDebitoOP = ""; 
    console.log("924");

    for(let valida of this.validaCombinaciones){
      if(valida.nombre == this.stringCombinacion){
        let cuentas = JSON.parse(valida.valor);
        console.log("cuentas ", cuentas);
        if(cuentas.operaProd.prima){
          this.primaOP = cuentas.operaProd.prima;
        }
        if(cuentas.operaProd.comision){
          this.comisionOP = cuentas.operaProd.comision;
        }
        if(cuentas.operaProd.devolucion){
          this.devolucionOP = cuentas.operaProd.devolucion;
        }
        if(cuentas.operaProd.recupero){
          this.recuperoOP = cuentas.operaProd.recupero;
        }
        if(cuentas.operaProd.gasto){
          this.gastoOP = cuentas.operaProd.gasto;
        }
        console.log("cuentas.operaRespaldo.factura ", cuentas.operaRespaldo.factura);
        if(cuentas.operaRespaldo.factura){
          this.facturaOP = cuentas.operaRespaldo.factura;
        }
        if(cuentas.operaRespaldo.notaCredito){
          this.notaCreditoOP = cuentas.operaRespaldo.notaCredito;
        }
        if(cuentas.operaRespaldo.notaDebito){
          this.notaDebitoOP = cuentas.operaRespaldo.notaDebito;
        }
      }
    }

    this.totalProduccion = [];
    this.sumaTotProduccion = 0;
    console.log("960");
    if(this.primasConci.length > 0){this.sumaProduccion2("PRIMA", this.primasConci, this.primaOP);}
    if(this.comisionesConci.length > 0){this.sumaProduccion2("COMISION", this.comisionesConci, this.comisionOP);}
    if(this.devolucionesConci.length > 0){this.sumaProduccion2("DEVOLUCION", this.devolucionesConci, this.devolucionOP);}
    if(this.recuperosConci.length > 0){this.sumaProduccion2("RECUPERO", this.recuperosConci, this.recuperoOP);}
    console.log("this.totalProduccion ", this.totalProduccion);
    if(this.gastosConci.length > 0){this.sumaProduccion2("GASTO", this.gastosConci, this.gastoOP);}

    console.log("this.totalProduccion ", this.totalProduccion);
    this.validaMontosTotales();
  }

  public sumaProduccion2(concepto, lista, operacion){
    let sumBruto = 0;
    let sumNeto = 0;
    let sumExcento = 0;
    let sumAfecto = 0;
    let sumIva = 0;
    this.totProduccion = {};
    console.log("691 concepto " , concepto);
    console.log("lista ", lista);
    if(concepto == "GASTO"){
        for(let concepto of lista){
          switch(operacion){
            case "suma": 
              if(Number(concepto.montoFinal) < 0){
                sumNeto = sumNeto + (Number(concepto.montoFinal)*-1);
                sumIva = sumIva + (Number(concepto.iva)*-1);
              }else{
                sumNeto = sumNeto + (Number(concepto.montoFinal));
                sumIva = sumIva + (Number(concepto.iva));
              }
              break;
            case "resta":
              if(Number(concepto.montoFinal) > 0){
                sumNeto = sumNeto + (Number(concepto.montoFinal)*-1);
                sumIva = sumIva + (Number(concepto.iva) * -1);
              }else{
                sumNeto = sumNeto + (Number(concepto.montoFinal));
                sumIva = sumIva + (Number(concepto.iva));
              }
              break;
          }
          if(operacion == "suma"){this.sumaTotProduccion = this.sumaTotProduccion + Number(concepto.montoFinal);}
          if(operacion == "resta"){this.sumaTotProduccion = this.sumaTotProduccion - Number(concepto.montoFinal);}
        }
    }else{
      for(let concepto of lista){
        switch(operacion){
          case "suma":
            console.log("operacion " , operacion);
            console.log("concepto " , concepto);
            if(Number(concepto.brutoPesosPims) < 0){sumBruto = sumBruto + Number(concepto.brutoPesosPims) * -1;}else{sumBruto = sumBruto + Number(concepto.brutoPesosPims);}
            if(Number(concepto.netoPesosPims) < 0){sumNeto = sumNeto + Number(concepto.netoPesosPims) * -1;}else{sumNeto = sumNeto + Number(concepto.netoPesosPims);}
            if(Number(concepto.excetoPesosPims) < 0){sumExcento = sumExcento + Number(concepto.excetoPesosPims) * -1;}else{sumExcento = sumExcento + Number(concepto.excetoPesosPims);}
            if(Number(concepto.afectoPesosPims) < 0){sumAfecto = sumAfecto + Number(concepto.afectoPesosPims) * -1;}else{sumAfecto = sumAfecto + Number(concepto.afectoPesosPims);}
            if(Number(concepto.ivaPesosPims) < 0){sumIva = sumIva + Number(concepto.ivaPesosPims) * -1;}else{sumIva = sumIva + Number(concepto.ivaPesosPims);}
          break;
          case "resta":
            if(Number(concepto.brutoPesosPims) > 0){sumBruto = sumBruto + Number(concepto.brutoPesosPims) * -1;}else{sumBruto = sumBruto + Number(concepto.brutoPesosPims);}
            if(Number(concepto.netoPesosPims) > 0){sumNeto = sumNeto + Number(concepto.netoPesosPims) * -1;}else{sumNeto = sumNeto + Number(concepto.netoPesosPims);}
            if(Number(concepto.excetoPesosPims) > 0){sumExcento = sumExcento + Number(concepto.excetoPesosPims) * -1;}else{sumExcento = sumExcento + Number(concepto.excetoPesosPims);}
            if(Number(concepto.afectoPesosPims) > 0){sumAfecto = sumAfecto + Number(concepto.afectoPesosPims) * -1;}else{sumAfecto = sumAfecto + Number(concepto.afectoPesosPims);}
            if(Number(concepto.ivaPesosPims) > 0){sumIva = sumIva + Number(concepto.ivaPesosPims) * -1;}else{sumIva = sumIva + Number(concepto.ivaPesosPims);}
        }
        //alert("concepto " + concepto + " operacion " + operacion + " concepto.netoPesosPims " + concepto.netoPesosPims);
        //alert("this.sumatotproduccion "  + this.sumaTotProduccion);
        if(operacion == "suma") {this.sumaTotProduccion =  this.sumaTotProduccion + Math.round(Number(concepto.brutoPesosPims))};
        if(operacion == "resta") {this.sumaTotProduccion =  this.sumaTotProduccion - Math.round(Number(concepto.brutoPesosPims))};
        //alert("this.sumatotproduccion "  + this.sumaTotProduccion);
      }
    }

    console.log("sumNeto " , sumNeto);
    if(concepto == "GASTO"){
      this.totProduccion.concepto = concepto;
      this.totProduccion.brutoPesosPims = sumNeto;
      this.totProduccion.netoPesosPims = sumBruto;
      this.totProduccion.excetoPesosPims = sumExcento;
      this.totProduccion.afectoPesosPims = sumAfecto;
      this.totProduccion.ivaPesosPims = sumIva;
    }else{
      this.totProduccion.concepto = concepto;
      this.totProduccion.brutoPesosPims = sumBruto;
      this.totProduccion.netoPesosPims = sumNeto;
      this.totProduccion.excetoPesosPims = sumExcento;
      this.totProduccion.afectoPesosPims = sumAfecto;
      this.totProduccion.ivaPesosPims = sumIva;
    }
    

    // this.totalProduccion.forEach( (item, index) => {
    //   if(item.concepto == concepto){
    //     this.totalProduccion.splice(index, 1);
    //   }
    // });

    this.totalProduccion.push(this.totProduccion);
  }

  public sumaProduccion(indice: number, conceptoTipo: string, lista: any[], operacion: string){
    let sumBruto = 0;
    let sumNeto = 0;
    let sumExcento = 0;
    let sumAfecto = 0;
    let sumIva = 0;
    this.totProduccion = {};
    
    switch(indice){
      case 4:
        for(let concepto of lista){
          switch(operacion){
            case "suma": 
              if(Number(concepto.montoFinal) < 0){
                sumNeto = sumNeto + (Number(concepto.montoFinal)*-1);
              }
              break;
            case "resta":
              if(Number(concepto.montoFinal) > 0){
                sumNeto = sumNeto + (Number(concepto.montoFinal)*-1);
              }
              break;
          }
          if(operacion == "suma"){this.sumaTotProduccion = this.sumaTotProduccion + Number(concepto.montoFinal);}
          if(operacion == "resta"){this.sumaTotProduccion = this.sumaTotProduccion - Number(concepto.montoFinal);}
        }
        break;
      default:
      for(let concepto of lista){
        switch(operacion){
          case "suma":
            if(Number(concepto.brutoPesosPims) < 0){sumBruto = sumBruto + Number(concepto.brutoPesosPims) * -1;}else{sumBruto = sumBruto + Number(concepto.brutoPesosPims);}
            if(Number(concepto.netoPesosPims) < 0){sumNeto = sumNeto + Number(concepto.netoPesosPims) * -1;}else{sumNeto = sumNeto + Number(concepto.netoPesosPims);}
            if(Number(concepto.excetoPesosPims) < 0){sumExcento = sumExcento + Number(concepto.excetoPesosPims) * -1;}else{sumExcento = sumExcento + Number(concepto.excetoPesosPims);}
            if(Number(concepto.afectoPesosPims) < 0){sumAfecto = sumAfecto + Number(concepto.afectoPesosPims) * -1;}else{sumAfecto = sumAfecto + Number(concepto.afectoPesosPims);}
            if(Number(concepto.ivaPesosPims) < 0){sumIva = sumIva + Number(concepto.ivaPesosPims) * -1;}else{sumIva = sumIva + Number(concepto.ivaPesosPims);}
          break;
          case "resta":
            if(Number(concepto.brutoPesosPims) > 0){sumBruto = sumBruto + Number(concepto.brutoPesosPims) * -1;}else{sumBruto = sumBruto + Number(concepto.brutoPesosPims);}
            if(Number(concepto.netoPesosPims) > 0){sumNeto = sumNeto + Number(concepto.netoPesosPims) * -1;}else{sumNeto = sumNeto + Number(concepto.netoPesosPims);}
            if(Number(concepto.excetoPesosPims) > 0){sumExcento = sumExcento + Number(concepto.excetoPesosPims) * -1;}else{sumExcento = sumExcento + Number(concepto.excetoPesosPims);}
            if(Number(concepto.afectoPesosPims) > 0){sumAfecto = sumAfecto + Number(concepto.afectoPesosPims) * -1;}else{sumAfecto = sumAfecto + Number(concepto.afectoPesosPims);}
            if(Number(concepto.ivaPesosPims) > 0){sumIva = sumIva + Number(concepto.ivaPesosPims) * -1;}else{sumIva = sumIva + Number(concepto.ivaPesosPims);}
        }
        //alert("conceptoTipo " + conceptoTipo + " operacion " + operacion + " concepto.netoPesosPims " + concepto.netoPesosPims);
        //alert("this.sumatotproduccion "  + this.sumaTotProduccion);
        if(operacion == "suma") {this.sumaTotProduccion =  this.sumaTotProduccion + Number(concepto.netoPesosPims)};
        if(operacion == "resta") {this.sumaTotProduccion =  this.sumaTotProduccion - Number(concepto.netoPesosPims)};
        //lert("this.sumatotproduccion "  + this.sumaTotProduccion);
      }
    }

    this.totProduccion.concepto = conceptoTipo;
    this.totProduccion.brutoPesosPims = sumBruto;
    this.totProduccion.netoPesosPims = sumNeto;
    this.totProduccion.excetoPesosPims = sumExcento;
    this.totProduccion.afectoPesosPims = sumAfecto;
    this.totProduccion.ivaPesosPims = sumIva;

    this.totalProduccion.forEach( (item, index) => {
      if(item.concepto == conceptoTipo){
        this.totalProduccion.splice(index, 1);
      }
    });

    this.totalProduccion.push(this.totProduccion);
  }  

  public validaTipoCuenta(){

    this.stringCombinacion = "";
    var tipoGasto = "";
    var distintoTipoGasto = 0;
    console.log("marca prima", this.marcaPrima);
    console.log("marca comision", this.marcaComision);

    if(this.marcaPrima == true){
      this.stringCombinacion = "PRIMA-";
    }

    if(this.marcaDevolucion == true){
      this.stringCombinacion = this.stringCombinacion + "DEVOLUCION-";
    }

    if(this.marcaComision == true){
      this.stringCombinacion = this.stringCombinacion + "COMISION-";
    }

    if(this.marcaRecupero == true){
      this.stringCombinacion = this.stringCombinacion + "RECUPERO-";
    }

    if(this.marcaGasto == true){ 
      // if(this.marcaPrima == true || this.marcaComision == true || 
      //   this.marcaDevolucion == true || this.marcaRecupero == true){
      //   this.stringCombinacion = this.stringCombinacion + "GASTO-";
      // }else{
        //CAMBIAR SI ES PARA MAS DE 1 GASTO A LA VEZ
        for(let gastos of this.gastosConci){
          if(tipoGasto != "" && tipoGasto != gastos.codigo){
            distintoTipoGasto = 1;
          }
          tipoGasto = gastos.codigo;
        }
      
        if(distintoTipoGasto == 0){
          this.stringCombinacion = this.stringCombinacion + "GASTO" + tipoGasto + "-";
        }else{
          this.stringCombinacion = "";
        }

      // }
    }

    if(this.marcaDevolucion == true && this.marcaPrima == false && this.marcaComision == false
      && this.marcaRecupero == false && this.marcaGasto == false){
      this.filtroFormGralD.patchValue({
        checkSalidaDinero: true
      })
    }else{
      this.filtroFormGralD.patchValue({
        checkSalidaDinero: false
      })
    }
    
    console.log("tipoGasto ", tipoGasto);
    console.log("this.stringCombiacion ", this.stringCombinacion);

    if(this.stringCombinacion.length > 0){
      this.stringCombinacion = this.stringCombinacion.substr(0,this.stringCombinacion.length-1);
    }

    console.log("stringCombinacion " , this.stringCombinacion);

    this.compruebaTiposCuenta(this.stringCombinacion);
    
  }

  public compruebaTiposCuenta(combinacion: String){
    this.showButtonCPD = false;
    this.esCombiacionValida = false;

    //verificar si gastos habilita a sunsystem
    if(this.primasConci.length > 0 || this.comisionesConci.length > 0 || 
      this.devolucionesConci.length > 0 || this.recuperosConci.length > 0){
      this.showButtonSun = true;
    }
    
    if(this.comisionesConci.length > 0 || this.gastosConci.length > 0 || this.recuperosConci.length > 0){
      this.showButtonNet = true;
    }

    console.log("this.validaCombinaciones " , this.validaCombinaciones);
    for(let valida of this.validaCombinaciones){
      console.log("valida " , valida);
      if(valida.nombre == combinacion){
        this.esCombiacionValida = true;
        this.configCombinacion = JSON.parse(valida.valor);
        console.log("valida.nombre" , valida.nombre + " combinacion " , combinacion);
        let cuentas = JSON.parse(valida.valor);

        console.log("CUENTAS ", cuentas);
        //habilita si es prio o no
        if(cuentas.prio == "true"){this.showButtonPRIO = true;
        }else{this.showButtonPRIO = false;}

        //Habilita si es sin salida de dinero
        if(cuentas.sinSalidaDinero == "true"){this.showButtonSSDinero = true;
        }else{this.showButtonSSDinero = false;}

        //Habilita busqueda en sunsystem si la configuración lo permite
        if(cuentas.respaldo.indexOf("sunsystem") >= 0){this.showButtonSun2 = true;
        }else{ this.showButtonSun2 = false;}

        if(cuentas.respaldo.indexOf("netsuite") >= 0){this.showButtonNet2 = true;
        }else{ this.showButtonNet2 = false;}

        if(cuentas.requiereSunsystem == "true"){this.requiereSunsystem = true;
        }else{this.requiereSunsystem = true;}
        
        if(cuentas.requiereRespaldo == "true"){this.requiereRespaldo = true;
        }else{this.requiereRespaldo = false;}

        //for que obtiene el string con los tipos de cuenta permitidos para esa combinación
        for(let tipoCuenta of cuentas.tipoCuentas){
          //console.log("tipoCuenta.nombre " , tipoCuenta.nombre);
          switch(tipoCuenta.nombre){
            case "CPD": {
              this.showButtonCPD = true;
              this.stringCuentas = "CPD-";
              break;
            }
            case "CRD": {
              this.stringCuentas += "CRD-";
              break;
            }
            case "CEF": {
              this.stringCuentas += "CEF-";
              break;
            }
            case "CDA": {
              this.stringCuentas += "CDA-";
              break;
            }
            case "CRD": {
              this.stringCuentas += "CRD-";
              break;
            }
            case "CRP": {
              this.stringCuentas += "CRP-";
              break;
            }
            case "FCP": {
              this.stringCuentas += "FCP-";
              break;
            }
            case "CPS": {
              this.stringCuentas += "CPS-";
              break;
            }            
            case "FOA": {
              this.stringCuentas += "FOA-";
              break;
            }            
          }
        }

        if(this.stringCuentas.length > 0 ){
          this.stringCuentas = this.stringCuentas.substr(0,this.stringCuentas.length-1);
        }
      }

      //console.log("this.stringCuentas ", this.stringCuentas);
    }
    if(!this.esCombiacionValida){
      //alert("No existe configuración para esta combinación");
      this.configCombinacion = "";
    }else{
      this.obtenerCuentas();
    }
  }

  obtenerCuentas(){
    this.cuentasSocio = []; 
      this.cuentasService.getCuentas(this.formFiltros.socio).subscribe((data: any) => {
        if(data.Status[0].code=="200"){
          this.cuentasSocio = data.Body.cuentas;
          /*for(let cuenta of data.Body.cuentas){
            if(this.stringCuentas.indexOf(cuenta.tipoCuenta) >= 0){
              this.cuentasSocio.push(cuenta);
            }
          }*/
        }
      });

  }

  public buscarSunsystem(){
    console.log("cuenta " , this.filtroFormSUN.value.cuenta);
    let compania = "";
    if(this.filtroFormGralD.value.compania != ""){ compania = this.filtroFormGralD.value.compania;}else{
      switch(this.formFiltros.compania){
        case "VIDA":compania = "SVC";break;
        case "GENERALES":compania = "SGC";break;
      }
    }
    if (this.filtroFormSUN.valid) {
      this.spinner = true;
      this.sunsystemService.getSunsystem(this.filtroFormSUN.value.cuenta, this.formFiltros.socio, compania).subscribe( (data: any) => {
        if(data.Status[0].code=="200"){
          this.spinner = false;
          if(data.Body.length > 0){
            this.dataSunsystem = this.filterSunsystem(data.Body, this.sunsystemConci);
          }else{
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', 'No existen registros para la búsqueda', 'error');
          }
        }else{
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
        }
      }, error => {
        this.spinner = false;
        this.alerta.alertBasicIcon('Error', 'Error al consultar a Sunsystem', 'error');
      });

    } else {
      this.spinner = false;
      this.alerta.alertBasicIcon('Error', 'Debe seleccionar una cuenta!', 'error');
    }

  }

  public buscarNetsuite(){
    console.log("this.filtroFormGralI.value.socio " , this.formFiltros.socio);
    console.log("this.formFiltros.socio " , this.formFiltros.socio);
    this.spinner = true;
    let compania = "";
    if(this.filtroFormGralD.value.compania != ""){ compania = this.filtroFormGralD.value.compania;}else{
      switch(this.formFiltros.compania){
        case "VIDA":compania = "SVC";break;
        case "GENERALES":compania = "SGC";break;
      }
    }
    
    this.netsuiteService.getNetsuite(this.formFiltros.socio, compania).subscribe( (data: any) => {
      if(data.Status[0].code=="200"){
        this.spinner = false;
        if(data.Body.length > 0){
          this.dataNetsuite = this.filterNetsuite(data.Body, this.netsuiteConci);
        }else{
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'No existen registros para la búsqueda', 'error');
        }
      }else{
        this.spinner = false;
        this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
      }
    }, error => {
      this.spinner = false;
      this.alerta.alertBasicIcon('Error', 'Error al consultar a Netsuite', 'error');
    });
  }

  public marcaCheckPrima(e: any){
    if(e.target.checked == true){
      for(let primas of this.dataPrimas){
        if(primas.idAgrupacion == e.target.value){
          console.log("primas.idAgrupacion ", primas.idAgrupacion);
          primas.isSelected = true;
          //this.listaPrimas.push(primas);
        }
      }
    }else{
      for(let primas of this.dataPrimas){
        if(primas.idAgrupacion == e.target.value){
          primas.isSelected = false;
          //this.listaPrimas.push(primas);
        }
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista con checks: ", this.listaPrimas);
  }

  public deletePrima(idAgrupacion){
      console.log("DELETE PRIMA");
      console.log("idAgrupacion " , idAgrupacion);
      console.log("this.primasConci " , this.primasConci);
      this.primasConci = this.primasConci.filter(agrupacion => agrupacion.idAgrupacion !== idAgrupacion);
      console.log("this.primasConci " , this.primasConci);
      console.log("this.primasConci.length", this.primasConci.length);
      if(this.primasConci.length > 0){this.marcaPrima = true;}else{this.marcaPrima = false;}
      console.log("marcaPrima " , this.marcaPrima);
      this.validaCombinacionTotaliza("PRIMA");
      this.totalizaRespaldo();
  }
  
  public deleteComision(idAgrupacion){
      this.comisionesConci = this.comisionesConci.filter(agrupacion => agrupacion.idAgrupacion !== idAgrupacion);
      if(this.comisionesConci.length > 0){this.marcaComision = true;}else{this.marcaComision = false;}
      this.validaCombinacionTotaliza("COMISION");
      this.totalizaRespaldo();
  }

  public deleteDevolucion(idAgrupacion){
      this.devolucionesConci = this.devolucionesConci.filter(agrupacion => agrupacion.idAgrupacion !== idAgrupacion);
      if(this.devolucionesConci.length > 0){this.marcaDevolucion = true;}else{this.marcaDevolucion = false;}
      this.validaCombinacionTotaliza("DEVOLUCION");
      this.totalizaRespaldo();
  }
  
  public deleteRecupero(idAgrupacion){
      this.recuperosConci = this.recuperosConci.filter(agrupacion => agrupacion.idAgrupacion !== idAgrupacion);
      if(this.recuperosConci.length > 0){this.marcaRecupero = true;}else{this.marcaRecupero = false;}
      this.validaCombinacionTotaliza("RECUPERO");
      this.totalizaRespaldo();
  }
  
  public deleteGasto(idGastos){
      this.gastosConci = this.gastosConci.filter(agrupacion => agrupacion.idGastos !== idGastos);
      if(this.gastosConci.length > 0){this.marcaGasto = true;}else{this.marcaGasto = false;}
      this.validaCombinacionTotaliza("GASTO");
      this.totalizaRespaldo();
  }

  public deleteSunsystem(idSystem){
      this.sunsystemConci = this.sunsystemConci.filter(sunsystem => sunsystem.idSystem !== idSystem);
      if(this.sunsystemConci.length > 0){this.marcaSunsystem = true;}else{this.marcaSunsystem = false;}
      this.totalizaRespaldo();
  }

  public deleteNetsuite(idnetsuite){
      this.netsuiteConci = this.netsuiteConci.filter(netsuite => netsuite.idnetsuite !== idnetsuite);
      if(this.netsuiteConci.length > 0){this.marcaNetsuite = true;}else{this.marcaNetsuite = false;}
      this.totalizaRespaldo();
  }

  public marcaCheckSSDinero(e: any){
      this.filtroFormGralD.patchValue({
        checkSalidaDinero: e.target.checked
      });
    console.log("this.filtroFormGralD.value.checkSalidaDinero " , this.filtroFormGralD.value.checkSalidaDinero);
    this.totalizaRespaldo();
  }

  

  public marcaCheckSun(e: any){
    if(e.target.checked){
      for(let sunsystem of this.dataSunsystem){
        if(sunsystem.idSystem == e.target.value){
          this.listaSunsystem.push(sunsystem);
        }
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista sunsystem con checks: ", this.listaSunsystem);
  }

  public marcaCheckNet(e: any){
    if(e.target.checked){
      for(let netsuite of this.dataNetsuite){
        if(netsuite.idnetsuite == e.target.value){
          this.listaNetsuite.push(netsuite);
        }
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista netsuite con checks: ", this.listaNetsuite);
  }
  
  public marcaCheckComision(e: any){
    //console.log("dataPrimas ", this.dataPrimas);
    if(e.target.checked){
      for(let comisiones of this.dataComisiones){
        if(comisiones.idAgrupacion == e.target.value){
          console.log("primas.idAgrupacion ", comisiones.idAgrupacion);
          this.listaComisiones.push(comisiones);
        }
        //this.listaPrimas.push()
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista con checks: ", this.listaComisiones);
  }

  public marcaCheckDevolucion(e: any){
    //console.log("dataPrimas ", this.dataPrimas);
    if(e.target.checked){
      for(let devoluciones of this.dataDevoluciones){
        if(devoluciones.idAgrupacion == e.target.value){
          console.log("devoluciones.idAgrupacion ", devoluciones.idAgrupacion);
          this.listaDevoluciones.push(devoluciones);
        }
        //this.listaPrimas.push()
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista con checks: ", this.listaDevoluciones);
  }

  public marcaCheckRecuperos(e: any){
    //console.log("dataPrimas ", this.dataPrimas);
    if(e.target.checked){
      for(let recuperos of this.dataRecuperos){
        if(recuperos.idAgrupacion == e.target.value){
          console.log("recuperos.idAgrupacion ", recuperos.idAgrupacion);
          this.listaRecuperos.push(recuperos);
        }
        //this.listaPrimas.push()
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista con checks: ", this.listaRecuperos);
  }

  public marcaCheckGastos(e: any){
    //console.log("dataPrimas ", this.dataPrimas);
    if(e.target.checked){
      for(let gastos of this.dataGastos){
        if(gastos.idGastos == e.target.value){
          this.listaGastos.push(gastos);
        }
        //this.listaPrimas.push()
      }
    }
    // else{
    //   this.listaPrimas.slice(primas);
    // }
    console.log("asi queda lista con checks: ", this.listaGastos);
  }

  uploadListener($event: any): void {
    // let texto = [];
    const files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {
      const input = $event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      this.nombreArchivo = input.files[0].name;
      console.log('archivo: ' + this.nombreArchivo );
      reader.onload = () => {
        const csvData = reader.result;
       // let csvRecordsArray = (<string> csvData).split(/\r\n|\n/);
        const csvRecordsArray = (csvData as string).split(/\r\n|\n/);

        // imprime los titulos de las columnas
        this.headersRow = this.getHeaderArray(csvRecordsArray);
        console.log(this.headersRow);
        console.log('lenght: ' + this.headersRow.length);
        // validando cantidad de columnas
        if (this.headersRow.length === 16) {
        } else {
         // alert('numero de columnas incorrecto');
         // this.alertService.basic('numero de columnas incorrecto');
          this.alertaService.alertBasicIcon('Error', 'Numero de columnas incorrectos.', 'error');
        }

        this.records = this.getDataRecordsArrayFromCSVFile(
          csvRecordsArray,
          this.headersRow.length
        );
        console.log('records: ' + JSON.stringify(this.records));
        this.recuperos = this.records;
        this.spinner = true;
        this.recuperosService.buscarRecuperos(this.recuperos).subscribe((data: any) => {
          if(data.Status[0].code=="200"){
            this.spinner = false;
            console.log("data AGRUPACIONES RECUPEROS ", data);
            this.recuperosConci = data.Body.Groupings;
          }else{
            this.spinner = false;
            this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
          }
        }, error => {
          this.spinner = false;
          this.alertaService.alertBasicIcon('Recuperos Parciales', error, 'error');
        });
      };

      // tslint:disable-next-line:only-arrow-functions
      reader.onerror = function() {
        console.log('error is occured while reading file!');
      };
    } else {
      this.alertaService.alertBasicIcon('Error', 'La extensión del archivo es incorrecta, debe ser .csv', 'error');
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    const csvArr = [];
    this.cantidadRegistros = csvRecordsArray.length - 1;
    console.log('registros: ' + csvRecordsArray.length);

    for (let i = 1; i < csvRecordsArray.length; i++) {
     // let curruntRecord = (<string> csvRecordsArray[i]).split(';');
      const curruntRecord = (csvRecordsArray[i] as string).split(';');
      console.log('curruntRecord: ' + curruntRecord);
      console.log('columnas:' + curruntRecord.length);
      if (curruntRecord.length === headerLength) {
         const recupero: any = new Recuperos();

         console.log('current: ' + curruntRecord[0].trim());

       // gasto.push({subconcepto : curruntRecord[0].trim()});
         recupero.socio = curruntRecord[0].trim();
         recupero.operacion = curruntRecord[1].trim();
         recupero.rutAse = curruntRecord[2].trim();
         recupero.poliza = curruntRecord[3].trim();
         recupero.prima = curruntRecord[4].trim();
         recupero.montoDev = curruntRecord[5].trim();
         recupero.tipoDevolucion = curruntRecord[6].trim();
         recupero.mesCobertura = curruntRecord[7].trim();
         recupero.conInt = curruntRecord[8].trim();
         recupero.conRec = curruntRecord[9].trim();
         recupero.comMKT = curruntRecord[10].trim();
         recupero.comUCA = curruntRecord[11].trim(); 
         recupero.tipoComision = curruntRecord[12].trim(); 
         recupero.compania = curruntRecord[13].trim(); 
         recupero.numeroSolicitudCore = curruntRecord[14].trim(); 
         recupero.mesContableCore = curruntRecord[15].trim(); 

        //  recupero.cantidadRegistros = this.cantidadRegistros;
        //  recupero.nombreArchivo = this.nombreArchivo;
        //  recupero.usuario = sessionStorage.getItem('usuario');
        //  recupero.fechaCargArch = this.fechaCarga;

         csvArr.push(recupero);

      } else if (curruntRecord.length < headerLength ) {
        this.countRegister = this.countRegister + 1;
      }
    }
    // this.alertService.alertBasic('No se mostraran: ' + this.countRegister + 'no seran cargados debidos ');
   // this.alertService.alertBasicIcon(this.countRegister + 'registros', 'no se cargaran debido poseer mas de 1 columnas vacia', 'warning');
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith('.csv');
  }

  getHeaderArray(csvRecordsArr: any) {
    const headers = (csvRecordsArr[0] as string).split(';');
    const headerArray = [];
    // tslint:disable-next-line:prefer-for-of
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = '';
    this.records = [];
  }

  conciliar(){

    let optDifPrima = this.formValidaPrima.value.optDifPrima;
    console.log(" optDifPrima ", this.formValidaPrima.value.optDifPrima);

    console.log("check prio" , this.filtroFormGralD.value.checkPrio );
    console.log("this.filtroFormGralD.value.compania" , this.filtroFormGralD.value.compania );
    let ciaFiltro = this.formFiltros.compania;
    if(this.filtroFormGralD.value.compania != ""){
      switch(ciaFiltro){
        case "VIDA":ciaFiltro = "SVC";break;
        case "GENERALES":ciaFiltro = "SGC";break;
      }
    }
    
    console.log("this.filtroFormGralD.value.checkPrio " , this.filtroFormGralD.value.checkPrio);
    console.log("this.filtroFormGralD.value.compania " , this.filtroFormGralD.value.compania );
    console.log("ciaFiltro " , ciaFiltro );


    // if((this.montoConci > this.tolerancia) && (!this.filtroFormGralD.value.checkTolerancia)){
    //   this.alertaService.alertBasicIcon('Información', 'Está conciliando un neteo mayor a la tolerancia, marque el check de verificación para continuar.', 'info');
    if(this.filtroFormConci.valid){
      if(this.filtroFormGralD.value.compania != "" && (this.filtroFormGralD.value.compania != ciaFiltro) && !this.showButtonPRIO){
          this.alertaService.alertBasicIcon('Información', 'Esta operatoria no permite la opción PRIO', 'info');
      }else{
        if(this.filtroFormGralD.value.compania != "" && !this.filtroFormGralD.value.checkPrio && (this.filtroFormGralD.value.compania != ciaFiltro)){
          this.alertaService.alertBasicIcon('Información', 'Debe marcar check "PRIO" para poder conciliar compañías distintas.', 'info');
        }else{
          if((this.marcaPrima && this.filtroFormGralD.value.checkSalidaDinero) && this.sunsystemConci.length == 0){
            this.alertaService.alertBasicIcon('Información', 'Debe agregar cuenta de Sunsystem.', 'info');
          }else{
            console.log("this.marcaGasto " , this.marcaGasto);
            console.log("this.sunsystemConci.length " , this.sunsystemConci.length);
            console.log("this.requiereSunsystem " , this.requiereSunsystem);
            if(this.marcaGasto && this.sunsystemConci.length == 0 && this.requiereSunsystem == true){
              this.alertaService.alertBasicIcon('Información', 'Debe agregar cuenta de Sunsystem.', 'info');
            }else{
              let facturaList: Array<any> = [];
              facturaList = this.netsuiteConci.filter(net => net.tipodocumento == "FACTURA");
              if(facturaList.length == 0 && this.requiereRespaldo == true){
                this.alertaService.alertBasicIcon('Información', 'Debe agregar Documento Tributable (Factura) como respaldo.', 'info');
              }else{

                  // if(this.sunsystemConci.length > 0 && !this.filtroFormGralD.value.checkSalidaDinero){
                  //   this.alertaService.alertBasicIcon('Información', 'Debe marchar Check "Sin Salida De Dinero".', 'info');
                  // }else{
                    // if(this.marcaPrima && (this.filtroFormGralD.value.checkSalidaDinero == false )){
                    //   this.alertaService.alertBasicIcon('Información', 'Debe marchar Check "Sin Salida De Dinero".', 'info');
                    // }else{
                  console.log("montoConci " , this.montoConci);
                  console.log("optDifPrima ", optDifPrima);
                  console.log("(this.montoConci > this.tolerancia) || (this.montoConci < (Number(this.tolerancia) * -1)) ", (this.montoConci > this.tolerancia) || (this.montoConci < (Number(this.tolerancia) * -1)));
                  if(optDifPrima == "diferencia"){
                    if((this.montoConci > this.tolerancia) || (this.montoConci < (Number(this.tolerancia) * -1))){
                      this.alertaService.alertBasicIcon('Información', 'No puede conciliar por una diferencia mayor a los $' + this.tolerancia + ' de tolerancia.', 'info');
                    }else{
                      if((this.sumaTotProduccion <= 0) || (this.sumaTotRespaldo <= 0)){
                        this.alertaService.alertBasicIcon('Información', 'Solo puede conciliar totales en positivo.', 'info');
                      }else{
                        this.callConciliar();
                      }
                    }
                  }else{
                    if(optDifPrima == "exceso" || optDifPrima == "saldo"){
                      if(this.sumaTotProduccion > this.sumaTotRespaldo){
                        this.alertaService.alertBasicIcon('Información', 'Para "Exceso/Rechazo" y "Saldo" el total de respaldo debe ser mayor al de producción.', 'info');
                      }else{
                        if((this.sumaTotProduccion <= 0) || (this.sumaTotRespaldo <= 0)){
                          this.alertaService.alertBasicIcon('Información', 'Solo puede conciliar totales en positivo.', 'info');
                        }else{
                          this.callConciliar();
                        }
                      }
                    }else{
                      if((this.sumaTotProduccion <= 0) || (this.sumaTotRespaldo <= 0)){
                        this.alertaService.alertBasicIcon('Información', 'Solo puede conciliar totales en positivo.', 'info');
                      }else{
                        this.callConciliar();
                      }
                    }
                  }
                }
                // }
              // }
            }
          }
        }
      }
    }else{
      this.alertaService.alertBasicIcon('Conciliación', 'Debe seleccionar Periodo Contable', 'error');
    }

  }

  public callConciliar(){
    //return false;
    let netValido : string;
    let prio = "";
    let salidaDinero = "";
    let periodoContable = "";
    periodoContable = this.filtroFormConci.value.periodoContable;
    if(this.filtroFormGralD.value.checkPrio){prio = "1";}else{prio = "0";}
    if(this.filtroFormGralD.value.checkSalidaDinero){salidaDinero = "0";}else{salidaDinero = "1";}

    //if(this.configCombinacion.respaldo.indexOf("netsuite") >= 0){
    console.log("ingresa if netsuite");
    //netValido = "ok"; // cambiar por función this.validaRespaldo()
    netValido = this.validaRespaldo();

    console.log("ingresa 1298 netValido " , netValido);

    if(netValido == "ok"){

      this.setValidaciones();

      Swal.fire({
        title: 'Se ralizará la Conciliación',
        text: '¿Esta seguro que desea ejecutar la Conciliación?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Ejecutar!',
        cancelButtonText: 'Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner = true;
          this.conciliacionService.getConciliacion(this.primasConci, this.comisionesConci, this.devolucionesConci, this.recuperosConci, this.gastosConci, this.sunsystemConci, this.netsuiteConci, this.montoConci, prio, this.tolerancia, salidaDinero, periodoContable, this.arrValidacion).subscribe((data: any) =>{
            if(data.Status[0].code == "200"){
              this.spinner = false;
              this.alertaService.alertBasicIcon('Conciliación', data.Body.mensaje, 'info');
            }else{
              this.spinner = false;
              this.alertaService.alertBasicIcon('Conciliación', data.Body.mensaje, 'error');
            }
          });
          this.resetear();
        }
      });
    }else{
      this.spinner = false;
      this.alertaService.alertBasicIcon('Error', netValido, 'error');
    }
  }

  public setValidaciones(){

    this.arrValidacion = [];
    for(let valida of this.validaConciliacion){ 
        //console.log("NETEA-NETSUITE");
          
        if(valida.nombre === "MARCA-CHECK-DIF-MAYOR-TOLERANCIA"){
          var objVal = { idValidacion: valida.id };
          this.arrValidacion.push(objVal);
        }

        if(Math.round(Number(this.sumaTotProduccion)) <= Math.round(Number(this.sumaTotRespaldo))){
          if(valida.nombre === "TOT-PROD-MENORIGUAL-TOT-RESP"){
            var objVal = { idValidacion: valida.id };
            this.arrValidacion.push(objVal);
          }
        }

        if(Math.round(Number(this.sumaTotProduccion)) == Math.round(Number(this.sumaTotRespaldo))){
          if(valida.nombre === "NETEO-TOTAL"){
            var objVal = { idValidacion: valida.id };
            this.arrValidacion.push(objVal);
          }
        }
        
        if(this.filtroFormGralD.value.checkPrio){
          if(valida.nombre === "MARCA-CHECK-PRIO"){
            var objVal = { idValidacion: valida.id };
            this.arrValidacion.push(objVal);
          }
        }

        if(Math.round(Number(this.montoConci)) <= this.tolerancia){
          if(valida.nombre === "NETEO-DENTRO-DE-TOLERANCIA"){
            var objVal = { idValidacion: valida.id };
            this.arrValidacion.push(objVal);
          }
        }

        if(Math.round(Number(this.montoConci)) > Math.round(Number(this.tolerancia))){
          if(valida.nombre === "NETEO-MAYOR-A-TOLERANCIA"){
            var objVal = { idValidacion: valida.id };
            this.arrValidacion.push(objVal);
          }
        }
        //ACEPTA-SELECCIÃ“N-PERIODO-ANTERIOR
    }
  }

  public validaRespaldo(){
    //facturaList = this.netsuiteConci;
    let totMontoComision = 0;
    let totIvaComision = 0;
    let totMontoFactura = 0;
    let totIvaFactura = 0;

    let totMontoRecupero = 0;
    let totIvaRecupero = 0;
    let totMontoNotaCredito = 0;
    let totIvaNotaCredito = 0;

    let montoRespFactura = 0;
    let ivaRespFactura = 0;
    let montoRespNotaCredito = 0;
    let ivaRespNotaCredito = 0;

    let facturaList: Array<any> = [];
    let notaCreditoList: Array<any> = [];
    let totIvaGasto = 0;
    let totMontoGasto = 0;

    //se valida la cantidad de comisiones/facturas y que monto sean iguales o estén dentro de la tolerancia permitida
    facturaList = this.netsuiteConci.filter(net => net.tipodocumento == "FACTURA");
    console.log("facturaList.length " , facturaList.length);

    if(this.marcaGasto == true && this.marcaPrima == false && this.marcaComision == false && 
      this.marcaDevolucion == false && this.marcaRecupero == false){
        console.log("this.montoConci gasto " , this.montoConci);

        for(let gasto of this.gastosConci){
          totMontoGasto = totMontoGasto + Math.round(Number(gasto.bruto));
          totIvaGasto = totIvaGasto + Math.round(Number(gasto.iva));
        }

        for(let netsuite of this.netsuiteConci){
          totIvaFactura = totIvaFactura + Math.round(Number(netsuite.iva));
          totMontoFactura = totMontoFactura + Math.round(Number(netsuite.bruto));
        }

        console.log("totIvaGasto ", totIvaGasto);
        console.log("totIvaFactura ", totIvaFactura);

        montoRespFactura = Math.round(Number(totMontoGasto)) - Math.round(Number(totMontoFactura));
        ivaRespFactura = Math.round(Number(totIvaFactura)) - Math.round(Number(totIvaGasto));
        
        if(montoRespFactura > this.tolerancia || montoRespFactura < Number(this.tolerancia) * -1){
          return "Diferencia de Monto entre total Respaldo y Gastos, supera la tolerancia permitida.";
        }

        if(ivaRespFactura > this.tolerancia || ivaRespFactura < Number(this.tolerancia) * -1){
          return "Diferencia de Iva entre total Respaldo y Gastos, supera la tolerancia permitida.";
        }

    }else{
      if(this.comisionesConci.length > 0 && facturaList.length == 0){
        return "Debe agregar Documento Tributable (Factura).";
      }else if(this.comisionesConci.length == 0 && facturaList.length > 0 && this.gastosConci.length == 0){
        return "Debe agregar Comisiones.";
      }else{


        //AGREGAR VALIDACIÓN DE LAS FACTURAS VS GASTOS EJ: PRIMA-COMISION-GASTO
        for(let comision of this.comisionesConci){
          totMontoComision = totMontoComision + Math.round(Number(comision.brutoPesosPims)); 
          totIvaComision = totIvaComision + Math.round(Number(comision.ivaPesosPims));
        }

        for(let factura of facturaList){
          totMontoFactura = totMontoFactura + Math.round(Number(factura.bruto));
          totIvaFactura = totIvaFactura + Math.round(Number(factura.iva));
        }

        montoRespFactura = Math.round(Number(totMontoComision)) - Math.round(Number(totMontoFactura));
        ivaRespFactura = Math.round(Number(totIvaComision)) - Math.round(Number(totIvaFactura));
        console.log("Math.round(Number(totMontoComision)) ", Math.round(Number(totMontoComision)));
        console.log("Math.round(Number(totMontoFactura)) ", Math.round(Number(totMontoFactura)));
        console.log("Math.round(Number(montoRespFactura)) ", Math.round(Number(montoRespFactura)));
        console.log("Math.round(Number(totIvaComision)) ", Math.round(Number(totIvaComision)));
        console.log("Math.round(Number(totIvaFactura)) ", Math.round(Number(totIvaFactura)));
        console.log("Math.round(Number(ivaRespFactura)) ", Math.round(Number(ivaRespFactura)));
        console.log("if 1 ", montoRespFactura > this.tolerancia);
        console.log("tolerancia menos ", Number(this.tolerancia) * -1);

        if(montoRespFactura > this.tolerancia || montoRespFactura < (Number(this.tolerancia) * -1)){
          return "Diferencia de monto entre total Facturas y Comisiones, supera la tolerancia permitida.";
        }

        if(ivaRespFactura > this.tolerancia || ivaRespFactura < (Number(this.tolerancia) * -1)){
          return "Diferencia de iva entre total Facturas y Comisiones, supera la tolerancia permitida";
        }
      }

      //se valida la cantidad de recuperos/notas de credito y que monto sean iguales o estén dentro de la tolerancia permitida
      notaCreditoList = this.netsuiteConci.filter(net => net.tipodocumento == "NOTA DE CREDITO");
      console.log("facturaList.length " , notaCreditoList.length);
      // if(this.recuperosConci.length > 0 || notaCreditoList.length > 0 ){
      //   if(this.recuperosConci.length == notaCreditoList.length){
      if(this.recuperosConci.length > 0 && notaCreditoList.length == 0){
        return "Debe agregar Documento Tributable (Nota de Crédito).";
      }else if(this.recuperosConci.length == 0 && notaCreditoList.length > 0){
        return "Debe agregar Recuperos.";
      }else{
        for(let recupero of this.recuperosConci){
          totMontoRecupero = totMontoRecupero + Math.round(Number(recupero.brutoPesosPims)); 
          totIvaRecupero = totIvaRecupero + Math.round(Number(recupero.ivaPesosPims));
        }

        for(let notaCredito of notaCreditoList){
          totMontoNotaCredito = totMontoNotaCredito + Math.round(Number(notaCredito.bruto));
          totIvaNotaCredito = totIvaNotaCredito + Math.round(Number(notaCredito.iva));
        }

        montoRespNotaCredito = Math.round(Number(totMontoRecupero)) - Math.round(Number(totMontoNotaCredito));
        ivaRespNotaCredito = Math.round(Number(totIvaRecupero)) - Math.round(Number(totIvaNotaCredito));
        console.log("Math.round(Number(totMontoRecupero)) ", Math.round(Number(totMontoRecupero)));
        console.log("Math.round(Number(totMontoNotaCredito)) ", Math.round(Number(totMontoNotaCredito)));
        console.log("Math.round(Number(montoRespNotaCredito)) ", Math.round(Number(montoRespNotaCredito)));
        console.log("Math.round(Number(totIvaRecupero)) ", Math.round(Number(totIvaRecupero)));
        console.log("Math.round(Number(totIvaNotaCredito)) ", Math.round(Number(totIvaNotaCredito)));
        console.log("Math.round(Number(ivaRespNotaCredito)) ", Math.round(Number(ivaRespNotaCredito)));
        console.log("if 1 ", montoRespNotaCredito > this.tolerancia);
        console.log("tolerancia menos ", Number(this.tolerancia) * -1);

        if(montoRespNotaCredito > this.tolerancia || montoRespNotaCredito < (Number(this.tolerancia) * -1)){
          return "Diferencia de monto entre total Notas de Crédito y Recuperos, supera la tolerancia permitida";
        }

        if(ivaRespFactura > this.tolerancia || ivaRespFactura < (Number(this.tolerancia) * -1)){
          return "Diferencia de iva entre total Notas de Crédito y Recuperos, supera la tolerancia permitida";
        }
      }
    }

    return "ok";
  }

  public displayIcono() {
    if (this.formFiltros.compania !== '' && this.formFiltros.socio !== '') {
      this.icono = true;
    }  else {
      this.icono = false;
    }
  }

  public setValueFiltros(f: Filtros){
    this.formFiltros = f;
    this.displayIcono();
  }

  public resetear(){
    this.totalProduccion = [];
    this.sumaTotProduccion = 0;
    this.primasConci = [];
    this.comisionesConci = [];
    this.devolucionesConci = [];
    this.recuperosConci = [];
    this.gastosConci = [];
    this.totalRespaldo = [];
    this.sumaTotRespaldo = 0;
    this.sunsystemConci = [];
    this.netsuiteConci = [];
    this.showButtonPRIO = false;
    this.showButtonTolerancia = false;
    this.showButtonConci = false;
    this.showButtonSSDinero = false;
    this.showButtonSun = false;
    this.showButtonSun2 = false;
    this.showButtonNet = false;
    this.filtroFormSUN.value.cuenta = "";
    this.marcaPrima = false;
    this.marcaComision = false;
    this.marcaDevolucion = false;
    this.marcaRecupero = false;
    this.marcaGasto = false;
    this.marcaSunsystem = false;
    this.marcaNetsuite = false;
    //this.filtroFormSUN.reset();
    this.icono = false;
    console.log("this.formFiltros.compania " , this.formFiltros.compania);
    this.child.limpiar();
    this.fileReset();
    this.filtroFormConci.patchValue({
      periodoContable: this.periodoActual
    });
    this.filtroFormGralD.patchValue({
      compania: ''
    });
    //this.filtroFormSUN.reset();
    this.listarCompaniesSun();
  }
  
  checkUncheckAll(listaAgrupacion: any[]) {
    for (var i = 0; i < listaAgrupacion.length; i++) {
      listaAgrupacion[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList(listaAgrupacion);
  }
  
  isAllSelected(listaAgrupacion: any[]) {
    this.masterSelected = listaAgrupacion.every(function(item: any) {
        return item.isSelected === true;
      })
    this.getCheckedItemList(listaAgrupacion);
  }
  
  getCheckedItemList(listaAgrupacion: any[]){
    this.checkedList = [];
    for (var i = 0; i < listaAgrupacion.length; i++) {
      if (listaAgrupacion[i].isSelected) {
        this.checkedList.push(listaAgrupacion[i]);
      }
    }
    console.log("this.checkedList ", this.checkedList);
  }
  // checkUncheckAll(listaAgrupacion: any) {
  //   for (var i = 0; i < this.dataPrimas.length; i++) {
  //     this.dataPrimas[i].isSelected = this.masterSelected;
  //   }
  //   this.getCheckedItemList();
  // }
  
  // isAllSelected() {
  //   this.masterSelected = this.dataPrimas.every(function(item: any) {
  //       return item.isSelected === true;
  //     })
  //   this.getCheckedItemList();
  // }
  
  // getCheckedItemList(){
  //   this.checkedList = [];
  //   for (var i = 0; i < this.dataPrimas.length; i++) {
  //     if (this.dataPrimas[i].isSelected) {
  //       this.checkedList.push(this.dataPrimas[i]);
  //     }
  //   }
  //   console.log("this.checkedList ", this.checkedList);
  // }

  showSpinner(){
    this.spinner = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner = false;
    }, 3000);
  }
  // public onCompaniaGralChange(element){
  //   console.log("valor socio "+ element);

  // }

  // onChange(): void {
  //   this.conciForm.controls['companiaGral'].valueChanges.subscribe((value) => {

  //     console.log("value", value);
  //   });
  // }

}
