import { Component, ViewChild, OnInit } from '@angular/core';
import { CSVRecord } from 'src/app/model/csvrecord';
import { AlertasService } from 'src/app/services/alertas.service';
import { Gastos } from 'src/app/model/gastos';
import { FormGroup, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { GastosService } from 'src/app/services/gastos.service';
import { GlobalConstants } from 'src/app/model/global-constants';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit{

  @ViewChild('csvReader') csvReader: any;
  public records: any[] = [];
  public headersRow: any[] = [];
  public gastos: Gastos[];

  countRegister = 0;
  resume: string;
  term: string;
  search: string;

  p = 1;
  filas = GlobalConstants.filas;
  total = 0;

  todayNumber: number = Date.now();
  todayDate: Date = new Date();
  todayString: string = new Date().toDateString();
  todayISOString: string = new Date().toISOString();

  usuario: string = sessionStorage.getItem('usuario');
  fechaCarga: string;
  nombreArchivo: string;
  cantidadRegistros: number;


  public busquedaForm: FormGroup;

  constructor(
    private alerta: AlertasService,
    private service: GastosService
    ) {
    const datePipe = new DatePipe('en-US');
    this.fechaCarga = datePipe.transform(this.todayISOString, 'dd/MM/yyyy');
  }

  ngOnInit(): void {
    this.busquedaForm = new FormGroup({
      search: new FormControl(null),
      term: new FormControl(null)
    });
  }


  uploadListener($event: any): void {
    // let texto = [];
    let files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {
      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);
      this.nombreArchivo = input.files[0].name;
      console.log('archivo: ' + this.nombreArchivo );
      reader.onload = () => {
        let csvData = reader.result;
       // let csvRecordsArray = (<string> csvData).split(/\r\n|\n/);
        let csvRecordsArray = (csvData as string).split(/\r\n|\n/);

        // imprime los titulos de las columnas
        this.headersRow = this.getHeaderArray(csvRecordsArray);
        console.log(this.headersRow);
        console.log('lenght: ' + this.headersRow.length);
        // validando cantidad de columnas
        if (this.headersRow.length === 11) {
        } else {
         // alert('numero de columnas incorrecto');
         // this.alertService.basic('numero de columnas incorrecto');
          this.alerta.alertBasicIcon('Error', 'Numero de columnas incorrectos.', 'error');
        }

        this.records = this.getDataRecordsArrayFromCSVFile(
          csvRecordsArray,
          this.headersRow.length
        );
        console.log('records: ' + JSON.stringify(this.records));
        this.gastos = this.records;
        this.addGastos(this.gastos);
      };

      reader.onerror = function() {
        console.log('error is occured while reading file!');
      };
    } else {
      alert('La extension del archivo es incorrecta.');
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];
    this.cantidadRegistros = csvRecordsArray.length - 1;
    console.log('registros: ' + csvRecordsArray.length);

    for (let i = 1; i < csvRecordsArray.length; i++) {
     // let curruntRecord = (<string> csvRecordsArray[i]).split(';');
      let curruntRecord = (csvRecordsArray[i] as string).split(';');
      console.log('curruntRecord: ' + curruntRecord);
      console.log('columnas:' + curruntRecord.length);
      if (curruntRecord.length === headerLength) {
        // let csvRecord: CSVRecord = new CSVRecord();
        // csvRecord.id = curruntRecord[0].trim();
        // csvRecord.firstName = curruntRecord[1].trim();
        // csvRecord.lastName = curruntRecord[2].trim();
        // csvRecord.age = curruntRecord[3].trim();
        // csvRecord.position = curruntRecord[4].trim();
        // csvRecord.mobile = curruntRecord[5].trim();
        // csvArr.push(csvRecord);
         let gasto: any = new Gastos();

         console.log('current: ' + curruntRecord[0].trim());

       // gasto.push({subconcepto : curruntRecord[0].trim()});
         gasto.subconcepto = curruntRecord[0].trim();
         gasto.operatoria = curruntRecord[1].trim();
         gasto.compania = curruntRecord[2].trim();
         gasto.socio = curruntRecord[3].trim();
         gasto.policy = curruntRecord[4].trim();
         gasto.concepto = curruntRecord[5].trim();
         gasto.idProducto = 1;
         gasto.mesCobertura = curruntRecord[7].trim();
         gasto.primaAsociada = curruntRecord[8].trim();
         gasto.montoFinal = curruntRecord[9].trim();
         gasto.porcentaje = curruntRecord[10].trim();
         gasto.idPrdt = curruntRecord[6].trim();

         gasto.cantidadRegistros = this.cantidadRegistros;
         gasto.nombreArchivo = this.nombreArchivo;
         gasto.usuario = sessionStorage.getItem('usuario');
         gasto.fechaCargArch = this.fechaCarga;

         csvArr.push(gasto);

      } else if (curruntRecord.length < headerLength ) {
        this.countRegister = this.countRegister + 1;
      }
    }
    // this.alertService.alertBasic('No se mostraran: ' + this.countRegister + 'no seran cargados debidos ');
   // this.alertService.alertBasicIcon(this.countRegister + 'registros', 'no se cargaran debido poseer mas de 1 columnas vacia', 'warning');
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith('.csv');
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string> csvRecordsArr[0]).split(';');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = '';
    this.records = [];
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  public addGastos(g: any) {
    console.log();

    this.service.save(g).subscribe(
        (data) => {
          console.log('DATA:' + JSON.stringify(data));
          // alert('agregado con exito!');
          this.alerta.alertBasic2('Guardado con exito!', 'success');
        },
        (error) => {
          console.log('ERROR: ', error);
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );

  }

}
