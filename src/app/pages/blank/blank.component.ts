import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as xlsx from 'xlsx';

import { ExportToCsv } from 'export-to-csv';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbDatepicker,
} from '@ng-bootstrap/ng-bootstrap';

import { AlertasService } from 'src/app/services/alertas.service';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';

interface CheesyObject {
  id: number;
  text: string;
}
@Component({
  selector: 'app-blank',
  templateUrl: './blank.component.html',
  styleUrls: ['./blank.component.scss'],
})
export class BlankComponent implements OnInit {
  // ** para el datepicker ***
  model: NgbDateStruct;
  date: { year: number; month: number };
  @ViewChild('dp') dp: NgbDatepicker;
  // ****  ***** *****

  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  @ViewChild('epltable2', { static: false }) epltable2: ElementRef;

  title = 'adminlte';
  spinner: boolean;
  spinnerConfig: ISpinnerConfig;

  clubs = [
    {
      position: 1,
      name: 'Liverpool',
      played: 20,
      won: 19,
      drawn: 1,
      lost: 0,
      points: 58,
    },
    {
      position: 2,
      name: 'Leicester City',
      played: 21,
      won: 14,
      drawn: 3,
      lost: 4,
      points: 45,
    },
    {
      position: 3,
      name: 'Manchester City',
      played: 21,
      won: 14,
      drawn: 2,
      lost: 5,
      points: 44,
    },
    {
      position: 4,
      name: 'Chelsea',
      played: 21,
      won: 11,
      drawn: 3,
      lost: 7,
      points: 36,
    },
    {
      position: 5,
      name: 'Manchester United',
      played: 21,
      won: 8,
      drawn: 7,
      lost: 6,
      points: 31,
    },
  ];

  data = [
    {
      name: 'Test 1',
      age: 13,
      average: 8.2,
      approved: true,
      description: 'using Content here, content here',
    },
    {
      name: 'Test 2',
      age: 11,
      average: 8.2,
      approved: true,
      description: 'using Content here, content here ',
    },
    {
      name: 'Test 4',
      age: 10,
      average: 8.2,
      approved: true,
      description: 'using Content here, content here ',
    },
  ];

  masterSelected: boolean;
  checklist: any;
  checkedList: any;

  cheeseTypes: CheesyObject[];
  selectedCheese: number;

  constructor(private alerta: AlertasService) // private calendar: NgbCalendar
  {
    this.masterSelected = false;
    this.checklist = [
      { id: 1, value: 'Elenor Anderson', isSelected: false },
      { id: 2, value: 'Caden Kunze', isSelected: true },
      { id: 3, value: 'Ms. Hortense Zulauf', isSelected: true },
      { id: 4, value: 'Grady Reichert', isSelected: false },
      { id: 5, value: 'Dejon Olson', isSelected: false },
      { id: 6, value: 'Jamir Pfannerstill', isSelected: false },
      { id: 7, value: 'Aracely Renner DVM', isSelected: false },
      { id: 8, value: 'Genoveva Luettgen', isSelected: false },
    ];

    this.cheeseTypes = [
      {
        id: 1001,
        text: 'Roquefort',
      },
      {
        id: 1002,
        text: 'Camembert',
      },
      {
        id: 1003,
        text: 'Cotija',
      },
      {
        id: 1004,
        text: 'Mozzarella',
      },
      {
        id: 1005,
        text: 'Feta',
      },
    ];

    // Selected Value
    this.selectedCheese = 1004;
  }

  ngOnInit() {
    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: '6rem',
      color: '#1dab5a',
      bgColor: 'rgba(255,255,255,0.8)'
    };

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner = false;
    }, 3000);
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet = xlsx.utils.table_to_sheet(
      this.epltable.nativeElement
    );
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'epltable.xlsx');
  }

  exportToExcel2() {
    const ws: xlsx.WorkSheet = xlsx.utils.table_to_sheet(
      this.epltable2.nativeElement
    );
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'epltable2.xlsx');
  }

  // // ******** para date picker*************
  // selectToday() {
  //   this.model = this.calendar.getToday();
  // }

  // setCurrent() {
  //   // Current Date
  //   this.dp.navigateTo();
  // }
  // setDate() {
  //   // Set specific date
  //   this.dp.navigateTo({ year: 2013, month: 2 });
  // }

  // navigateEvent(event) {
  //   this.date = event.next;
  // }

  checkUncheckAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.masterSelected = this.checklist.every(function (item: any) {
      return item.isSelected === true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if (this.checklist[i].isSelected) {
        this.checkedList.push(this.checklist[i]);
      }
    }
    this.checkedList = JSON.stringify(this.checkedList);
  }

  getValues() {
    console.log(this.selectedCheese);
  }

  testAlerta() {
   // result = this.alerta.alertConfirmation();
  }

  showSpinner(){
    this.spinner = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner = false;
    }, 3000);
  }
  
  hideSpinner(){
    this.spinner = false;
  }
}
