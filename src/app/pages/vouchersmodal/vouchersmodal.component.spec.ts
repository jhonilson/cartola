import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VouchersmodalComponent } from './vouchersmodal.component';

describe('VouchersmodalComponent', () => {
  let component: VouchersmodalComponent;
  let fixture: ComponentFixture<VouchersmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VouchersmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VouchersmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
