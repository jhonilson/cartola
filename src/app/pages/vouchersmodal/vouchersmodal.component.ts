import { Component, OnInit, Input } from '@angular/core';
import { ContaPagoService } from 'src/app/services/conta-pago.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { AlertasService } from 'src/app/services/alertas.service';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';

@Component({
  selector: 'app-vouchersmodal',
  templateUrl: './vouchersmodal.component.html',
  styleUrls: ['./vouchersmodal.component.scss']
})
export class VouchersmodalComponent implements OnInit {

  @Input() voucherConciliacion: any[];
  
  spinner: boolean;
  spinnerConfig: ISpinnerConfig;
  element: HTMLElement;
  
  constructor(
    private contaPagoService: ContaPagoService,
    private alertaService: AlertasService,
  ) { }

  ngOnInit(): void {
    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: "6rem",
      color: "#1dab5a",
      bgColor: "rgba(255,255,255,0.8)"
    };
  }

  public voucherReintento(idVoucher: string, idContabilizacion: string){
    Swal.fire({
      title: 'Se va a Reintentar la Contabilización',
      text: '¿Está seguro que desea realizar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner = true;
        this.contaPagoService.getReintento(idVoucher, idContabilizacion).subscribe((data: any) => {
          if(data.Status[0].code == "200"){
            this.spinner = false;
            this.alertaService.alertBasicIcon('Reintento Contabilización', data.Body.detalle, 'info');
          }else{
            this.spinner = false;
            this.alertaService.alertBasicIcon('Reintento Contabilización', data.Body.detalle, 'error');
          }
        },
          (error) => {
            this.spinner = false;
            this.alertaService.alertBasicIcon('Reintento Contabilización', error, 'error');
          }
        );
      }
    });
  }

  public guardarIdVoucher(idVoucher: string){
    let resultado = null;
    console.log("idVoucher " , idVoucher);
    resultado = this.voucherConciliacion.find( voucher => voucher.voucherId === idVoucher);
    console.log("resultado " , resultado.voucher);
    if(resultado.voucher != undefined){
      Swal.fire({
        title: 'Se va a guardar el Id de Voucher manualmente',
        text: '¿Está seguro que desea realizar esta operación?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Ejecutar!',
        cancelButtonText: 'Cancelar',
      }).then((result) => {
        if (result.value) {
          
          this.contaPagoService.setVoucherManual(resultado).subscribe((data: any) => {
            console.log("data conta manual id voucher ", data);
            if(data.Status[0].code == "200"){
              this.spinner = false;
              this.alertaService.alertBasicIcon('Contabilización Manual', data.Body.detalle, 'info');
            }else{
              this.spinner = false;
              this.alertaService.alertBasicIcon('Contabilización Manual', data.Body.detalle, 'error');
            }
          },
            (error) => {
              this.spinner = false;
              this.alertaService.alertBasicIcon('Reintento Contabilización', error, 'error');
            }
          );
        }
      });
    }else{
      this.alertaService.alertBasicIcon('Id Voucher', 'Debe ingresar Id de Voucher', 'error');
    }
  }

  public onBlurMethod($event: any, voucherId: string){
    console.log("event.target.value ", $event.target.value);
    for (let i = 0; i < this.voucherConciliacion.length ; i++) {
      if(this.voucherConciliacion[i].voucherId == voucherId){
        this.voucherConciliacion[i].voucher = $event.target.value;
      }
    }

    console.log("this.voucherList ", this.voucherConciliacion);
  }

}
