import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlujoReversaComponent } from './flujo-reversa.component';

describe('FlujoReversaComponent', () => {
  let component: FlujoReversaComponent;
  let fixture: ComponentFixture<FlujoReversaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlujoReversaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlujoReversaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
