import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FiltrosService } from 'src/app/services/filtros.service';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as xlsx from 'xlsx';
import { DetalleConciliacion } from 'src/app/model/detalle-conciliacion';
import { AlertasService } from 'src/app/services/alertas.service';
import { Filtros } from 'src/app/model/filtros';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-flujo-reversa',
  templateUrl: './flujo-reversa.component.html',
  styleUrls: ['./flujo-reversa.component.scss']
})
export class FlujoReversaComponent implements OnInit {

  @ViewChild('concitable', { static: false }) concitable: ElementRef;

  conciliaciones: any[];
  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];
  conciliacionDetalle: any;
  conciliacionPrima: any[];
  conciliacionComision: any[];
  conciliacionDevolucion: any[];
  conciliacionRecupero: any[];
  conciliacionGastos: any[];
  conciliacionValidaciones: any[];
  conciliacionSunsystem: any[];
  conciliacionNetsuite: any[];

  items = [];
  pageOfItems: Array<any>;
  agrupaciones: any[];
  p = 1;
  searchCompany: string;
  searchSocio: string;
  searchConcepto: string;
  filas = GlobalConstants.filas;
  total = 0;
  idconciliacion: string;


  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private filtroService: FiltrosService,
    private conciliaService: ConciliacionService,
    private alerta: AlertasService
  ) {}

  ngOnInit(): void {

    // this.filtroForm = new FormGroup({
    //   compania: new FormControl(null, Validators.required),
    //   socio: new FormControl(null, Validators.required),
    //   mesCobertura: new FormControl(),
    //   poliza: new FormControl(),
    //   periodoContable: new FormControl(),
    //   estado: new FormControl(),
    //   concepto: new FormControl(),
    //   anoContable: new FormControl(),
    //   aprobacion: new FormControl()
    // });

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(null),
      searchSocio: new FormControl(null),
      searchConcepto: new FormControl(null)
    });

    // this.listarConciliacion();
  }

  public listarConciliacion(f: Filtros) {
    console.log('FORMULARIO:' + JSON.stringify(f));
    // this.total = this.agrupaciones.length;
    // if (this.filtroForm.valid) {

    this.conciliaService.getAllReverse(f).subscribe(
        (data: any) => {
          console.log('CONCILICIONES:' + JSON.stringify(data.Body));
         // this.conciliaciones = data.Body;
          // alert('funciona:  ' + data.Body);
          // this.total = data.Body.length;
          if(data.Body.length > 0){
            this.conciliaciones = data.Body;
          }else{
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      );

    // } else {
    //   this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son abligatorios!', 'error');
    // }
  }


  public detalleConciliacion(id: string) {
    console.log('ID:' + id);
    this.conciliaService.getDetalles(id).subscribe(
      (data) => {
        console.log('DetalleConciliacion: ' + JSON.stringify(data));
        this.conciliacionDetalle = data.Body;
        this.conciliacionPrima = data.Body[0].Conciliacion.Prima;
        this.conciliacionComision = data.Body[0].Conciliacion.Comision;
        this.conciliacionDevolucion = data.Body[0].Conciliacion.Devolucion;
        this.conciliacionRecupero = data.Body[0].Conciliacion.Recupero;
        this.conciliacionGastos = data.Body[0].Conciliacion.Gastos;
        this.conciliacionValidaciones = data.Body[0].Conciliacion.Validaciones;
        this.conciliacionSunsystem = data.Body[0].Conciliacion.Sunsystems;
        this.conciliacionNetsuite = data.Body[0].Conciliacion.Netsuite;

        console.log('SUSNSYSTEMS:' + this.conciliacionSunsystem);
        console.log('NETSUITE:' + JSON.stringify(this.conciliacionNetsuite));

      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.concitable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'conciliacion.xlsx');
   }


}
