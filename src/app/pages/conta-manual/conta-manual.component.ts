import { Component, OnInit, ɵConsole, ViewChild, ElementRef } from '@angular/core';
import { FiltrosService } from 'src/app/services/filtros.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResponseAgrupaciones } from 'src/app/model/response-agrupaciones';
import * as xlsx from 'xlsx';
import { AlertasService } from 'src/app/services/alertas.service';
import { Filtros } from 'src/app/model/filtros';
import { registerLocaleData } from '@angular/common';
import { GlobalConstants } from 'src/app/model/global-constants';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';

declare var $: any;

@Component({
  selector: 'app-conta-manual',
  templateUrl: './conta-manual.component.html',
  styleUrls: ['./conta-manual.component.scss']
})
export class ContaManualComponent implements OnInit {

  @ViewChild('agruptable', { static: false }) agruptable: ElementRef;
  @ViewChild('myDiv') myDiv: ElementRef<HTMLElement>;

  spinner: boolean;
  spinnerConfig: ISpinnerConfig;

  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];
  agrupaciones: any[];

  // agrupacion: ResponseAgrupaciones;
  searchCompany: string;
  searchSocio: string;
  searchConcepto: string;
  searchSubConcepto: string;
  searchPoliza: string;

  p = 1;
  filas = 5;
  total = 0;
  seleccionado = GlobalConstants.selected;

  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;
  public formIdVoucher: FormGroup;

  public esValidoSocio = true;
  public esValidoCompania = true;
  public idAgrupacion: string;
  voucherPattern = '^[0-9]*$';
  // voucherPattern = '^[a-z0-9_-]{4,15}$';
  filtros: Filtros;

  constructor(
    private filtroService: FiltrosService,
    private alerta: AlertasService
  ) { }

  ngOnInit(): void {

    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: '6rem',
      color: '#1dab5a',
      bgColor: 'rgba(255,255,255,0.8)'
    };

    this.formIdVoucher = new FormGroup({
      idVoucher: new FormControl('', [ Validators.maxLength(25), Validators.pattern(this.voucherPattern)])
    });

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(null),
      searchSocio: new FormControl(null),
      searchPoliza: new FormControl(null),
      searchConcepto: new FormControl(null),
      searchSubConcepto: new FormControl(null)
    });

    this.listarCompanies();
    this.listarCoberturas();
    this.listarSocios();
    this.listarPeriodos();
    this.listarEstados();
    this.listarConceptos();
    //  this.listarAgrupacion(this.filtroForm.value);

    registerLocaleData( 'es' );
  }

  public listarCompanies() {
    this.filtroService.getCompany().subscribe( (data: any) => {
      console.log('COMPAÑIAS:' + JSON.stringify(data.Body) );
      this.companias = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarCoberturas() {
    this.filtroService.getMesCobertura().subscribe( (data: any) => {
      console.log('COBERTURAS:' + JSON.stringify(data.Body) );
      this.mesesCobertura = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarSocios() {
    this.filtroService.getSocio().subscribe( (data: any) => {
      console.log('SOCIOS:' + JSON.stringify(data.Body) );
      this.socios = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarPeriodos() {
    this.filtroService.getPeriodoContable().subscribe( (data: any) => {
      console.log('PERIODOS:' + JSON.stringify(data.Body) );
      this.periodosContable = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarEstados() {
    this.filtroService.getEstado().subscribe( (data: any) => {
      console.log('ESTADOS:' + JSON.stringify(data.Body) );
      this.estados = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  public listarConceptos() {
    this.filtroService.getConcepto().subscribe( (data: any) => {
      console.log('CONCEPTOS:' + JSON.stringify(data.Body) );
      this.conceptos = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }


  public listarAgrupacion(f: Filtros) {
    // console.log('esValidoSocio ' , this.esValidoSocio);
    // console.log('validaForm() ' , this.validaForm());
    // if (this.filtroForm.valid) {
   // alert('entro de nuevo');
    console.log('FORMULARIO:' + JSON.stringify(f));
    this.filtros = f; // para ser usado despues que se agrega el voucher
    this.spinner = true;
    this.filtroService.getGroup(f).subscribe( (data: any) => {
        console.log('AGRUPACIONES:' + JSON.stringify(data.Body) );
        // this.agrupaciones = data.Body.Groupings;
        this.spinner = false;
        console.log("data.Body.Groupings.lenght " , data.Body.length);
        if(data.Body.Groupings.length > 0){
          this.agrupaciones = data.Body.Groupings;
        }else{
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      }, error => {
        this.spinner = false;
        console.log('ERROR: ', error);
      });
   // }
  }


  validaForm() {

    let formValido = true;
    console.log('select socio: ' , this.filtroForm.value.socio);
    if (this.filtroForm.value.socio == null || this.filtroForm.value.socio === '') { formValido = false; this.esValidoSocio = false; }
    // tslint:disable-next-line:max-line-length
    if (this.filtroForm.value.compania == null || this.filtroForm.value.compania === '') { formValido = false; this.esValidoCompania = false; }

    return formValido;

  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  selectChangeCompania(event: any) {
    if (!this.filtroForm.get('compania').invalid && (this.filtroForm.get('compania').touched || this.filtroForm.get('compania').dirty)) {
      this.esValidoCompania = true;
    }
  }

  selectChangeSocio(event: any) {
    if (!this.filtroForm.get('socio').invalid && (this.filtroForm.get('socio').touched || this.filtroForm.get('socio').dirty)) {
      this.esValidoSocio = true;
    }
  }

/*
  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.agruptable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'agruptable.xlsx');
   }
   */

  exportToExcel() {
     const ws: xlsx.WorkSheet =
     xlsx.utils.json_to_sheet(this.agrupaciones);
     const wb: xlsx.WorkBook = xlsx.utils.book_new();
     xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
     xlsx.writeFile(wb, 'contabilizacion.xlsx');
  }

  contabilizar(idAgrupacion) {
    this.spinner = true;
    this.filtroService.getContabilizacion(idAgrupacion).subscribe( (data: any) => {
      console.log('Respuesta Contabilizacion:', JSON.stringify(data.Body) );
     // this.alerta.alertBasicIcon('Respuesta Contabilizacion:', JSON.stringify(data.Body), 'info');
      this.spinner = true;
      this.alerta.alertBasicIcon('', JSON.stringify(data.Body.detalle), 'info');
    }, error => {
      console.log('ERROR: ', error);
      this.spinner = true;
      this.alerta.alertBasicIcon('Error', error.message, 'error');
    });
   }

  agregarIdVoucher(idAgrupacion) {
    console.log('agregarIdVoucher - idagrupacion ' , idAgrupacion);
  //  this.alerta.alertBasicIcon('agregarIdVoucher - idagrupacion:', idAgrupacion, 'info');
    this.idAgrupacion = idAgrupacion;
  }

  guardarIdVoucher() {
     console.log('guardarIdVoucher - idagrupacion ', this.idAgrupacion);
     console.log('idvoucher ', this.formIdVoucher.value.idVoucher);

     if (this.formIdVoucher.valid) {

      this.filtroService.getGuardarIdVoucher(this.idAgrupacion, this.formIdVoucher.value.idVoucher).subscribe( (data: any) => {
        console.log('Respuesta Contabilizacion Id Voucher:', JSON.stringify(data.Body) );
       // this.alerta.alertBasicIcon('Respuesta Id Voucher :', JSON.stringify(data.Body), 'info');
        this.alerta.alertBasicIcon('', JSON.stringify(data.Body.detalle), 'info');
        console.log('MENSAJE: ' + JSON.stringify(data.Body.detalle));
        console.log('FILTROS :' + JSON.stringify(this.filtros));
        this.listarAgrupacion(this.filtros);
        this.formIdVoucher.reset();
      }, error => {
        console.log('ERROR: ', error);
       // this.alerta.alertBasicIcon('Error', 'Voucher ' + 'this.formIdVoucher.value.idVoucher' + ' agregado sastifactoriamente', 'error');
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      });

      } else {
        this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario!', 'error');
      }


  }

  reversar(idAgrupacion) {
    this.spinner = true;
    this.filtroService.getReversa(idAgrupacion).subscribe((data: any) => {
       console.log('Respuesta Reversa: ', JSON.stringify(data.Body) );
    //   this.alerta.alertBasicIcon('Respuesta Reversa:', JSON.stringify(data.Body), 'info');
       this.spinner = false;
       this.alerta.alertBasicIcon('', JSON.stringify(data.Body.detalle), 'info');
     }, error => {
       console.log('ERROR: ', error);
       this.spinner = false;
       this.alerta.alertBasicIcon('Error', error.message, 'error');
     });
   }

   fireEvent(e) {
     // alert('entro');
     console.log('entro: ' + e );
     const el: HTMLElement = this.myDiv.nativeElement;
     el.click();
     // alert(' *** :' + el);

     // const keyPressed = e.keyCode;
     if (e !== null) {
        console.log('Escape!');
     }
   }



}
