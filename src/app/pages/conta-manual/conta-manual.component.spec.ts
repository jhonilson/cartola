import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaManualComponent } from './conta-manual.component';

describe('ContaManualComponent', () => {
  let component: ContaManualComponent;
  let fixture: ComponentFixture<ContaManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
