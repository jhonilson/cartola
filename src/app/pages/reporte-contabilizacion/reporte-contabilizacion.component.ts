import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as xlsx from 'xlsx';
import { FormGroup, FormControl } from '@angular/forms';
import { LoteService } from 'src/app/services/lote.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Fechas } from 'src/app/model/fechas';
import { ContabilizacionService } from 'src/app/services/contabilizacion.service';
import { ContabilizacionDetalles } from 'src/app/model/contabilizacion-detalles';
import { Contabilizacion } from 'src/app/model/contabilizacion';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-contabilizacion',
  templateUrl: './reporte-contabilizacion.component.html',
  styleUrls: ['./reporte-contabilizacion.component.scss']
})
export class ReporteContabilizacionComponent implements OnInit {

  @ViewChild('contatable', { static: false }) contatable: ElementRef;

  contalist: any[];
  contaDetalles: ContabilizacionDetalles[];
  p = 1;
  searchFechaIni: string;
  searchFechaFin: string;
  tipo: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados  = 'contabilizacion';

  public busquedaForm: FormGroup;

  constructor(
    private service: ContabilizacionService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchFechaIni: new FormControl(''),
      searchFechaFin: new FormControl(''),
      tipo: new FormControl('')
    });

    // this.listarConta();
  }

  public listarConta() {
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('COntables:' + JSON.stringify(data));
        this.contalist = data;
       // this.contaDetalle = data.detalle;
        if(data.length > 0 ){
          this.contalist = data;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarContaRango(f: Fechas) {
    this.service.getContabilizaciones(f).subscribe(
      (data: any) => {
        console.log('lotes:' + JSON.stringify(data));
        this.contalist = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public detalles(detalle: any){
     this.contaDetalles = detalle;
     // console.log('DETALLES: ' + JSON.stringify(detalle));
     console.log('DETALLES: ' + JSON.stringify(this.contaDetalles));
  }

/*   public loteDetalle(id: string) {
    console.log('IdLote: ' + id);
    this.service.getLoteDetalle(id).subscribe(
      (data: any) => {
        this.loteDetalles = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
        console.log('loteDetalle::' + JSON.stringify(this.loteDetalles));
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  } */


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.contatable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'auditoria.xlsx');
   }


}
