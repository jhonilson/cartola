import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteContabilizacionComponent } from './reporte-contabilizacion.component';

describe('ReporteContabilizacionComponent', () => {
  let component: ReporteContabilizacionComponent;
  let fixture: ComponentFixture<ReporteContabilizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteContabilizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteContabilizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
