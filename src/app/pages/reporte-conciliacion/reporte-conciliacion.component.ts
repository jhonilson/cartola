import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FiltrosService } from 'src/app/services/filtros.service';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as xlsx from 'xlsx';
import { DetalleConciliacion } from 'src/app/model/detalle-conciliacion';
import { AlertasService } from 'src/app/services/alertas.service';
import { Filtros } from 'src/app/model/filtros';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-reporte-conciliacion',
  templateUrl: './reporte-conciliacion.component.html',
  styleUrls: ['./reporte-conciliacion.component.scss'],
})
export class ReporteConciliacionComponent implements OnInit {
  @ViewChild('concitable', { static: false }) concitable: ElementRef;

  conciliaciones: any[];
  companias: any[];
  mesesCobertura: any[];
  socios: any[];
  periodosContable: any[];
  estados: any[];
  conceptos: any[];
  conciliacionDetalle: any;
  conciliacionPrima: any[];
  conciliacionComision: any[];
  conciliacionDevolucion: any[];
  conciliacionRecupero: any[];
  conciliacionGastos: any[];
  conciliacionValidaciones: any[];
  conciliacionSunsystem: any[];
  conciliacionNetsuite: any[];

  items = [];
  pageOfItems: Array<any>;
  agrupaciones: any[];
  p = 1;
  searchCompany: string;
  searchSocio: string;
  searchConcepto: string;
  filas = 5;
  total = 0;
  idconciliacion: string;


  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private filtroService: FiltrosService,
    private conciliaService: ConciliacionService,
    private alerta: AlertasService
  ) {}

  ngOnInit(): void {

    // this.filtroForm = new FormGroup({
    //   compania: new FormControl(null, Validators.required),
    //   socio: new FormControl(null, Validators.required),
    //   mesCobertura: new FormControl(),
    //   poliza: new FormControl(),
    //   periodoContable: new FormControl(),
    //   estado: new FormControl(),
    //   concepto: new FormControl(),
    //   anoContable: new FormControl(),
    //   aprobacion: new FormControl()
    // });

    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(null),
      searchSocio: new FormControl(null),
      searchConcepto: new FormControl(null)
    });

    // this.listarConciliacion();
  }

  public listarConciliacion(f: Filtros) {
    console.log('FORMULARIO:' + JSON.stringify(f));
    // this.total = this.agrupaciones.length;
    // if (this.filtroForm.valid) {

    this.conciliaService.getAll(f).subscribe(
        (data: any) => {
          console.log('CONCILICIONES:' + JSON.stringify(data.Body));
          // this.conciliaciones = data.Body;
          // alert('funciona:  ' + data.Body);
          // this.total = data.Body.length;
          if(data.Body.length > 0){
            this.conciliaciones = data.Body;
          }else{
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      );

    // } else {
    //   this.alerta.alertBasicIcon('Error', 'Campos Socio y Compañia son abligatorios!', 'error');
    // }
  }


  public detalleConciliacion(id: string) {
    console.log('ID:' + id);
    this.conciliaService.getDetalles(id).subscribe(
      (data) => {
        console.log('DetalleConciliacion: ' + JSON.stringify(data));
        this.conciliacionDetalle = data.Body;
        this.conciliacionPrima = data.Body[0].Conciliacion.Prima;
        this.conciliacionComision = data.Body[0].Conciliacion.Comision;
        this.conciliacionDevolucion = data.Body[0].Conciliacion.Devolucion;
        this.conciliacionRecupero = data.Body[0].Conciliacion.Recupero;
        this.conciliacionGastos = data.Body[0].Conciliacion.Gastos;
        this.conciliacionValidaciones = data.Body[0].Conciliacion.Validaciones;
        this.conciliacionSunsystem = data.Body[0].Conciliacion.Sunsystems;
        this.conciliacionNetsuite = data.Body[0].Conciliacion.Netsuite;

        console.log('SUSNSYSTEMS:' + this.conciliacionSunsystem);
        console.log('NETSUITE:' + JSON.stringify(this.conciliacionNetsuite));

      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public accionAprobar(id: string) {
    console.log('ID:' + id);

    Swal.fire({
      title: 'Usted aprobará la conciliación',
      text: '¿Está seguro que desea ejecutar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {

        this.conciliaService.aprobarConciliacion(id).subscribe(
          (data) => {
            console.log('Accion: ' + JSON.stringify(data));
            if(data.Status[0].code == "200"){
              //this.accion = data.Body;
              this.alerta.alertBasic2( 'Conciliación Aprobada con Éxito!', 'success');
            }else{
              this.alerta.alertBasicIcon('Error', data.Body.mensaje, 'error');  
            }
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );
        
      }
    });

  }

  public accionRechazar(id: string) {
    console.log('ID:' + id);

    Swal.fire({
      title: 'Se va a Rechazar la Conciliación',
      text: '¿Está seguro que desea ejecutar esta operación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.conciliaService.rechazarConciliacion(id).subscribe(
          (data) => {
            console.log('Accion: ' + JSON.stringify(data));
            if(data.Status[0].code == "200"){
              this.alerta.alertBasic2(data.Body.mensaje, 'success');
            }else{
              this.alerta.alertBasicIcon('Error', data.Body.mensaje, 'error');  
            }
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );

      }
    });

  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.concitable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'conciliacion.xlsx');
   }



}
