import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteConciliacionComponent } from './reporte-conciliacion.component';

describe('ReporteConciliacionComponent', () => {
  let component: ReporteConciliacionComponent;
  let fixture: ComponentFixture<ReporteConciliacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteConciliacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteConciliacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
