import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as xlsx from 'xlsx';
import { GlobalConstants } from 'src/app/model/global-constants';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuditoriaService } from 'src/app/services/auditoria.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { Fechas } from 'src/app/model/fechas';
import { Devolucion } from 'src/app/model/devolucion';
import { DevolucionService } from 'src/app/services/devolucion.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FiltrosService } from 'src/app/services/filtros.service';
import { SociosService } from 'src/app/services/socios.service';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';

import { Carta1content } from 'src/app/model/carta1content';
import { Carta1detalle } from 'src/app/model/carta1detalle';
import { Carta1total } from 'src/app/model/carta1total';
import { CartaService } from 'src/app/services/carta.service';
import { Carta2detalle } from 'src/app/model/carta2detalle';
import { Carta2content } from 'src/app/model/carta2content';

@Component({
  selector: 'app-devolucion-pago',
  templateUrl: './devolucion-pago.component.html',
  styleUrls: ['./devolucion-pago.component.scss']
})
export class DevolucionPagoComponent implements OnInit {

  @ViewChild('SinTitulo', { static: false }) SinTitulo: ElementRef;
  @ViewChild('carta1', { static: false }) carta1: ElementRef;
  @ViewChild('carta2', { static: false }) carta2: ElementRef;

  var: string = '';
  distinctThings = [];
  nrequer:any[];
  spinner: boolean;
  spinnerConfig: ISpinnerConfig;

  devoluciones: Devolucion[] = null;
  pagos: any[];
  p = 1;
  idAgrupacion: string;
  montoPago: string;
  beneficiario: string;
  tipoCuenta: string;
  rut: string;
  ruta = '';
  sumatoria = 0;

  filas = GlobalConstants.filas;
  total = 0;
  // formFiltros: Fechas;
  companias: any[];
  mediosPago: any[];
  socios: any[];
  sociosPago: any[];

  masterSelected: boolean;
  checklist: any;
  checkedList: Devolucion[];

  cartaContent: Carta1content[];
  cartaDetalle: Carta1detalle[];
  cartaTotales: Carta1total[];
  carta2Content: Carta2content[];
  carta2Detalle: Carta2detalle[];

  public filtroForm: FormGroup;
  public busquedaForm: FormGroup;

  constructor(
    private service: DevolucionService,
    private filtroService: FiltrosService,
    private socioService: SociosService,
    private alerta: AlertasService,
    private sanitizer: DomSanitizer,
    private cartaService: CartaService
  ) { }

  ngOnInit(): void {

    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: '6rem',
      color: '#1dab5a',
      bgColor: 'rgba(255,255,255,0.8)'
    };


    this.filtroForm = new FormGroup({
      compania: new FormControl('', Validators.required),
      socio: new FormControl('', Validators.required),
      portalBancario: new FormControl('', Validators.required),
      medioPago: new FormControl(''),
      pagara: new FormControl('')
    //  tipoCliente: new FormControl(''),

      // estado: new FormControl(''),
      // concepto: new FormControl(''),
      // subConcepto: new FormControl(''),
      // anoContable: new FormControl(''),
      // aprobacion: new FormControl('')
    });

    this.busquedaForm = new FormGroup({
      montoPago: new FormControl(null),
      idAgrupacion: new FormControl(null),
      beneficiario: new FormControl(null),
      tipoCuenta: new FormControl(null),
      rut: new FormControl(null)
    });

    // this.listarDevolucion();
    this.listarCompanies();
    this.listarSocios();
    this.listarMediosPago();
    this.listarSociosPagos();
  //  this.listarDevolucion();
  }

  public listarDevolucion() {
    console.log(this.filtroForm.value);
    this.spinner = true;
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('pagos:' + JSON.stringify(data));
        this.devoluciones = data;
        this.spinner = false;
      },
      (error) => {
        this.spinner = false;
        console.log('ERROR: ', error);
      }
    );
  }

  public listarPagos() {
    console.log(this.filtroForm.value);
    this.spinner = true;
    this.service.getPagos(this.filtroForm.value).subscribe(
      (data: any) => {        
        console.log('pagos:' + JSON.stringify(data));
        if (data.length > 0) {
          this.devoluciones = data;
          this.spinner = false;
        } else {
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        this.spinner = false;
        console.log('ERROR: ', error);
      }
    );
  }

  public generarNomina() {
    console.log(this.pagos);

    if (this.filtroForm.valid) {
        if (typeof this.pagos === 'undefined') {
          this.spinner = false;
          this.alerta.alertBasicIcon('Error', 'Debe seleccionar pagos del listado', 'error');
        } else {
        this.spinner = true;
        console.log('form: ' + JSON.stringify(this.filtroForm.value));
        this.service.crearNomina(this.filtroForm.value, this.pagos).subscribe(
          (data: any) => {
            console.log(data);
            this.ruta = data.ruta;
            this.spinner = false;
            this.alerta.alertBasic2('Nomina generada con exito!', 'success');
          }, error => {
            console.log('ERROR: ', error);
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );

        }
      } else {
        this.alerta.alertBasicIcon('Error', 'Hay errores en el formulario', 'error');
      }

  }

  public generarNomina2() {
    console.log('FORMULARIO: ' + JSON.stringify(this.filtroForm.value));

    this.service.crearNomina2(this.filtroForm.value, this.pagos).subscribe(
      (data: any) => {
        console.log(data);
        this.ruta = data.ruta;
      }, error => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }


  sanitizar(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.SinTitulo.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'sin_titulo.xlsx');
  }

  // ************************************************** */

  public listarCompanies() {
    this.filtroService.getCompany().subscribe(
      (data: any) => {
        console.log('COMPAÑIAS:' + JSON.stringify(data.Body));
        this.companias = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSocios() {
    this.filtroService.getSocio().subscribe(
      (data: any) => {
        console.log('SOCIOS:' + JSON.stringify(data.Body));
        this.socios = data.Body;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarSociosPagos() {
    this.socioService.getSociosPago().subscribe(
      (data: any) => {
        console.log('SOCIOS:' + JSON.stringify(data));
        this.sociosPago = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public listarMediosPago() {
    this.filtroService.getMediosPago().subscribe((data: any) => {
      console.log('MEDIOSPAGO:' + JSON.stringify(data.Body));
      this.mediosPago = data.Body;
    }, error => {
      console.log('ERROR: ', error);
    });
  }

  // ******************  CARTAS 1 **************************************

  public getCarta1Content(){
    if (this.filtroForm.get('socio').value !== '' && this.filtroForm.get('socio').value !== null) {
      this.cartaService.getCarta1Content(this.filtroForm.get('socio').value).subscribe((data: any) => {
        this.cartaContent = data;
        console.log('CARTA_CONTENT: ' + JSON.stringify(this.cartaContent));

        this.distinctThings = this.cartaContent.filter((thing, i, arr) => {
          return arr.indexOf(arr.find(t => t.panNorequerimiento === thing.panNorequerimiento)) === i;
        });
        console.log('DISTINTOS: '+ JSON.stringify(this.distinctThings));

        this.nrequer = this.cartaContent
                 .map(item => item.panNorequerimiento)
                 .filter((value, index, self) => self.indexOf(value) === index);
        console.log('DISTINTOS: ' + this.nrequer);

        this.getCarta1Totales();
      });
    } else {
      this.alerta.alertBasic2('Debe seleccionar un Socio', 'error');
    }
  }

  public getCarta1Detalles(){
    if (this.filtroForm.get('socio').value !== '' && this.filtroForm.get('socio').value !== null) {
      this.cartaService.getCarta1Detalle(this.filtroForm.get('socio').value).subscribe((data: any) => {
        this.cartaDetalle = data;
        console.log('CARTA_DETALLE: ' + JSON.stringify(this.cartaDetalle));
        this.getCarta1Content();
      });
    } else {
      this.alerta.alertBasic2('Debe seleccionar un Socio', 'error');
    }
  }

  public getCarta1Totales(){
    if (this.filtroForm.get('socio').value !== '' && this.filtroForm.get('socio').value !== null) {
      this.cartaService.getCarta1Total(this.filtroForm.get('socio').value).subscribe((data: any) => {
        this.cartaTotales = data;
        console.log('CARTA_TOTALES: ' + JSON.stringify(this.cartaTotales));
      });
    } else {
      this.alerta.alertBasic2('Debe seleccionar un Socio', 'error');
    }
  }  

  exportToExcelCarta1() {
    const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.carta1.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'sin_titulo.xlsx');
  }

    // ******************  CARTAS 2 **************************************
    public getCarta2Content(){
      if (this.filtroForm.get('socio').value !== '' && this.filtroForm.get('socio').value !== null) {
        this.cartaService.getCarta2Content(this.filtroForm.get('socio').value).subscribe((data: any) => {
          this.carta2Content = data;
          console.log('CARTA_CONTENT: ' + JSON.stringify(this.cartaContent));
  
          // this.distinctThings = this.cartaContent.filter((thing, i, arr) => {
          //   return arr.indexOf(arr.find(t => t.panNorequerimiento === thing.panNorequerimiento)) === i;
          // });
          // console.log('DISTINTOS: '+ JSON.stringify(this.distinctThings));
  
          // this.nrequer = this.cartaContent
          //          .map(item => item.panNorequerimiento)
          //          .filter((value, index, self) => self.indexOf(value) === index);
          // console.log('DISTINTOS: ' + this.nrequer);
        });
      } else {
        this.alerta.alertBasic2('Debe seleccionar un Socio', 'error');
      }
    }


    public getCarta2Detalles(){
      if (this.filtroForm.get('socio').value !== '' && this.filtroForm.get('socio').value !== null) {
        this.cartaService.getCarta2Detalle(this.filtroForm.get('socio').value).subscribe((data: any) => {
          this.carta2Detalle = data;
          console.log('CARTA_DETALLE: ' + JSON.stringify(this.cartaDetalle));
          this.getCarta2Content();
        });
      } else {
        this.alerta.alertBasic2('Debe seleccionar un Socio', 'error');
      }
    }

    exportToExcelCarta2() {
      const ws: xlsx.WorkSheet =
        xlsx.utils.table_to_sheet(this.carta2.nativeElement);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
      xlsx.writeFile(wb, 'sin_titulo.xlsx');
    }

    // ********************************************************

  checkUncheckAll() {
    for (var i = 0; i < this.devoluciones.length; i++) {
      this.devoluciones[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.masterSelected = this.devoluciones.every(function (item: any) {
      return item.isSelected === true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    this.sumatoria = 0;
    for (var i = 0; i < this.devoluciones.length; i++) {
      if (this.devoluciones[i].isSelected) {
        this.checkedList.push(this.devoluciones[i]);
      }
    }
    // this.checkedList = JSON.stringify(this.checkedList);
    this.pagos = this.checkedList;
    this.sumar(this.checkedList);
    // for (let index = 0; index < this.checkedList.length; index++) {
    //   const element = this.checkedList[index];
    //   this.sumatoria = this.sumatoria + Math.round(Number(element.panMontopago));
    // }
    // console.log('SUMATORIAplus: ' + this.sumatoria);
  }

  public sumar(lista: Devolucion[]) {
    for (let index = 0; index < lista.length; index++) {
      const element = lista[index];
      this.sumatoria = this.sumatoria + Math.round(Number(element.panMontopago));
    }
    console.log('SUMATORIA: ' + this.sumatoria);
  }

}
