import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalconciliacionComponent } from './modalconciliacion.component';

describe('ModalconciliacionComponent', () => {
  let component: ModalconciliacionComponent;
  let fixture: ComponentFixture<ModalconciliacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalconciliacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalconciliacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
