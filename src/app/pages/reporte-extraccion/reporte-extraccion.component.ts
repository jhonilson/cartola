import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Fechas } from 'src/app/model/fechas';
import { FormGroup, FormControl } from '@angular/forms';
import { AuditoriaService } from 'src/app/services/auditoria.service';
import { AlertasService } from 'src/app/services/alertas.service';
import * as xlsx from 'xlsx';
import { LoteService } from 'src/app/services/lote.service';
import { GlobalConstants } from 'src/app/model/global-constants';

@Component({
  selector: 'app-reporte-extraccion',
  templateUrl: './reporte-extraccion.component.html',
  styleUrls: ['./reporte-extraccion.component.scss']
})
export class ReporteExtraccionComponent implements OnInit {

  @ViewChild('lotetable', { static: false }) lotetable: ElementRef;

  lotes: any[];
  loteDetalles: any[];
  p = 1;
  searchFechaIni: string;
  searchFechaFin: string;
  estatus: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados = '';

  public busquedaForm: FormGroup;

  constructor(
    private service: LoteService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchFechaIni: new FormControl(null),
      searchFechaFin: new FormControl(null),
      estatus: new FormControl(null)
    });

    // this.listarAuditoria();
  }

  public listarLote(f: Fechas) {
    this.service.getLotes(f).subscribe(
      (data: any) => {
        console.log('lotes:' + JSON.stringify(data));
        this.lotes = data;
        if(data.length > 0 ){
          this.lotes = data;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public loteDetalle(id: string) {
    console.log('IdLote: ' + id);
    this.service.getLoteDetalle(id).subscribe(
      (data: any) => {
        this.loteDetalles = data;
        if (data.length === 0) {
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
        console.log('loteDetalle::' + JSON.stringify(this.loteDetalles));
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.lotetable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'auditoria.xlsx');
   }

}
