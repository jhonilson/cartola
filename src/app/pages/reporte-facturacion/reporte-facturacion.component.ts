import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EntidadService } from 'src/app/services/entidad.service';
import { SociosService } from 'src/app/services/socios.service';
import { Router } from '@angular/router';
import { Entidad } from 'src/app/model/entidad';
import { Socio } from 'src/app/model/socio';
import { FiltrosService } from 'src/app/services/filtros.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { Filtros } from 'src/app/model/filtros';
import * as xlsx from 'xlsx';
import { GlobalConstants } from 'src/app/model/global-constants';
import { AlertasService } from 'src/app/services/alertas.service';

@Component({
  selector: 'app-reporte-facturacion',
  templateUrl: './reporte-facturacion.component.html',
  styleUrls: ['./reporte-facturacion.component.scss']
})
export class ReporteFacturacionComponent implements OnInit {

  @ViewChild('auditable', { static: false }) auditable: ElementRef;
  archivos: any[];
  numero: string;
  busquedaForm: FormGroup;
  searchFile: string;

  filas = GlobalConstants.filas;
  total = 0;
  p = 1;
  
  constructor(
    private service: FiltrosService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private alerta: AlertasService
  ) { }

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      searchFile: new FormControl(null)
    });

   // this.listarFiles();
  }


  public listarFiles() {
    this.service.getFiles().subscribe( (data: any[]) => {
      console.log('datos:' , data);
      this.archivos = data;
      if(data.length > 0 ){
        this.archivos = data;
      }else{
        this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
      }
    }, error => {
      console.log('ERROR: ', error);
    });
  }


  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.auditable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'auditoria.xlsx');
   }

// ***********************

  sanitizar(url: string) {
  return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
