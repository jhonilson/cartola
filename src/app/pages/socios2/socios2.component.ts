import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Socio } from 'src/app/model/socio';
import { SociosService } from 'src/app/services/socios.service';
import { Router } from '@angular/router';
import { Entidad } from 'src/app/model/entidad';
import { EntidadService } from 'src/app/services/entidad.service';
import { AlertasService } from 'src/app/services/alertas.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-socios2',
  templateUrl: './socios2.component.html',
  styleUrls: ['./socios2.component.scss'],
})
export class Socios2Component implements OnInit {
  public socio: Socio = new Socio();
  public socios: Socio[];
  public entidades: Entidad[];
  public socioForm: FormGroup;

  validacion = false;
  numero: number;
  digito: string;
 // rutPattern = '^(\b(d{1,3}(?:(.?)d{3}){2}(-?)[dkK])\b)$';
  // nombrePattern = '^[a-zA-Z]*$';
  detalle: string; 

  constructor(
    private socioService: SociosService,
    private entidadService: EntidadService,
    private alerta: AlertasService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.socioForm = new FormGroup({
      idsocio: new FormControl(''),
      nombregeneral: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]),
      descripcion: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(100),
      ]),
      rut: new FormControl('', [Validators.required]),
    });
    /*rut: ['30972198', [Validators.required, rutValidator]]
      public idsocio: string;
  public razonSocial: string;
  public rut: string;
  public cuentaContable: string;
  public descripcion: string;
  */
    this.listarSocios();
  }

  public listarSocios() {
    this.socioService.getAll().subscribe(
      (data: any) => {
        console.log(data);
        this.socios = data;
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public addSocio() {
    console.log(this.socioForm.value);
    if (this.socioForm.valid) {
      this.socioService.save(this.socioForm.value).subscribe(
        (data) => {
          console.log('DATA:' + JSON.stringify(data));
          // alert('agregado con exito!');
          this.alerta.alertBasic2('agregado con exito!', 'success');
          this.onResetForm();
          this.listarSocios();
        },
        (error) => {
          console.log('ERROR: ', error);
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );
    } else {
      this.alerta.alertBasicIcon(
        'Error',
        'Hay errores en el formulario',
        'error'
      );
    }
  }

  public deleteSocio(s: Socio) {
    console.log('socioID:' + s.idsocio);

    Swal.fire({
      title: '¿Esta seguro que desea ejecutar esta operacion?',
      text: 'Esta no se podra revertir!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {

        this.socioService.delete(s.idsocio).subscribe(
          (data) => {
            // alert('eliminado con exito!');
            this.alerta.alertBasic2('Eliminado con Exito!', 'success');
            sessionStorage.removeItem('confirm');
            console.log('confirmacion2: ' + confirm);
            this.listarSocios();
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );
        
      }
    });
  }

  editar(s: Socio): void {
    console.log('RolID:' + s.idsocio);
    this.socioService.findById(+s.idsocio).subscribe(
      (data) => {
        console.log('DATA:' + JSON.stringify(data));
        this.socio = data;
        this.loadData();
      },
      (error) => {
        console.log('ERROR: ', error.message);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  loadData() {
    this.socioForm.setValue({
      idsocio: this.socio.idsocio,
      nombregeneral: this.socio.nombregeneral,
      descripcion: this.socio.descripcion,
      rut: this.socio.rut,
    });
  }

  update() {
    // if (this.rolForm.valid) {

    console.log(this.socioForm.value);
    if (this.socioForm.valid) {
      this.socioService.update(this.socioForm.value).subscribe(
        (data) => {
          // alert('Se modifico con Exito!!');
          // alert('Se modifico con Exito!');
          this.alerta.alertBasic2('Se modifico con exito!', 'success');
          this.onResetForm();
          this.listarSocios();
        },
        (error) => {
          console.log('ERROR: ', error);
          //  alert('Error' +  error.message);
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );
    } else {
      this.alerta.alertBasicIcon(
        'Error',
        'Debe llenar todos los campos de formulario!',
        'error'
      );
    }
  }

  public entidadBySocio(s: Socio) {
    console.log('ID:' + s.idsocio);
    this.detalle = s.descripcion;
    this.entidadService.entBySocio(s.idsocio).subscribe(
      (data) => {
        console.log('entidadBySocio: ' + JSON.stringify(data));
        this.entidades = data;
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  onResetForm() {
    this.socioForm.reset();
    this.socio.idsocio = null;
  }

  validaRut(e: any) {
    this.validacion = false;

    e = e.toUpperCase();
    e = e.replace('.', '');
    e = e.replace('-', '');
    this.numero = Number(e.substring(1, e.length() - 1));

    this.digito = e.charAt(e.length() - 1);

    // let m = 0;
    // let s = 1;
  }

  //   formateaRut(rut) {

  //     let actual = rut.replace(/^0+/, '');
  //     if (actual != '' && actual.length > 1) {
  //         let sinPuntos = actual.replace(/\./g, '');
  //         let actualLimpio = sinPuntos.replace(/-/g, '');
  //         let inicio = actualLimpio.substring(0, actualLimpio.length - 1);
  //         let rutPuntos = '';
  //         let i = 0;
  //         let j = 1;
  //         for (i = inicio.length - 1; i >= 0; i--) {
  //             var letra = inicio.charAt(i);
  //             rutPuntos = letra + rutPuntos;
  //             if (j % 3 === 0 && j <= inicio.length - 1) {
  //                 rutPuntos = '.' + rutPuntos;
  //             }
  //             j++;
  //         }
  //         let dv = actualLimpio.substring(actualLimpio.length - 1);
  //         rutPuntos = rutPuntos + '-' + dv;
  //     }
  //     return rutPuntos;
  // }
}
/*
boolean validacion = false;
try {
rut =  rut.toUpperCase();
rut = rut.replace(".", "");
rut = rut.replace("-", "");
int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

char dv = rut.charAt(rut.length() - 1);

int m = 0, s = 1;
for (; rutAux != 0; rutAux /= 10) {
s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
}
if (dv == (char) (s != 0 ? s + 47 : 75)) {
validacion = true;
}

} catch (java.lang.NumberFormatException e) {
} catch (Exception e) {
}
return validacion;
*/
