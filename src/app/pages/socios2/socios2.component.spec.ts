import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Socios2Component } from './socios2.component';

describe('Socios2Component', () => {
  let component: Socios2Component;
  let fixture: ComponentFixture<Socios2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Socios2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Socios2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
