import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Filtros } from 'src/app/model/filtros';
import { FormGroup, FormControl } from '@angular/forms';
import * as xlsx from 'xlsx';
import { ConciliacionService } from 'src/app/services/conciliacion.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { ContaPagoService } from 'src/app/services/conta-pago.service';
import { ISpinnerConfig } from '@hardpool/ngx-spinner';

@Component({
  selector: 'app-conta-pago',
  templateUrl: './conta-pago.component.html',
  styleUrls: ['./conta-pago.component.scss']
})
export class ContaPagoComponent implements OnInit {

  @ViewChild('concitable', { static: false }) concitable: ElementRef;
  
  spinner: boolean;
  spinnerConfig: ISpinnerConfig;
  
  conciliaciones: any[];
  voucherConciliacion: any[];
  
  public busquedaForm: FormGroup;
  searchCompany: string;
  searchSocio: string;
  searchConcepto: string;

  conciliacionDetalle: any;
  conciliacionPrima: any[];
  conciliacionComision: any[];
  conciliacionDevolucion: any[];
  conciliacionRecupero: any[];
  conciliacionGastos: any[];
  conciliacionValidaciones: any[];
  conciliacionSunsystem: any[];
  conciliacionNetsuite: any[];
  filas = 5;
  total = 0;
  p = 1;

  constructor(
    private conciliaService: ConciliacionService,
    private contaPagoService: ContaPagoService,
    private alerta: AlertasService
  ) { }

  ngOnInit(): void {
    
    this.busquedaForm = new FormGroup({
      searchCompany: new FormControl(null),
      searchSocio: new FormControl(null),
      searchConcepto: new FormControl(null)
    });

  }

  public listarConciliacion(f: Filtros) {
    console.log('FORMULARIO:' + JSON.stringify(f));
    this.conciliaService.getAll(f).subscribe(
        (data: any) => {
          console.log('CONCILICIONES:' + JSON.stringify(data.Body));
          
          console.log('data.Body.Groupings.lenght ' , data.Body.length);
          if(data.Body.length > 0){
            this.conciliaciones = data.Body;
          }else{
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      );
  }

  public listarContaPago(f: Filtros) {
    console.log('FORMULARIO:' + JSON.stringify(f));
    this.contaPagoService.getContaPago(f).subscribe(
        (data: any) => {
          console.log('CONCILICIONES:' + JSON.stringify(data.Body));
         
          console.log("data.Body.Groupings.lenght " , data.Body.length);
          if(data.Body.length > 0){
            this.conciliaciones = data.Body;
          }else{
            this.spinner = false;
            this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
          }
        },
        (error) => {
          console.log('ERROR: ', error);
        }
      );
  }

  public detalleConciliacion(id: string) {
    console.log('ID:' + id);
    this.conciliaService.getDetalles(id).subscribe(
      (data) => {
        console.log('DetalleConciliacion: ' + JSON.stringify(data));
        this.conciliacionDetalle = data.Body;
        this.conciliacionPrima = data.Body[0].Conciliacion.Prima;
        this.conciliacionComision = data.Body[0].Conciliacion.Comision;
        this.conciliacionDevolucion = data.Body[0].Conciliacion.Devolucion;
        this.conciliacionRecupero = data.Body[0].Conciliacion.Recupero;
        this.conciliacionGastos = data.Body[0].Conciliacion.Gastos;
        this.conciliacionValidaciones = data.Body[0].Conciliacion.Validaciones;
        this.conciliacionSunsystem = data.Body[0].Conciliacion.Sunsystems;
        this.conciliacionNetsuite = data.Body[0].Conciliacion.Netsuite;

        console.log('SUSNSYSTEMS:' + this.conciliacionSunsystem);
        console.log('NETSUITE:' + JSON.stringify(this.conciliacionNetsuite));

      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public detalleVouchers(vouchers: any[]){
    this.voucherConciliacion = vouchers;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.concitable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'conciliacion.xlsx');
   }

   selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

}
