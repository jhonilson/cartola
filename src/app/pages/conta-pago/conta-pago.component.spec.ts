import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaPagoComponent } from './conta-pago.component';

describe('ContaPagoComponent', () => {
  let component: ContaPagoComponent;
  let fixture: ComponentFixture<ContaPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
