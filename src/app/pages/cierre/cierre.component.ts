import { Component, OnInit } from '@angular/core';
import { PeriodosCierreService } from 'src/app/services/periodos-cierre.service';
import { AlertasService } from 'src/app/services/alertas.service';
import { SPINNER_ANIMATIONS, SPINNER_PLACEMENT, ISpinnerConfig } from '@hardpool/ngx-spinner';
import { PeriodosCierre } from 'src/app/model/periodos-cierre';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-cierre',
  templateUrl: './cierre.component.html',
  styleUrls: ['./cierre.component.scss']
})
export class CierreComponent implements OnInit {

  spinner: boolean;
  spinnerConfig: ISpinnerConfig;
  periodosCierre: any[] = [];
  periodoVigente: number;
  periodoAnterior: number;

  constructor(
    private periodosCierreService: PeriodosCierreService,
    private alertaService: AlertasService,
  ) { }

  ngOnInit(): void {

    this.spinnerConfig = {
      placement: SPINNER_PLACEMENT.block_window,
      animation: SPINNER_ANIMATIONS.spin_3,
      size: '6rem',
      color: '#1dab5a',
      bgColor: 'rgba(255,255,255,0.8)'
    };

    this.getPeriodosCierre();

  }

  public getPeriodosCierre(){
    this.periodosCierre = [];
    this.periodosCierreService.getPeriodosCierre().subscribe((data: any) => {
      console.log('PERIODOS CIERRE ' , data);
      if (data.status[0].code == '200'){
        this.periodosCierre = data.body;
        this.periodoVigente = data.body[0].value;
        console.log('this.periodosCierre ', this.periodosCierre);
        console.log('this.periodoVigente ', this.periodoVigente);
      } else {
        this.alertaService.alertBasicIcon('Error', data.status[0].error, 'error');
      }
    }, error => {
      this.alertaService.alertBasicIcon('Error', 'Error al consultar Periodos', 'error');
    });
  }

  public cerrarPeriodo(idPeriodo: string, periodo: string){
    console.log('idPeriodo ', idPeriodo);
    Swal.fire({
      title: 'Usted se encuentra cerrando el periodo ' + periodo,
      text: '¿Está seguro de cerrar?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner = true;
        this.periodosCierreService.getCierre(idPeriodo).subscribe((data: any) => {
          console.log('cierre respuesta ' , data);
          if (data.Status[0].code == '200'){
            this.alertaService.alertBasicIcon('Periodo Cerrado', data.Body.message, 'info');
            this.getPeriodosCierre();
            this.spinner = false;
          }else{
            this.spinner = false;
            this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
          }
        }, error => {
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', 'Error al Cerrar Periodo', 'error');
        });
      }
    });
  }

  public abrirPeriodo(idPeriodo: string, periodo: string){
    console.log('idPeriodo ', idPeriodo);
    Swal.fire({
      title: 'Usted se encuentra abriendo el periodo ' + periodo,
      text: '¿Está seguro de reabrir el periodo?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner = true;
        this.periodosCierreService.getAbrirPeriodo(idPeriodo).subscribe((data: any) => {
          console.log('abrir periodo respuesta ' , data);
          if (data.Status[0].code == '200'){
            this.alertaService.alertBasicIcon('Periodo Abierto', data.Body.message, 'info');
            this.getPeriodosCierre();
            this.spinner = false;
          }else{
            this.spinner = false;
            this.alertaService.alertBasicIcon('Error', data.Status[0].error, 'error');
          }
        }, error => {
          this.spinner = false;
          this.alertaService.alertBasicIcon('Error', 'Error al Abrir Periodo', 'error');
        });
      }
    });
    
  }

}
