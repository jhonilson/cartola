import { Component, OnInit } from '@angular/core';
import { Entidad } from 'src/app/model/entidad';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EntidadService } from 'src/app/services/entidad.service';
import { Router } from '@angular/router';
import { SociosService } from 'src/app/services/socios.service';
import { Socio } from 'src/app/model/socio';
import { AlertasService } from 'src/app/services/alertas.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-entidades',
  templateUrl: './entidades.component.html',
  styleUrls: ['./entidades.component.scss'],
})
export class EntidadesComponent implements OnInit {
  public socio: Socio = new Socio();
  public entidad: Entidad = new Entidad();
  public entidades: Entidad[];
  public socios: Socio[];
  public entidadForm: FormGroup;

  constructor(
    private entidadService: EntidadService,
    private socioService: SociosService,
    private alerta: AlertasService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.entidadForm = new FormGroup({
      identidad: new FormControl(''),
      razonsocial: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
      ]),
      rut: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15),
        // Validators.pattern('\b(\d{1,3}(?:(\.?)\d{3}){2}(-?)[\dkK])\b')
      ]),
      cuentacontable: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        //  Validators.pattern('\w')
      ]),
      descripcion: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(100),
      ]),
      socio: new FormGroup({
        idsocio: new FormControl('', Validators.required),
      }),
    });

    this.listarEntidades();
    this.listarSocios();
  }

  public listarSocios() {
    this.socioService.getAll().subscribe(
      (data: any) => {
        console.log('socios: ' + JSON.stringify(data));
        this.socios = data;
      },
      (error) => {
        console.log('ERROR: ', error);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  public listarEntidades() {
    this.entidadService.getAll().subscribe(
      (data: any) => {
        console.log(data);
        this.entidades = data;
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  public add() {
    console.log(this.entidadForm.value);
    if (this.entidadForm.valid) {
      this.entidadService.save(this.entidadForm.value).subscribe(
        (data) => {
          console.log('DATA:' + JSON.stringify(data));
          // alert('agregado con exito!');
          this.alerta.alertBasic2('agregado con exito!', 'success');
          this.onResetForm();
          this.listarEntidades();
        },
        (error) => {
          console.log('ERROR: ', error);
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );
    } else {
      this.alerta.alertBasicIcon(
        'Error',
        'Debe llenar todos los campos de formulario!',
        'error'
      );
    }
  }

  public delete(s: Entidad) {
    console.log('ID:' + s.identidad);

    Swal.fire({
      title: '¿Esta seguro que desea ejecutar esta operacion?',
      text: 'Esta no se podra revertir!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Ejecutar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.value) {
        this.entidadService.delete(s.identidad).subscribe(
          (data) => {
            this.listarEntidades();
            // alert('eliminado con exito!');
            this.alerta.alertBasic2('eliminado con exito!', 'success');
          },
          (error) => {
            console.log('ERROR: ', error);
            this.alerta.alertBasicIcon('Error', error.message, 'error');
          }
        );
      }
    });
  }

  editar(s: Entidad): void {
    console.log('ID:' + s.identidad);
    this.entidadService.findById(+s.identidad).subscribe(
      (data) => {
        console.log('DATA:' + JSON.stringify(data));
        this.entidad = data;
        this.loadData();
      },
      (error) => {
        console.log('ERROR: ', error.message);
        this.alerta.alertBasicIcon('Error', error.message, 'error');
      }
    );
  }

  loadData() {
    this.entidadForm.setValue({
      identidad: this.entidad.identidad,
      razonsocial: this.entidad.razonsocial,
      rut: this.entidad.rut,
      cuentacontable: this.entidad.cuentacontable,
      descripcion: this.entidad.descripcion,
      socio: {
        idsocio: this.entidad.socio.idsocio,
      },
    });
  }

  update() {
    // if (this.rolForm.valid) {

    console.log('entidadupdate: ' + JSON.stringify(this.entidadForm.value));
    if (this.entidadForm.valid) {
      this.entidadService.update(this.entidadForm.value).subscribe(
        (data) => {
          this.onResetForm();
          this.listarEntidades();
          alert('Se modifico con Exito!');
          this.alerta.alertBasic2('Se modifico con exito!', 'success');
        },
        (error) => {
          console.log('ERROR: ', error);
          // alert('Error' +  error.message);
          this.alerta.alertBasicIcon('Error', error.message, 'error');
        }
      );
    } else {
      this.alerta.alertBasicIcon(
        'Error',
        'Debe llenar todos los campos de formulario!',
        'error'
      );
    }
  }

  onResetForm() {
    this.entidadForm.reset();
    this.entidad.identidad = null;
  }
}
