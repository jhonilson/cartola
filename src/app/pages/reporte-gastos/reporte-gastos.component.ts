import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as xlsx from 'xlsx';
import { FormGroup, FormControl } from '@angular/forms';
import { GastosService } from 'src/app/services/gastos.service';
import { GastosLista } from 'src/app/model/gastos-lista';
import { Fechas } from 'src/app/model/fechas';
import { GlobalConstants } from 'src/app/model/global-constants';
import { AlertasService } from 'src/app/services/alertas.service';


@Component({
  selector: 'app-reporte-gastos',
  templateUrl: './reporte-gastos.component.html',
  styleUrls: ['./reporte-gastos.component.scss']
})
export class ReporteGastosComponent implements OnInit {

  @ViewChild('gastotable', { static: false }) gastotable: ElementRef;

  public allGastos: GastosLista[];
  p = 1;
  concepto: string;
  subconcepto: string;
  mescobertura: string;
  compania: string;
  socio: string;
  filas = GlobalConstants.filas;
  total = 0;
  listarEstados = '';

  public busquedaForm: FormGroup;

  constructor(
    private service: GastosService,
    private alerta: AlertasService
    ) {}

  ngOnInit(): void {

    this.busquedaForm = new FormGroup({
      concepto: new FormControl(''),
      subconcepto: new FormControl(''),
      mescobertura: new FormControl(''),
      compania: new FormControl(''),
      socio: new FormControl('')
    });

   // this.listarGastos();
  }

  // public listarGastos(f: Fechas) {
  //   console.log('FECHAS: ' + JSON.stringify(f));
    
  //   this.service.getGastosRango(f).subscribe(
  //     (data: any) => {
  //       console.log('Gastos:', data.Body);
  //       this.allGastos = data.Body;
  //     },
  //     (error) => {
  //       console.log('ERROR: ', error);
  //     }
  //   );
  // }

  public listarGastos(f: Fechas) {
    console.log('FECHAS: ' + JSON.stringify(f));
    
    this.service.getAll().subscribe(
      (data: any) => {
        console.log('Gastos:', data.Body);
        this.allGastos = data.Body;
        if(data.Body.length > 0 ){
          this.allGastos = data.Body;
        }else{
          this.alerta.alertBasicIcon('Error', 'No existen registros para los criterios de búsqueda.', 'error');
        }
      },
      (error) => {
        console.log('ERROR: ', error);
      }
    );
  }

  selectChangeHandler(event: any) {
    this.filas = event.target.value;
    this.p = 1;
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
   // xlsx.utils.table_to_sheet(this.gastotable.nativeElement);
    xlsx.utils.json_to_sheet(this.allGastos);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'gastos.xlsx');
   }


}
