export class GastosLista {
  Body: Body[] = [];
  Status: Status;
}

class Body {
  idGastos: any;
  subconcepto: any;
  operatoria: any;
  compania: any;
  socio: any;
  policy: any;
  concepto: any;
  idProducto: any;
  mesCobertura: any;
  primaAsociada: any;
  montoFinal: any;
  porcentaje: any;

  cantidadRegistros: number;
  nombreArchivo: string;
  usuario: string;
  fechaCargArch: string;
  iva: string;
  codigo: any;
  codigoDesc: any;
  ivaAfecto: any;
  ivaExento: any;
  isSelected?:boolean;
}

class Status {
  code: string;
  status: string;
  error: string;
}
