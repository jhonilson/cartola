export class ResponseFiltros {
  Body: Body[] = [];
  Status: Status;
}

class Body {
  name: string;
  value: string;
}

class Status {
  code: string;
  status: string;
  error: string;
}


