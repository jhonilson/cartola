export class Gastos {
    public subconcepto: any;
    public operatoria: any;
    public compania: any;
    public socio: any;
    public policy: any;
    public concepto: any;
    public idProducto: any;
    public mesCobertura: any;
    public primaAsociada: any;
    public montoFinal: any;
    public porcentaje: any;
    public idPrdt: any;

    public cantidadRegistros: number;
    public nombreArchivo: string;
    public usuario: string;
    public fechaCargArch: string;

    public codigo: any;
    public codigoDesc: any;
    public ivaAfecto: any;
    public ivaExento: any;
}