export class Filtros {
  public compania: string;
  public mesCobertura: string;
  public socio: string;
  public poliza: string;
  public periodoContable: string;
  public estado: string;
  public concepto: string;
  public subConcepto: string;

  public anoContable: string;
  public aprobacion: string;

  public portalBancario?: string;
  public medioPago?: string;
  public pagara?: string;
}

