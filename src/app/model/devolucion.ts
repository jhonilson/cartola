export class Devolucion {
  public panId: string;
  public panRutempresa: string;
  public panTiposervicio: string;
  public panTipocuentacargo: string;
  public panNumcuentacargo: string;
  public panDescripcionpago: string;
  public panRutbeneficiario: string;
  public panNombrebeneficiario: string;
  public panEmailbeneficiario: string;
  public panMetodopago: string;
  public panCodigobancoabono: string;
  public panTipocuentaabono: string;
  public panNumcuentaabono: string;
  public panFechapago: string;
  public panCampobanco1: string;
  public panCampobanco2: string;
  public panMontopago: string;
  public panTipocuentacargodet: string;
  public panNumcuentacargodet: string;
  public panCodigosucursal: string;
  public panReferenciacliente: string;
  public panRetencionfactura: string;
  public panAbonovalesvistalinea: string;
  public panCantidaddocumentos: string;

  public panTiporendicion: string;
  public panTipopago: string;
  public panNumerodocumento: string;
  public panMontodocumento: string;
  public panNumerooperacion: string;
  public panFechapreprenun: string;
  public panTipooperacion: string;
  public panComuna: string;
  public panCiudad: string;
  public panpoliza: string;
  public panFormapago: string;
  public panEstado: string;
  public panIdagrupacion: string;
  public panIdcontabilizacion: string;
  public panFechaproceso: string;

  public panSocio: string;
  public panCompania: string;
  public panNombrepoliza: string;

  public panPagara: string;
  public panIdsolicitudegreso: string;

  public  panProducto: string;
  public  panNorequerimiento: string;
  public  panFecrequer: string;

  public isSelected?: boolean;
}
