export class CerrarPeriodo {
    Body: Body;
    Status: Status[] = [];
}

class Body {
    message: string;
}

class Status {
    code: string;
    status: string;
    error: string;
}