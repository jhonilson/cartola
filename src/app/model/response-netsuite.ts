export class ResponseNetsuite {
    Body: Body[] = [];
    Status: Status;
  }
  
  class Body {
    idnetsuite: string;
    folio: string;
    bruto: string;
    razonsocial: string;
    estado: string;
    iva: string;
    neto: string;
    total: string;
    compania: string;
    documento: string;
    factura: string;
    tipodocumento: string;
    tipofactura: string;
    rutsocio: string;
    fechaenvio: string;
    fecharecepcion: string;
    fechaproceso: string;
    fechaemisiondoc: string;
    ivaexento: string;
    ivarecuperable: string;
    mtorecuperable: string;
    montoexento: string;
    uso: string;
    isSelected?:boolean;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }