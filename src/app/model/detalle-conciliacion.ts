export class DetalleConciliacion {
  Body: Body[] = [];
  Status: Status;
}

class Body {
  Conciliacion: Conciliacion;
}

class Conciliacion {
  idConciliacion: string;
  justiToleranciaPims: string;
  fechaConversion: string;
  salidaDinero: string;
  periodoConci: string;
  idEstadoConci: string;
  idError: string;
  aprobacion: string;
  fechaCreacion: string;
  usuario: string;
  Prima: Agrupacion[] = [];
  Comision: Agrupacion[] = [];
  Devolucion: Agrupacion[] = [];
  Recupero: Agrupacion[] = [];
  Gastos: Gastos[] = [];
  Validaciones: Validaciones[] = [];
  Sunsystems: Sunsystems[] = [];
  Netsuite: Netsuite[] = [];
}

class Agrupacion {
  idAgrupacion: string;
  idEstadiAgrup: string;
  idPeriodoConcar: string;
  socioPims: string;
  companiaPims: string;
  polizaPims: string;
  conceptoPims: string;
  subconceptoPims: string;
  productoPims: string;
  ramoPims: string;
  canalPims: string;
  facturablePims: string;
  tipoFacturacionPims: string;
  fechaUFPims: string;
  mesCoberturaPims: string;
  brutoPesosPims: string;
  netoPesosPims: string;
  excetoPesosPims: string;
  afectoPesosPims: string;
  ivaPesosPims: string;
  brutoUFPims: string;
  netoUFPims: string;
  exentoUFOims: string;
  afectoUFPims: string;
  ivaUFPims: string;
  idError: string;
  fecha: string;
}

class Gastos {
  idGastos: string;
  subconcepto: string;
  operatoria: string;
  compania: string;
  socio: string;
  policy: string;
  concepto: string;
  idProducto: string;
  mesCobertura: string;
  primaAsociada: string;
  montoFinal: string;
  porcentaje: string;
  cantidadRegistros: string;
  nombreArchivo: string;
  usuario: string;
  fechaCargArch: string;
  usado: string;
}
class Validaciones {
  idValidacion: string;
  nombre: string;
  descripcion: string;
  valor: string;
  tipo: string;
}

class Sunsystems {
  idSunsystem: string;
  codCuenta: string;
  refTransaccion: string;
  montoBase: string;
  descripcion: string;
  numeroLibro: string;
  fechaTransaccion: string;
  codAnalitico: string;
  asignado: string;
}

class Netsuite {
  idNetsuite: string;
  idFacturaNet: string;
  montoFacturaNet: string;
  desNet: string;
  estadoFacturaNet: string;
}

class Status {
  code: string;
  status: string;
  error: string;
}
