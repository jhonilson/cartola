export class Conciliacion {
  Body: Body;
  Status: Status;
}

class Body {
  mensaje: string;
}

class Status {
  code: string;
  status: string;
  error: string;
}
