export class ResponseReversa {
    Body: Body;
    Status: Status;
  }
  
  class Body {
    idContabilizacion: string;
    detalle: string;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }