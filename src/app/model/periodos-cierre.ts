export class PeriodosCierre {
    body: Body[] = [];
    status: Status;
  }
  
  class Body {
    name: string;
    value: string;
    status: string;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }