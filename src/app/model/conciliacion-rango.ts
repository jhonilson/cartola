export class ConciliacionRango {
  public cncIdconciliacion: string;
  public cncJustitoleranciapims: string;
  public cncFechaconversion: string;
  public cncSalidadinero: string;
  public cncPeriodoconci: string;
  public cncIdestadoconci: string;
  public cncIderror: string;
  public cncAprobacion: string;
  public cncFechacreacion: string;
  public cncUsuario: string;
}
