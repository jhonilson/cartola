export class VoucherReintento {
    Body: Body;
    Status: Status[] = [];
  }
  
  class Body {
    idContablizacion: any;
    detalle: string;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }
  