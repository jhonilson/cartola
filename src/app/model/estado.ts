export class Estado {
  public estIdestado: string;
  public estNombreestado: string;
  public estProceso: string;
  public estTipo: string;
  public estDescripcion: string;
}
