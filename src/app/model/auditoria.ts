export class Auditoria {
    public audIdauditoria: number;
    public cloIdLote: number;
    public audFechaIni: Date;
    public audFlujo: string;
    public audNombreSeq: string;
    public audNombreJob: string;
    public audFechaFin: Date;
    public audEstatus: string;
    public audObservacion: string;
}
