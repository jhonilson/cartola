export class ConciliacionReporte {
  Body: Body[] = [];
  Status: Status;
}

class Body {
    estado: any;
    idConciliacion: any;
    policy: any;
    nEgreso: any;
    Estado: any;
    medioPago: any;
    anoContable: any;
    mesContable: any;
    fechaProceso: any;
    codUsuario: any;
    concepto: any;
    idProducto: any;
    compania: any;
    socio: any;
    mesCobertura: any;
    primaAsociada: any;
    montoFinal: any;
    porcentaje: any;
    montoFinalUF: any;
    estadoNombre: any;
}

class Status {
  code: string;
  status: string;
  error: string;
}
