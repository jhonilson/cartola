export class RecuperosLista {
    Body: Body;
    Status: Status;
  }
  
  class Body {
    Groupings: Groupings[] = [];
  }

  class Groupings {
    idAgrupacion: any;
    idEstadiAgrup: any;
    idNominaPims: any;
    idPeriodoConcar: any;
    socioPims: any;
    companiaPims: any;
    polizaPims: any;
    conceptoPims: any;
    subconceptoPims: any;
    productoPims: any;
    ramoPims: any;
    canalPims: any;
    facturablePims: any;
    tipoFacturacionPims: any;
    fechaUFPims: any;
    mesCoberturaPims: any;
    brutoPesosPims: any;
    netoPesosPims: any;
    excetoPesosPims: any;
    afectoPesosPims: any;
    ivaPesosPims: any;
    brutoUFPims: any;
    netoUFPims: any;
    exentoUFOims: any;
    afectoUFPims: any;
    ivaUFPims: any;
    idError: any;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }
  