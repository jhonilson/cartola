export class ResponseCuentas {
        Body: Body;
        Status: Status;
    }
  
    class Body {
        idSocio: string;
        cuentas: Cuentas[] = [];
    }

    class Cuentas {
        cuenta: string;
        entidad: string;
    }
    
    class Status {
        code: string;
        status: string;
        error: string;
    }

