export class ResponseSunsystem {
    Body: Body[] = [];
    Status: Status;
  }
  
  class Body {
    // idSystem: string;
    // codCuenta: string;
    // refTransaccion: string;
    // montoBase: string;
    // descripcion: string;
    // numeroLibro: string;
    // fechaTransaccion: string;
    // codAnalitico: string;
    // asignado: string;
    idSystem: string;
    accountCode: string;
    allocationMarker: string;
    analysisCode1: string;
    analysisCode2: string;
    analysisCode3: string;
    analysisCode4: string;
    analysisCode5: string;
    analysisCode6: string;
    analysisCode7: string;
    analysisCode8: string;
    analysisCode9: string;
    analysisCode10: string;
    baseAmount: string;
    debitCredit: string;
    description: string;
    journalNumber: string
    transactionDate: string;
    transactionReference: string;
    compania: string;
    isSelected?:boolean;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }