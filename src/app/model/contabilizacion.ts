import { ContabilizacionDetalles } from './contabilizacion-detalles';

export class Contabilizacion {
  public conIdcontabilizacion: string;
  public ctiIdcontatipo: string;
  public conIdvouchersun: string;
  public conFechatransac: string;
  public conReftransac: string;
  public conIdestadoconta: string;
  public conIderror: string;
  public conTipocontab: string;

  public contaDetalle: ContabilizacionDetalles;
}
