export class ResponseValidaciones {
    Body: Body[] = [];
    Status: Status;
  }
  
  class Body {
    id: string;
    nombre: string;
    descripcion: string;
    valor: string;
    tipo: string;
  }
  
  class Status {
    code: string;
    status: string;
    error: string;
  }