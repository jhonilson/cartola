import { Socio } from './socio';

export class Entidad {
  public identidad: number;
  public razonsocial: string;
  public rut: string;
  public cuentacontable: string;
  public descripcion: string;
  public socio: Socio;
}
