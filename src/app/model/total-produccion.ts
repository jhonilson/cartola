export class TotalProduccion{
    concepto: string;
    brutoPesosPims: number;
    netoPesosPims: number;
    excetoPesosPims: number;
    afectoPesosPims: number;
    ivaPesosPims: number;
}