export class ResponseAgrupaciones {
  Body: Body;
  Status: Status;
}


class Body {
  Groupings: Groupings[] = [];
}


class Groupings {
  idAgrupacion: string;
  estado: string;
  idEstadiAgrup: string;
  idNominaPims: string;
  idPeriodoConcar: string;
  socioPims: string;
  companiaPims: string;
  polizaPims: string;
  conceptoPims: string;
  subconceptoPims: string;
  productoPims: string;
  ramoPims: string;
  canalPims: string;
  facturablePims: string;
  tipoFacturacionPims: string;
  fechaUFPims: string;
  mesCoberturaPims: string;
  brutoPesosPims: string;
  netoPesosPims: string;
  excetoPesosPims: string;
  afectoPesosPims: string;
  ivaPesosPims: string;
  brutoUFPims: string;
  netoUFPims: string;
  exentoUFOims: string;
  afectoUFPims: string;
  ivaUFPims: string;
  idError: string;
  isSelected?:boolean;
}


class Status {
  code: string;
  status: string;
  error: string;
}
