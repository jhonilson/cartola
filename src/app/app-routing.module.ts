import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { BlankComponent } from './pages/blank/blank.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './utils/guards/auth.guard';
import { NonAuthGuard } from './utils/guards/non-auth.guard';
import { UploadComponent } from './pages/upload/upload.component';
import { PdfExportComponent } from './pages/pdf-export/pdf-export.component';
import { EntidadesComponent } from './pages/entidades/entidades.component';
import { Socios2Component } from './pages/socios2/socios2.component';

import { ReporteAgrupacionComponent } from './pages/reporte-agrupacion/reporte-agrupacion.component';
import { PlantillaComponent } from './pages/plantilla/plantilla.component';
import { ContaManualComponent } from './pages/conta-manual/conta-manual.component';
import { ContabilizacionComponent } from './pages/contabilizacion/contabilizacion.component';
import { ReporteFacturacionComponent } from './pages/reporte-facturacion/reporte-facturacion.component';
import { ReporteAuditoriaComponent } from './pages/reporte-auditoria/reporte-auditoria.component';
import { ReporteTransaccionComponent } from './pages/reporte-transaccion/reporte-transaccion.component';
import { GastosComponent } from './pages/gastos/gastos.component';
import { ConciliacionComponent } from './pages/conciliacion/conciliacion.component';
import { ReporteGastosComponent } from './pages/reporte-gastos/reporte-gastos.component';
import { ReporteConciliacionComponent } from './pages/reporte-conciliacion/reporte-conciliacion.component';
import { ModalconciliacionComponent } from './pages/modalconciliacion/modalconciliacion.component';
import { FechasComponent } from './components/fechas/fechas.component';
import { PrevisualizacionmodalComponent } from './components/previsualizacionmodal/previsualizacionmodal.component';
import { FiltrosComponent } from './components/filtros/filtros.component';
import { ReporteExtraccionComponent } from './pages/reporte-extraccion/reporte-extraccion.component';
import { ReporteContabilizacionComponent } from './pages/reporte-contabilizacion/reporte-contabilizacion.component';
import { ReporteAgrupacion2Component } from './pages/reporte-agrupacion2/reporte-agrupacion2.component';
import { ReporteConciliacion2Component } from './pages/reporte-conciliacion2/reporte-conciliacion2.component';
import { FlujoReversaComponent } from './pages/flujo-reversa/flujo-reversa.component';
import { CierreComponent } from './pages/cierre/cierre.component';
import { ContaPagoComponent } from './pages/conta-pago/conta-pago.component';
import { DevolucionPagoComponent } from './pages/devolucion-pago/devolucion-pago.component';

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'blank',
        component: BlankComponent,
      },
      {
        path: 'upload',
        component: UploadComponent,
      },
      {
        path: 'pdfExport',
        component: PdfExportComponent,
      },
      {
        path: 'entidad',
        component: EntidadesComponent,
      },
      {
        path: 'socio',
        component: Socios2Component,
      },
      {
        path: 'contaManual',
        component: ContaManualComponent,
      },
      {
        path: 'contabilizacion',
        component: ContabilizacionComponent,
      },
      {
        path: 'conciliacion',
        component: ConciliacionComponent,
      },
      {
        path: 'contaPago',
        component: ContaPagoComponent,
      },
      {
        path: 'devolucion',
        component: DevolucionPagoComponent,
      },
      {
        path: 'reportAgrupaciones2',
        component: ReporteAgrupacion2Component,
      },
      {
        path: 'reportAgrupaciones',
        component: ReporteAgrupacionComponent,
      },
      {
        path: 'reportConciliacion2',
        component: ReporteConciliacion2Component,
      },
      {
        path: 'reportFacturacion',
        component: ReporteFacturacionComponent,
      },
      {
        path: 'reportAuditoria',
        component: ReporteAuditoriaComponent,
      },
      {
        path: 'reportTransaccion',
        component: ReporteTransaccionComponent,
      },
      {
        path: 'reportGastos',
        component: ReporteGastosComponent,
      },
      {
        path: 'reportExtraccion',
        component: ReporteExtraccionComponent,
      },
      {
        path: 'reportContabilizacion',
        component: ReporteContabilizacionComponent,
      },
      {
        path: 'flujoAprobacion',
        component: ReporteConciliacionComponent,
      },
      {
        path: 'flujoReversa',
        component: FlujoReversaComponent,
      },
      {
        path: 'cierre',
        component: CierreComponent,
      },
      {
        path: 'plantilla',
        component: PlantillaComponent,
      },
      {
        path: 'gastos',
        component: GastosComponent,
      },
      {
        path: 'modal',
        component: PrevisualizacionmodalComponent,
      },
      {
        path: 'fecha',
        component: FechasComponent,
      },
      {
        path: 'filtro',
        component: FiltrosComponent,
      },
      {
        path: '',
        component: DashboardComponent,
      },
    ],
  },
  {
    path: '',
    component: LoginComponent,
    canActivate: [NonAuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NonAuthGuard],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
