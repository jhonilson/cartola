import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './main/header/header.component';
import { FooterComponent } from './main/footer/footer.component';
import { MenuSidebarComponent } from './main/menu-sidebar/menu-sidebar.component';
import { BlankComponent } from './pages/blank/blank.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProfileComponent } from './pages/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { MessagesDropdownMenuComponent } from './main/header/messages-dropdown-menu/messages-dropdown-menu.component';
import { NotificationsDropdownMenuComponent } from './main/header/notifications-dropdown-menu/notifications-dropdown-menu.component';

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { UploadComponent } from './pages/upload/upload.component';
import { PdfExportComponent } from './pages/pdf-export/pdf-export.component';


import * as $ from 'jquery';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe } from './model/filter-pipe';
import { CommonModule, registerLocaleData } from '@angular/common';

import {NgxPaginationModule} from 'ngx-pagination';
import { PlantillaComponent } from './pages/plantilla/plantilla.component'; // <-- import the module

import { Socios2Component } from './pages/socios2/socios2.component';
import { EntidadesComponent } from './pages/entidades/entidades.component';

import { ContaManualComponent } from './pages/conta-manual/conta-manual.component';
import { ReporteAgrupacionComponent } from './pages/reporte-agrupacion/reporte-agrupacion.component';
import { ContabilizacionComponent } from './pages/contabilizacion/contabilizacion.component';
import { ReporteFacturacionComponent } from './pages/reporte-facturacion/reporte-facturacion.component';
import { FiltrosComponent } from './components/filtros/filtros.component';
import { ReporteAuditoriaComponent } from './pages/reporte-auditoria/reporte-auditoria.component';
import { ReporteTransaccionComponent } from './pages/reporte-transaccion/reporte-transaccion.component';
import { GastosComponent } from './pages/gastos/gastos.component';
import { ConciliacionComponent } from './pages/conciliacion/conciliacion.component';
import { ReporteGastosComponent } from './pages/reporte-gastos/reporte-gastos.component';
import { ReporteConciliacionComponent } from './pages/reporte-conciliacion/reporte-conciliacion.component';
import { ModalconciliacionComponent } from './pages/modalconciliacion/modalconciliacion.component';
import { ConciliacionmodalComponent } from './components/conciliacionmodal/conciliacionmodal.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FechasComponent } from './components/fechas/fechas.component';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { PrevisualizacionmodalComponent } from './components/previsualizacionmodal/previsualizacionmodal.component';
import { RemoveCommaPipe } from './pipe/removeCommaPipe';
import { NgSelectModule } from '@ng-select/ng-select';

    // importar locales
import localePy from '@angular/common/locales/es-PY';
import { FiltrosConciliacionComponent } from './components/filtros-conciliacion/filtros-conciliacion.component';
import { ReporteExtraccionComponent } from './pages/reporte-extraccion/reporte-extraccion.component';
import { ReporteContabilizacionComponent } from './pages/reporte-contabilizacion/reporte-contabilizacion.component';
import { ReporteAgrupacion2Component } from './pages/reporte-agrupacion2/reporte-agrupacion2.component';
import { ReporteConciliacion2Component } from './pages/reporte-conciliacion2/reporte-conciliacion2.component';

import { NgxSpinnerModule } from '@hardpool/ngx-spinner';
import { DevolucionPagoComponent } from './pages/devolucion-pago/devolucion-pago.component';
import { FlujoReversaComponent } from './pages/flujo-reversa/flujo-reversa.component';
import { CierreComponent } from './pages/cierre/cierre.component';
import { ContaPagoComponent } from './pages/conta-pago/conta-pago.component';
import { VouchersmodalComponent } from './pages/vouchersmodal/vouchersmodal.component';

    // registrar los locales con el nombre que quieras utilizar a la hora de proveer
registerLocaleData(localePy, 'es');

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    MenuSidebarComponent,
    BlankComponent,
    ProfileComponent,
    RegisterComponent,
    DashboardComponent,
    MessagesDropdownMenuComponent,
    NotificationsDropdownMenuComponent,
    UploadComponent,
    PdfExportComponent,
    Socios2Component,
    EntidadesComponent,
    FilterPipe,
    ReporteAgrupacionComponent,
    PlantillaComponent,
    ContaManualComponent,
    ContabilizacionComponent,
    FiltrosComponent,
    ReporteFacturacionComponent,
    ReporteAuditoriaComponent,
    ReporteTransaccionComponent,
    GastosComponent,
    ConciliacionComponent,
    ReporteGastosComponent,
    ReporteConciliacionComponent,
    ModalconciliacionComponent,
    ConciliacionmodalComponent,
    FechasComponent,
    PrevisualizacionmodalComponent,
    RemoveCommaPipe,
    FiltrosConciliacionComponent,
    ReporteExtraccionComponent,
    ReporteContabilizacionComponent,
    ReporteAgrupacion2Component,
    ReporteConciliacion2Component,
    DevolucionPagoComponent,
    FlujoReversaComponent,
    CierreComponent,
    ContaPagoComponent,
    VouchersmodalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    DateTimePickerModule,
    DatePickerModule,
    NgSelectModule,
    NgxSpinnerModule,
    SweetAlert2Module.forRoot(),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  exports: [
    FilterPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
