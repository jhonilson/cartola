import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeComma'
})
export class RemoveCommaPipe implements PipeTransform {

  transform(value: string): string {
    if (value !== undefined && value !== null) {
      // let resultado: string;
      // resultado = value;
      // resultado = resultado.replace(/,/g, '.');
      // resultado = resultado.replace('.', ',');
      // return resultado;
     return value.replace(/,/g, '.');
    } else {
      return '';
    }
  }
}
