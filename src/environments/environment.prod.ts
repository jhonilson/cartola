export const environment = {
  production: true,

  urlConciliacionReporte: 'http://logicad.cardif.cl/CartolaBack/conciliacion/',
  urlEntidades: 'http://logicad.cardif.cl/CartolaBack/entidades/',
  urlGastos: 'http://logicad.cardif.cl/CartolaBack/gastos',
  urlSocios: 'http://logicad.cardif.cl/CartolaBack/socios',
  urlSocios2: 'http://localhost:7000/socios',

  urlAuditoria: 'http://logicad.cardif.cl/CartolaBack/auditoria',
  urlTransacciones: 'http://logicad.cardif.cl/CartolaBack/transaccionesRej',
  urlExtraccion: 'http://logicad.cardif.cl/CartolaBack',
  urlContabilizacionReporte: 'http://logicad.cardif.cl/CartolaBack',
  urlEstados: 'http://logicad.cardif.cl/CartolaBack',
  urlConciliacionReporteRango: 'http://logicad.cardif.cl/CartolaBack',
  urlAgrupacionReporteRango:  'http://logicad.cardif.cl/CartolaBack',
  urlDevolucion: 'http://logicad.cardif.cl/CartolaBack/pagos',

  urlFiltros: 'http://logicad.cardif.cl/CartolaBack/ws/get_filters',
  urlGroup: 'http://logicad.cardif.cl/CartolaBack/ws/get_groupings',
  urlGroupDetalles: 'http://logicad.cardif.cl/CartolaBack/ws/det_agrup/',
  urlContabilizacion: 'http://logicad.cardif.cl/CartolaBack/ws/get_assess',
  urlReversa: 'http://logicad.cardif.cl/CartolaBack/ws/get_assess_reverse',
  urlContaIdVoucher: 'http://logicad.cardif.cl/CartolaBack/ws/set_voucher_manual',

  urlNetsuite: 'http://logicad.cardif.cl/CartolaBack/netsuite/get',
  urlConciliar: 'http://logicad.cardif.cl/CartolaBack/conciliacion/conciliar',
  urlSunsystem:  'http://logicad.cardif.cl/CartolaBack/sunsystem/get',
  urlGroupConci: 'http://logicad.cardif.cl/CartolaBack/ws/get_groupings_pago',
  urlCuentas: 'http://logicad.cardif.cl/CartolaBack/socios/cuentas',
  urlValidaciones: 'http://logicad.cardif.cl/CartolaBack/conciliacion/get_validaciones',

  urlCerrarPeriodo: 'http://logicad.cardif.cl/CartolaBack/ws/set_close',
  urlAbrirPeriodo: 'http://logicad.cardif.cl/CartolaBack/ws/open_periodo',
  urlPeriodosCierre: 'http://logicad.cardif.cl/CartolaBack/ws/periodo_contable',

  urlRecuperosParciales: 'http://logicad.cardif.cl/CartolaBack/recuperos/recuperos_parciales',
  urlContaPago: 'http://logicad.cardif.cl/CartolaBack/conciliacion/get_contabilizacion_manual',
  urlVoucherReintento: 'http://logicad.cardif.cl/CartolaBack/conciliacion/voucher_reintento',
  urlVoucherManual: 'http://logicad.cardif.cl/CartolaBack/conciliacion/set_voucher_manual'
};
