export const environment = {
    production: false,
    
    urlConciliacionReporte: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/',
    urlEntidades: 'http://logicad9.cl.xcd.net.intra/CartolaBack/entidades/',
    urlGastos: 'http://logicad9.cl.xcd.net.intra/CartolaBack/gastos',
    urlSocios: 'http://logicad9.cl.xcd.net.intra/CartolaBack/socios',
    urlSocios2: 'http://localhost:7000/socios',

    urlAuditoria: 'http://logicad9.cl.xcd.net.intra/auditoria',
    urlTransacciones: 'http://logicad9.cl.xcd.net.intra/CartolaBack/transaccionesRej',
    urlExtraccion: 'http://logicad9.cl.xcd.net.intra',
    urlContabilizacionReporte: 'http://logicad9.cl.xcd.net.intra',
    urlEstados: 'http://logicad9.cl.xcd.net.intra',
    urlConciliacionReporteRango: 'http://logicad9.cl.xcd.net.intra',
    urlAgrupacionReporteRango:  'http://logicad9.cl.xcd.net.intra',
    urlDevolucion: 'http://logicad9.cl.xcd.net.intra/pagos',
  
    urlFiltros: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/get_filters',
    urlGroup: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/get_groupings',
    urlGroupDetalles: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/det_agrup/',
    urlContabilizacion: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/get_assess',
    urlReversa: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/get_assess_reverse',
    urlContaIdVoucher: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/set_voucher_manual',
  
    urlNetsuite: 'http://logicad9.cl.xcd.net.intra/CartolaBack/netsuite/get',
    urlConciliar: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/conciliar',
    urlSunsystem:  'http://logicad9.cl.xcd.net.intra/CartolaBack/sunsystem/get',
    urlGroupConci: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/get_groupings_pago',
    urlCuentas: 'http://logicad9.cl.xcd.net.intra/CartolaBack/socios/cuentas',
    urlValidaciones: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/get_validaciones',
  
    urlCerrarPeriodo: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/set_close',
    urlAbrirPeriodo: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/open_periodo',
    urlPeriodosCierre: 'http://logicad9.cl.xcd.net.intra/CartolaBack/ws/periodo_contable',
  
    urlRecuperosParciales: 'http://logicad9.cl.xcd.net.intra/CartolaBack/recuperos/recuperos_parciales',
    urlContaPago: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/get_contabilizacion_manual',
    urlVoucherReintento: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/voucher_reintento',
    urlVoucherManual: 'http://logicad9.cl.xcd.net.intra/CartolaBack/conciliacion/set_voucher_manual'

};
