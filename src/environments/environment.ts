// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // urlAuditoria: 'http://localhost:8080/CARWSCartola/auditoria',
  urlConciliacionReporte: 'http://localhost:8080/CARWSCartola/conciliacion/',
  urlGastos: 'http://localhost:8080/CARWSCartola/gastos',
  urlSocios: 'http://localhost:8080/CARWSCartola/socios',
  urlEntidades: 'http://localhost:8080/CARWSCartola/entidades/',
  urlSocios2: 'http://localhost:7000/socios',
  // urlEntidades: 'http://localhost:7000/entidades/', 
  // urlTransacciones: 'http://localhost:8080/CARWSCartola/transaccionesRej',
  urlAuditoria: 'http://localhost:7000/auditoria',
  urlTransacciones: 'http://localhost:7000/transaccionesRej',
  urlExtraccion: 'http://localhost:7000',
  urlContabilizacionReporte: 'http://localhost:7000',
  urlEstados: 'http://localhost:7000',
  urlConciliacionReporteRango: 'http://localhost:7000',
  urlAgrupacionReporteRango:  'http://localhost:7000',
  urlDevolucion: 'http://localhost:7000/pagos',

  urlFiltros: 'http://localhost:8080/CARWSCartola/ws/get_filters',
  urlGroup: 'http://localhost:8080/CARWSCartola/ws/get_groupings',
  urlGroupDetalles: 'http://localhost:8080/CARWSCartola/ws/det_agrup/',
  urlContabilizacion: 'http://localhost:8080/CARWSCartola/ws/get_assess',
  urlReversa: 'http://localhost:8080/CARWSCartola/ws/get_assess_reverse',
  urlContaIdVoucher: 'http://localhost:8080/CARWSCartola/ws/set_voucher_manual',

  urlNetsuite: 'http://localhost:8080/CARWSCartola/netsuite/get',
  urlConciliar: 'http://localhost:8080/CARWSCartola/conciliacion/conciliar',
  urlSunsystem:  'http://localhost:8080/CARWSCartola/sunsystem/get',
  urlGroupConci: 'http://localhost:8080/CARWSCartola/ws/get_groupings_pago',
  urlCuentas: 'http://localhost:8080/CARWSCartola/socios/cuentas',
  urlValidaciones: 'http://localhost:8080/CARWSCartola/conciliacion/get_validaciones',

  urlCerrarPeriodo: 'http://localhost:8080/CARWSCartola/ws/set_close',
  urlAbrirPeriodo: 'http://localhost:8080/CARWSCartola/ws/open_periodo',
  urlPeriodosCierre: 'http://localhost:8080/CARWSCartola/ws/periodo_contable',

  urlRecuperosParciales: 'http://localhost:8080/CARWSCartola/recuperos/recuperos_parciales',
  urlContaPago: 'http://localhost:8080/CARWSCartola/conciliacion/get_contabilizacion_manual',
  urlVoucherReintento: 'http://localhost:8080/CARWSCartola/conciliacion/voucher_reintento',
  urlVoucherManual: 'http://localhost:8080/CARWSCartola/conciliacion/set_voucher_manual',

  urlCarta1: 'http://localhost:7000/pagos',
};
