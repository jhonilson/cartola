export const environment = {
    production: false,
    
    urlConciliacionReporte: 'http://logicat.cardif.cl/CartolaBack/conciliacion/',
    urlEntidades: 'http://logicat.cardif.cl/CartolaBack/entidades/',
    urlGastos: 'http://logicat.cardif.cl/CartolaBack/gastos',
    urlSocios: 'http://logicat.cardif.cl/CartolaBack/socios',
    urlSocios2: 'http://localhost:7000/socios',

    urlAuditoria: 'http://logicat.cardif.cl/auditoria',
    urlTransacciones: 'http://logicat.cardif.cl/CartolaBack/transaccionesRej',
    urlExtraccion: 'http://logicat.cardif.cl',
    urlContabilizacionReporte: 'http://logicat.cardif.cl',
    urlEstados: 'http://logicat.cardif.cl',
    urlConciliacionReporteRango: 'http://logicat.cardif.cl',
    urlAgrupacionReporteRango:  'http://logicat.cardif.cl',
    urlDevolucion: 'http://logicat.cardif.cl/pagos',
  
    urlFiltros: 'http://logicat.cardif.cl/CartolaBack/ws/get_filters',
    urlGroup: 'http://logicat.cardif.cl/CartolaBack/ws/get_groupings',
    urlGroupDetalles: 'http://logicat.cardif.cl/CartolaBack/ws/det_agrup/',
    urlContabilizacion: 'http://logicat.cardif.cl/CartolaBack/ws/get_assess',
    urlReversa: 'http://logicat.cardif.cl/CartolaBack/ws/get_assess_reverse',
    urlContaIdVoucher: 'http://logicat.cardif.cl/CartolaBack/ws/set_voucher_manual',
  
    urlNetsuite: 'http://logicat.cardif.cl/CartolaBack/netsuite/get',
    urlConciliar: 'http://logicat.cardif.cl/CartolaBack/conciliacion/conciliar',
    urlSunsystem:  'http://logicat.cardif.cl/CartolaBack/sunsystem/get',
    urlGroupConci: 'http://logicat.cardif.cl/CartolaBack/ws/get_groupings_pago',
    urlCuentas: 'http://logicat.cardif.cl/CartolaBack/socios/cuentas',
    urlValidaciones: 'http://logicat.cardif.cl/CartolaBack/conciliacion/get_validaciones',
  
    urlCerrarPeriodo: 'http://logicat.cardif.cl/CartolaBack/ws/set_close',
    urlAbrirPeriodo: 'http://logicat.cardif.cl/CartolaBack/ws/open_periodo',
    urlPeriodosCierre: 'http://logicat.cardif.cl/CartolaBack/ws/periodo_contable',
  
    urlRecuperosParciales: 'http://logicat.cardif.cl/CartolaBack/recuperos/recuperos_parciales',
    urlContaPago: 'http://logicat.cardif.cl/CartolaBack/conciliacion/get_contabilizacion_manual',
    urlVoucherReintento: 'http://logicat.cardif.cl/CartolaBack/conciliacion/voucher_reintento',
    urlVoucherManual: 'http://logicat.cardif.cl/CartolaBack/conciliacion/set_voucher_manual'

};
