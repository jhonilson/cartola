export const environment = {
    production: false,
    
    urlConciliacionReporte: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/',
    urlEntidades: 'http://logicat9.cl.xcd.net.intra/CartolaBack/entidades/',
    urlGastos: 'http://logicat9.cl.xcd.net.intra/CartolaBack/gastos',
    urlSocios: 'http://logicat9.cl.xcd.net.intra/CartolaBack/socios',
    urlSocios2: 'http://localhost:7000/socios',

    urlAuditoria: 'http://logicat9.cl.xcd.net.intra/auditoria',
    urlTransacciones: 'http://logicat9.cl.xcd.net.intra/CartolaBack/transaccionesRej',
    urlExtraccion: 'http://logicat9.cl.xcd.net.intra',
    urlContabilizacionReporte: 'http://logicat9.cl.xcd.net.intra',
    urlEstados: 'http://logicat9.cl.xcd.net.intra',
    urlConciliacionReporteRango: 'http://logicat9.cl.xcd.net.intra',
    urlAgrupacionReporteRango:  'http://logicat9.cl.xcd.net.intra',
    urlDevolucion: 'http://logicat9.cl.xcd.net.intra/pagos',
  
    urlFiltros: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/get_filters',
    urlGroup: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/get_groupings',
    urlGroupDetalles: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/det_agrup/',
    urlContabilizacion: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/get_assess',
    urlReversa: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/get_assess_reverse',
    urlContaIdVoucher: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/set_voucher_manual',
  
    urlNetsuite: 'http://logicat9.cl.xcd.net.intra/CartolaBack/netsuite/get',
    urlConciliar: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/conciliar',
    urlSunsystem:  'http://logicat9.cl.xcd.net.intra/CartolaBack/sunsystem/get',
    urlGroupConci: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/get_groupings_pago',
    urlCuentas: 'http://logicat9.cl.xcd.net.intra/CartolaBack/socios/cuentas',
    urlValidaciones: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/get_validaciones',
  
    urlCerrarPeriodo: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/set_close',
    urlAbrirPeriodo: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/open_periodo',
    urlPeriodosCierre: 'http://logicat9.cl.xcd.net.intra/CartolaBack/ws/periodo_contable',
  
    urlRecuperosParciales: 'http://logicat9.cl.xcd.net.intra/CartolaBack/recuperos/recuperos_parciales',
    urlContaPago: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/get_contabilizacion_manual',
    urlVoucherReintento: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/voucher_reintento',
    urlVoucherManual: 'http://logicat9.cl.xcd.net.intra/CartolaBack/conciliacion/set_voucher_manual'

};
